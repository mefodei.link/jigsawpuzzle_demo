using Cysharp.Threading.Tasks;
using Game.Code.Game.Models;
using Game.Code.JigsawPuzzle.Runtime.Services;
using UniGame.AddressableTools.Runtime;
using UniGame.Core.Runtime;
using UniGame.Core.Runtime.Extension;
using UnityEngine.AddressableAssets;

namespace Game.Code.Gameplay.Models
{
    using UniModules.UniGame.SerializableContext.Runtime.Abstract;
    using UnityEngine;
    
    [CreateAssetMenu(menuName = "Game/Settings/" + nameof(GameSettingsSource) ,
                     fileName = nameof(GameSettingsSource))]
    public class GameSettingsSource : ClassValueAsset<GameModel,IGameModel>
    {
        public AssetReferenceT<JigsawModeSettingsAsset> jigsawModeSettings;
        
        protected override async UniTask<IGameModel> OnRegisterAsync(GameModel value, IContext context)
        {
            var sharedAsset = await jigsawModeSettings
                .LoadAssetTaskAsync<JigsawModeSettingsAsset>(LifeTime)
                .ToSharedInstanceAsync(LifeTime);

            await sharedAsset.RegisterAsync(context);
            
            return value;
        }
    }
}
