using Cysharp.Threading.Tasks;
using Game.Code.Game.StateService;
using Game.Code.JigsawPuzzle.Runtime.Models;
using Game.Code.JigsawPuzzle.Runtime.Services;
using Sirenix.OdinInspector;
using UnityEngine.AddressableAssets;

namespace Game.Code.Game.Models
{
    using System;

    [Serializable]
    public class GameModel : IGameModel
    {
        public bool loadStateAtStart = true;
        
        [ShowIf(nameof(loadStateAtStart))]
        public GameStateId startState;

        public bool AllowLoadGameState => loadStateAtStart;
        public GameStateId StartGameState => startState;
    }
}
