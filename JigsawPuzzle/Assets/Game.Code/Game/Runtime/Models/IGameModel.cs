﻿using Game.Code.Game.StateService;
using Game.Code.JigsawPuzzle.Runtime.Models;

namespace Game.Code.Game.Models
{
    public interface IGameModel
    {
        
        bool AllowLoadGameState { get; }
        
        GameStateId StartGameState { get; }
        
    }
}