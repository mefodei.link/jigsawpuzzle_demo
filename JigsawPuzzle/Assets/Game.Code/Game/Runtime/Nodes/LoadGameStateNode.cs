using System;
using Cysharp.Threading.Tasks;
using Game.Code.Game.Models;
using Game.Code.Game.StateService;
using UniGame.Context.Runtime.Extension;
using UniGame.Core.Runtime;
using UniModules.UniGameFlow.NodeSystem.Runtime.Core.Attributes;

namespace Game.Code.Game.Runtime.Nodes
{
    using UniGame.UniNodes.Nodes.Runtime.Common;

    [CreateNodeMenu("Game/Bootstrap/LoadGameState")]
    [Serializable]
    public class LoadGameStateNode : SContextNode
    {
        protected override async UniTask<bool> OnContextActivate(IContext context)
        {
            var gameModel = await context.ReceiveFirstAsync<IGameModel>();
            var stateService = await context.ReceiveFirstAsync<IGameStateService>(LifeTime);

            var startStateId = (string)gameModel.StartGameState;
            
            if (gameModel.AllowLoadGameState && !string.IsNullOrEmpty(startStateId))
                await stateService.Load(gameModel.StartGameState);
            
            return true;
        }
    }
}
