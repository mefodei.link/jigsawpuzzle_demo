using Game.Code.Game.StateService;
using Sirenix.OdinInspector;

namespace Game.Code.Game.Runtime.Services
{
    using System;
    using Cysharp.Threading.Tasks;
    using UniGame.AddressableTools.Runtime;
    using UniGame.Core.Runtime;
    using UnityEngine.AddressableAssets;
    using UnityEngine;

    [Serializable]
    public class SceneGameState : IGameState
    {
        [DrawWithUnity]
        public AssetReference scene;
        
        public async UniTask<GameStateLoadResult> Load(IContext context, ILifeTime stateLifeTime)
        {

            var sceneInstance = await scene.LoadSceneTaskAsync(stateLifeTime);

            return new GameStateLoadResult()
            {
                isComplete = sceneInstance.Scene.IsValid(),
            };
            
        }
    }
}
