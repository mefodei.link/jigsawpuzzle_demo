﻿using System;
using Cysharp.Threading.Tasks;
using UniGame.Core.Runtime;
using UnityEngine.AddressableAssets;

namespace Game.Code.Game.StateService
{
    [Serializable]
    public class EmptyState : IGameState
    {
        #region inspector

        public AssetReference emptyScene;

        #endregion

        public async UniTask<GameStateLoadResult> Load(IContext context, ILifeTime stateLifeTime)
        {
            await LoadEmptyScene(stateLifeTime);

            return new GameStateLoadResult()
            {
                isComplete = true,
            };
        }

        private async UniTask LoadEmptyScene(ILifeTime lifeTime)
        {
            if (!emptyScene.RuntimeKeyIsValid())
                return;

            await emptyScene.LoadSceneAsync().ToUniTask();
            
            lifeTime.AddCleanUpAction(() => emptyScene.UnLoadScene().ToUniTask().Forget());
        }
    }
}