using Cysharp.Threading.Tasks;
using UniGame.AddressableTools.Runtime;
using UniGame.Core.Runtime;
using UniModules.UniGameFlow.GameFlow.Runtime.Services;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game.Code.Game.StateService
{
    [CreateAssetMenu(menuName = "Game/Services/GameStateService",fileName = nameof(GameStateService))]
    public class GameStateServiceSource : ServiceDataSourceAsset<IGameStateService>
    {
        
        public AssetReferenceT<GameStateSettingsAsset> _settings;

        protected override async UniTask<IGameStateService> CreateServiceInternalAsync(IContext context)
        {
            var settings = await _settings
                .LoadAssetInstanceTaskAsync<GameStateSettingsAsset>(LifeTime,true);

            var service = new GameStateService(context, settings.Value);

            return service;
        }
    }
}
