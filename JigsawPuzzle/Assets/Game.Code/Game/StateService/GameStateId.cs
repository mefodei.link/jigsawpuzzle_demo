﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Code.Game.StateService
{
    [Serializable]
    [ValueDropdown("@Game.Code.Game.StateService.GameStateId.GetKeys()")]
    public struct GameStateId  : IEquatable<GameStateId>, IEquatable<string>
    {
        [SerializeField]
        private string _value;

        public static implicit operator string(GameStateId v)
        {
            return v._value;
        }

        public static explicit operator GameStateId(string v)
        {
            return new GameStateId { _value = v };
        }

        public override string ToString() => string.IsNullOrEmpty(_value) 
            ? string.Empty 
            : _value;

        public override int GetHashCode() => string.IsNullOrEmpty(_value) 
            ? 0 
            : _value.GetHashCode();

        public GameStateId FromInt(string data)
        {
            _value = data;
            return this;
        }

        public bool Equals(GameStateId other) => other._value == _value;
        
        public bool Equals(string other) => other == _value;

        public override bool Equals(object obj)
        {
            if (obj is GameStateId mask)
                return mask._value == _value;
            
            return false;
        }

        public static IEnumerable<GameStateId> GetKeys()
        {
            var keys = GameStateKeys.GetStates();
            foreach (var key in keys)
            {
                yield return (GameStateId) key;
            }
        }
    }
}