using UnityEngine.AddressableAssets;

namespace Game.Code.Game.StateService
{
    using System;
    using UniModules.UniGame.Core.Runtime.DataStructure;
    using UnityEngine;
    
    [Serializable]
    public class GameStateSettings
    {
        [SerializeReference]
        public IGameState emptyState = new EmptyState();
        
        [SerializeField]
        public SerializableDictionary<string, GameStateData> states 
            = new SerializableDictionary<string, GameStateData>();
    }
}