namespace Game.Code.Game.StateService
{
    using System;
    using System.Collections.Generic;
    using Cysharp.Threading.Tasks;
    using UniGame.Core.Runtime;
    using UniGame.UniNodes.GameFlow.Runtime;
    using UniModules.UniCore.Runtime.DataFlow;
    using UnityEngine.SceneManagement;

    [Serializable]
    public class GameStateService : GameService, IGameStateService
    {
        public static GameStateLoadResult NonValidStateId = new GameStateLoadResult()
        {
            error = "State with target ID not found",
            isComplete = false,
        };

        private readonly IContext _context;
        private readonly GameStateSettings _settings;
        private Dictionary<string, GameStateData> _states;
        private LifeTimeDefinition _stateLifeTime = new LifeTimeDefinition();

        public GameStateService(IContext context,GameStateSettings settings)
        {
            _context = context;
            _settings = settings;
            _states = _settings.states;
        }

        public async UniTask<GameStateLoadResult> Load(string stateId)
        {
            if(!_states.TryGetValue(stateId,out var stateData))
                return NonValidStateId;

            _stateLifeTime.Release();
            
            var state = stateData.state;

            await LoadEmptyState(stateData, _context, _stateLifeTime);
            
            var loadResult = await state.Load(_context, _stateLifeTime);

            return loadResult;
        }

        private async UniTask LoadEmptyState(GameStateData data,IContext context,ILifeTime lifeTime)
        {
            if (data.loadMode == LoadSceneMode.Additive)
                return;
            
            var emptyState = _settings.emptyState;
            if (emptyState == null) return;
            
            await emptyState.Load(context, lifeTime);
        }
    }

    [Serializable]
    public struct GameStateLoadResult
    {
        public bool isComplete;
        public Exception exception;
        public string error;
    }

    [Serializable]
    public struct GameStateLoadRequest
    {
        public string id;
    }
}
