﻿using Cysharp.Threading.Tasks;
using UniGame.GameFlow.Runtime.Interfaces;

namespace Game.Code.Game.StateService
{
    public interface IGameStateService : IGameService
    {
        UniTask<GameStateLoadResult> Load(string stateId);
    }
}