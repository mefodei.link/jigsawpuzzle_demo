﻿namespace Game.Code.Game.StateService
{
    using UniModules.UniGame.SerializableContext.Runtime.Abstract;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Game/Services/GameStateService Settings",fileName = nameof(GameStateSettings))]
    public class GameStateSettingsAsset : ClassValueAsset<GameStateSettings,GameStateSettings>
    {
        
    }
}