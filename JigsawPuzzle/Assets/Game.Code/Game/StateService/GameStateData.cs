﻿using UnityEngine.SceneManagement;

namespace Game.Code.Game.StateService
{
    using System;
    using UniGame.GameFlow.Runtime.Interfaces;
    using UnityEngine;
    
    [Serializable]
    public class GameStateData
    {
        public LoadSceneMode loadMode = LoadSceneMode.Single;

        public bool showLoader = true;
        
        [SerializeReference]
        public IGameState state;
    }
}