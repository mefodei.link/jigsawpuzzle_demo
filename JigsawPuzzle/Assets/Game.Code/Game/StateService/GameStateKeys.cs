﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;

namespace Game.Code.Game.StateService
{
    public static class GameStateKeys
    {
        public static IEnumerable<string> GetStates()
        {

#if UNITY_EDITOR

            var asset = AssetDatabase.FindAssets($"t:{nameof(GameStateSettingsAsset)}");
            var assetId = asset.FirstOrDefault();
            if (string.IsNullOrEmpty(assetId)) yield break;

            var path = AssetDatabase.GUIDToAssetPath(assetId);
            var settingsAsset = AssetDatabase.LoadAssetAtPath<GameStateSettingsAsset>(path);
            var settings = settingsAsset.defaultValue;
            
            foreach (var state in settings.states)
            {
                yield return state.Key;
            }
            
#endif

            yield break;

        }
    }
}