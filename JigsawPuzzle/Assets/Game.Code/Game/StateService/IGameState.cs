﻿using Cysharp.Threading.Tasks;
using UniGame.Core.Runtime;

namespace Game.Code.Game.StateService
{
    public interface IGameState
    {
        UniTask<GameStateLoadResult> Load(IContext context, ILifeTime stateLifeTime);
    }
}