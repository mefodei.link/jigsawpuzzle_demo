namespace Game.Code.Game.LevelService
{
    using Cysharp.Threading.Tasks;
    using UniGame.AddressableTools.Runtime;
    using UniGame.Core.Runtime;
    using UniGame.Core.Runtime.Extension;
    using UniModules.UniGameFlow.GameFlow.Runtime.Services;
    using UnityEngine;
    using UnityEngine.AddressableAssets;
    
    [CreateAssetMenu(menuName = "Game/Services/LevelsService/"+nameof(LevelsServiceSource),fileName = nameof(LevelsServiceSource))]
    public class LevelsServiceSource : ServiceDataSourceAsset<ILevelsService>
    {

        public AssetReferenceT<LevelServiceSettingsAsset> settingsAsset;

        protected override async UniTask<ILevelsService> CreateServiceInternalAsync(IContext context)
        {
            var settings = await settingsAsset
                .LoadAssetInstanceTaskAsync<LevelServiceSettingsAsset>(LifeTime,true)
                .ToSharedInstanceAsync();

            var settingsModel = settings.Value;
            var levelsService = new LevelsService(settingsModel);
            
            context.Publish(settingsModel.DifficultyPresets);
            
            return levelsService;
        }
    }
}
