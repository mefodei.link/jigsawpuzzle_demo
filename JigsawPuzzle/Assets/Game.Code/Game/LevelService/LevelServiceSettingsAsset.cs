﻿using System;
using UniModules.UniGame.SerializableContext.Runtime.Abstract;
using UnityEngine;

namespace Game.Code.Game.LevelService
{
    [CreateAssetMenu(menuName = "Game/Services/LevelsService/Settings")]
    public class LevelServiceSettingsAsset : ClassValueAsset<LevelsServiceSettings, LevelsServiceSettings>
    {
        
    }
}