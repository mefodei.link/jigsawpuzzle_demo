﻿using System;
using System.Collections.Generic;
using Game.Code.Lobby.Models;

namespace Game.Code.Game.LevelService
{
    [Serializable]
    public class DifficultyPresets : IDifficultyPresets
    {
        
        public List<LevelDifficultyModel> difficultyPresets = new List<LevelDifficultyModel>();

        public List<LevelDifficultyModel> Difficulties => difficultyPresets;
        
    }
}