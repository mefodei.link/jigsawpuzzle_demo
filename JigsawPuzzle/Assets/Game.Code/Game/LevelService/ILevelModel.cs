﻿using UnityEngine.AddressableAssets;

namespace Game.Code.Game.LevelService
{
    public interface ILevelModel
    {
        string Id { get; }
        bool GenerateBackground { get; }
        AssetReferenceSprite SpriteReference { get; }
    }
}