﻿using System;
using UnityEngine.AddressableAssets;

namespace Game.Code.Game.LevelService
{
    [Serializable]
    public class LevelModel : ILevelModel
    {
        public string id;
        public string category;
        public AssetReferenceSprite sprite;
        public bool generateBackground = true;

        public string Id => id;

        public AssetReferenceSprite SpriteReference => sprite;

        public bool GenerateBackground => generateBackground;
    }
}