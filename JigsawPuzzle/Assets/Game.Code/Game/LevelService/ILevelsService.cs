﻿using Cysharp.Threading.Tasks;
using Game.Code.Lobby.Models;

namespace Game.Code.Game.LevelService
{
    using UniGame.GameFlow.Runtime.Interfaces;
    
    public interface ILevelsService : IGameService
    {
        IGameLevelModel ActiveLevel { get; }
        
        UniTask ActivateLevelAsync(string id, int preset);
        
        IDifficultyPresets GetLevelPresets();
        
        UniTask<ILevelModel> GetLevelAsync(string id);
    }
}