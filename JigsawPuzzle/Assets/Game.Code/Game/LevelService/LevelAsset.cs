﻿using UniModules.UniGame.SerializableContext.Runtime.Abstract;
using UnityEngine;

namespace Game.Code.Game.LevelService
{
    [CreateAssetMenu(menuName = "Game/Services/LevelsService/Level",fileName = "Level")]
    public class LevelAsset : ClassValueAsset<LevelModel,ILevelModel>
    {
    
    }
}