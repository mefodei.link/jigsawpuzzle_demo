﻿namespace Game.Code.Lobby.Models
{
    public interface ILevelDifficultyModel
    {
        int Rows { get; }
        int Duration { get; }
        int Columns { get; }
    }
}