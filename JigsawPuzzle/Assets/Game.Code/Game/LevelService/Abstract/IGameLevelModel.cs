﻿using Game.Code.Game.LevelService;
using UnityEngine.AddressableAssets;

namespace Game.Code.Lobby.Models
{
    public interface IGameLevelModel
    {
        ILevelModel Level { get; }

        ILevelDifficultyModel Difficulty { get; }
    }
}