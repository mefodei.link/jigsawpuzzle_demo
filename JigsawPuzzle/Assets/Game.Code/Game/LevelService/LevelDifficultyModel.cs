﻿using System;

namespace Game.Code.Lobby.Models
{
    [Serializable]
    public class LevelDifficultyModel : ILevelDifficultyModel
    {
        public int columns;
        public int rows;
        public int duration;

        public int Columns => columns;

        public int Rows => rows;
        
        public int Duration => duration;
    }
}