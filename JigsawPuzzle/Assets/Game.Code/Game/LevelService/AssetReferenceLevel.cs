﻿using System;
using UnityEngine.AddressableAssets;

namespace Game.Code.Game.LevelService
{
    [Serializable]
    public class AssetReferenceLevel : AssetReferenceT<LevelAsset>
    {
        public AssetReferenceLevel(string guid) : base(guid)
        {
        }
    }
}