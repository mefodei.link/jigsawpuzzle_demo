using System.Linq;
using Cysharp.Threading.Tasks;
using Game.Code.Game.LevelService;
using Game.Code.Lobby.Models;
using UniGame.AddressableTools.Runtime;

namespace Game.Code.Game.LevelService
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UniGame.UniNodes.GameFlow.Runtime;
    using UnityEngine;

    [Serializable]
    public class LevelsService : GameService, ILevelsService
    {
        private readonly LevelsServiceSettings _settings;
        private GameLevelModel _activeLevel = new GameLevelModel();

        public LevelsService(LevelsServiceSettings settings)
        {
            _settings = settings;
            ActivateLevel(_settings.defaultLevel.Value,_settings.defaultPreset);
        }

        public IGameLevelModel ActiveLevel => _activeLevel;

        public async UniTask ActivateLevelAsync(string id, int preset)
        {
            _activeLevel.level = await GetLevelAsync(id);
            _activeLevel.difficulty = GetPreset(preset);
        }

        public ILevelDifficultyModel GetPreset(int id)
        {
            return GetLevelPresets().Difficulties[id];
        }
        
        public IDifficultyPresets GetLevelPresets()
        {
            return _settings.difficultyPresets;
        }

        public async UniTask<ILevelModel> GetLevelAsync(string id)
        {
            var levelData = _settings.levels.FirstOrDefault();
            if (levelData == null) return _settings.defaultLevel.Value;

            var asset = levelData.Value;
            return asset;
        }

        private void ActivateLevel(ILevelModel level, ILevelDifficultyModel difficultyModel)
        {
            _activeLevel.level = level;
            _activeLevel.difficulty = difficultyModel;
        }
        
    }
}