using System;
using Game.Code.Game.LevelService;
using UnityEngine.AddressableAssets;

namespace Game.Code.Lobby.Models
{
    [Serializable]
    public class GameLevelModel : IGameLevelModel
    {
        public ILevelModel level = new LevelModel();

        public ILevelDifficultyModel difficulty = new LevelDifficultyModel()
        {
            columns = 8,
            rows = 8,
            duration = 20,
        };
        
        public ILevelModel Level => level;

        public ILevelDifficultyModel Difficulty => difficulty;
    }
}
