using System.Collections.Generic;
using Game.Code.Lobby.Models;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Code.Game.LevelService
{
    using System;
    using System.Collections;

    [Serializable]
    public class LevelsServiceSettings
    {
        [InlineProperty] [HideLabel] [BoxGroup("presets")]
        public DifficultyPresets difficultyPresets = new DifficultyPresets();

        [InlineEditor()] 
        [BoxGroup("defaults")]
        public LevelAsset defaultLevel;

        [SerializeField]
        [InlineProperty]
        [BoxGroup("defaults")]
        public LevelDifficultyModel defaultPreset = new LevelDifficultyModel()
        {
            columns = 6,
            rows = 6,
            duration = 2,
        };
        
        [Space]
        [InlineEditor()]
        [BoxGroup("levels")]
        public List<LevelAsset> levels = new List<LevelAsset>();

        public IDifficultyPresets DifficultyPresets => difficultyPresets;
    }
}