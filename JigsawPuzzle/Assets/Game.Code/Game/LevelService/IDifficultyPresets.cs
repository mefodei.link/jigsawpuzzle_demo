﻿using System.Collections.Generic;
using Game.Code.Lobby.Models;

namespace Game.Code.Game.LevelService
{
    public interface IDifficultyPresets
    {
        List<LevelDifficultyModel> Difficulties { get; }
    }
}