namespace GameContent.Editor.GameEditor
{
    using UniModules.Editor.OdinTools.GameEditor;
    using UnityEditor;
    using UnityEngine;
    
    public class JigsawGameEditorWindow : GeneralGameEditorWindow<JigsawGameEditorConfiguration>
    {
        #region static data
        
        [MenuItem("Game/Editors/GameEditor")]
        static public void OpenWindow()
        {
            var window = GetWindow<JigsawGameEditorWindow>();
            window.minSize      = new Vector2(512f, 512f);
            window.titleContent = new GUIContent(nameof(JigsawGameEditorWindow));
            window.Show();
        }

        #endregion
    }
}
