﻿using UniModules.Editor.OdinTools.GameEditor;
using UniModules.UniGame.Core.Editor.EditorProcessors;

namespace GameContent.Editor.GameEditor
{
    [GeneratedAssetInfo("Game/Editors/Assets/" + nameof(JigsawGameEditorConfiguration))]
    public class JigsawGameEditorConfiguration : BaseEditorConfiguration<JigsawGameEditorConfiguration>
    {
        
    }
}