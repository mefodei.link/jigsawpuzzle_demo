﻿using System;
using System.Collections.Generic;
using UniGame.Core.Runtime;

namespace Game.Code.Bootstrap.Runtime
{
    using Cysharp.Threading.Tasks;
    using UniCore.Runtime.ProfilerTools;
    using UniGame.GameRuntime.Abstract;
    using UniModules.UniCore.Runtime.DataFlow;
    using UniGame.AddressableTools.Runtime;
    using UniModules.UniGame.GameFlow.GameFlow.Runtime;
    using UnityEngine;
    using UnityEngine.AddressableAssets;
    using Object = UnityEngine.Object;

    /// <summary>
    /// game bootstrap point
    /// </summary>
    public static class GameBootstrap
    {
        private static LifeTimeDefinition _lifeTime = new LifeTimeDefinition();

        public static GameBootData GameBootData = new GameBootData();

        public static Dictionary<string, Func<ILifeTime,UniTask<bool>>> _bootStages
            = new() {
                {nameof(OnSetupAsync), OnSetupAsync},
                {nameof(LoadBootSettingsAsync), LoadBootSettingsAsync},
                {nameof(InitializeAddressableAsync), InitializeAddressableAsync},
                {nameof(OnInitializeGameManagerAsync), OnInitializeGameManagerAsync},
                {nameof(OnStartGameManagerAsync), OnStartGameManagerAsync},
            };
        
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void Initialize()
        {
            InitializeInner().Forget();
        }

        public static void RestartGame() => Restart().Forget();

        private static async UniTask InitializeInner()
        {
            foreach (var stage in _bootStages)
            {
                var stageName = stage.Key;
                var stageFunc = stage.Value;
                
                Debug.Log($"Game Boot STAGE Execute: {stageName}");
                
                var stageResult = await stageFunc.Invoke(_lifeTime);
                if (!stageResult)
                {
                    Debug.LogError($"Game Boot STAGE ERROR: {stageName}");
                    return;
                }
                
                Debug.Log($"Game Boot STAGE Complete: {stageName}");
            }
        }

        private static async UniTask<bool> OnInitializeGameManagerAsync(ILifeTime lifeTime)
        {
            GameLog.LogRuntime("GameBootstrap: Initialize");

            var settings = GameBootData.Settings;
            var gameManagerObject = await settings.gameManagerReference
                .LoadAssetInstanceTaskAsync<GameObject>(lifeTime,true);
            Object.DontDestroyOnLoad(gameManagerObject);
            gameManagerObject.SetActive(true);

            GameBootData.GameManager = gameManagerObject.GetComponent<GameManager>();

            return true;
        }

        private static UniTask<bool> OnStartGameManagerAsync(ILifeTime lifeTime)
        {
            var manager = GameBootData.GameManager;
            manager.Execute()
                .AttachExternalCancellation(lifeTime.TokenSource)
                .Forget();

            return UniTask.FromResult(true);
        }

        private static UniTask<bool> OnSetupAsync(ILifeTime lifeTime)
        {
            _lifeTime ??= new LifeTimeDefinition();
            _lifeTime.Release();
            _lifeTime.AddCleanUpAction(() =>
            {
                GameBootData.GameManager = null;
            });
            
            return UniTask.FromResult(true);
        }

        private static async UniTask<bool> InitializeAddressableAsync(ILifeTime lifeTime)
        {
            await Addressables.InitializeAsync();
            GameLog.LogRuntime("Addressable initialized");
            return true;
        }

        private static async UniTask<bool> LoadBootSettingsAsync(ILifeTime lifeTime)
        {
            var settings = await GameBootstrapSettings
                .GameBootstrapSettingsTag
                .LoadAddressableByResourceAsync<GameBootstrapSettings>(lifeTime);

            settings = Object.Instantiate(settings);
            settings.DestroyWith(lifeTime);
            GameBootData.Settings = settings;
            return settings.IsBootEnabled;
        }

        private static async UniTask Restart()
        {
            GameLog.LogRuntime("GameBootstrap: Restart Game");

            await UniTask.SwitchToMainThread();

            _lifeTime?.Release();

            await UniTask.Yield(PlayerLoopTiming.LastPostLateUpdate);
            await UniTask.Yield();
            await UniTask.Yield(PlayerLoopTiming.LastPostLateUpdate);

            Initialize();
        }


        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        private static void OnPlayModeChanged()
        {
            Resources.UnloadUnusedAssets();
            _lifeTime?.Release();
            Resources.UnloadUnusedAssets();

#if UNITY_EDITOR
            GameLog.Log("GAME: RESET GAMEMANAGER", Color.green);
#endif
        }
    }
    
            
    public struct GameBootData
    {
        public IGameManager GameManager;
        public GameBootstrapSettings Settings;
    }
}