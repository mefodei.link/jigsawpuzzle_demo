using Game.Code.Game.StateService;
using UniGame.AddressableTools.Runtime;
using UniModules.UniGame.GameFlow.GameFlow.Runtime;
using UnityEngine.AddressableAssets;

namespace Game.Code.Bootstrap.Runtime
{
    using UnityEngine;

    [CreateAssetMenu(menuName = "Game/Bootstrap/"+nameof(GameBootstrapSettings),fileName = nameof(GameBootstrapSettings))]
    public class GameBootstrapSettings : ScriptableObject
    {
        public static string GameBootstrapSettingsTag = nameof(GameBootstrapSettings);

        [SerializeField]
        public bool isBootEnabled = true;

        public AssetReferenceComponent<GameManager> gameManagerReference;

        public bool IsBootEnabled
        {
            get => isBootEnabled;
            set
            {
                isBootEnabled = value;
#if UNITY_EDITOR
                UniModules.Editor.EditorExtension.MarkDirty(this);
#endif
            }
        }
        
    }
    
}
