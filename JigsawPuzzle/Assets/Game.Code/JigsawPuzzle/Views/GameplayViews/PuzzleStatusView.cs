namespace Game.Code.JigsawPuzzle.Views.GameplayViews
{
    using Cysharp.Threading.Tasks;
    using TMPro;
    using UniGame.Rx.Runtime.Extensions;
    using UnityEngine.UI;
    using UniGame.UiSystem.Runtime;

    public class PuzzleStatusView : UiCanvasGroupView<IPuzzleStatusViewModel>
    {
        #region inspector

        public TextMeshProUGUI label;
        
        public Image icon;
        
        #endregion

        protected override UniTask OnViewInitialize(IPuzzleStatusViewModel model)
        {
            this.Bind(model.Image, icon)
                .Bind(model.Label, label);
            
            return UniTask.CompletedTask;
        }
    }
}
