namespace Game.Code.JigsawPuzzle.Views.GameplayViews
{
    using Cysharp.Threading.Tasks;
    using Runtime.Services;
    using ViewModels;
    using PuzzleScrollViews;
    using UniGame.UiSystem.Runtime;
    using UniGame.Rx.Runtime.Extensions;
    using UnityEngine;

    public class JigsawGameplayView : UiView<JigsawGameplayAreaViewModel>,IPuzzleArea
    {
        #region inspector

        public PuzzleStatusView timeStatus;
        public PuzzleStatusView progressStatus;

        public JigsawButtonsPanelView buttonsPanel;
        public PuzzleGameAreaView puzzleGameAreaView;
        
        public PuzzleItemsScrollView scrollView;
        public Canvas canvas;
        public RectTransform gameArea;

        #endregion
        
        protected override UniTask OnInitialize(JigsawGameplayAreaViewModel model)
        {
            canvas.renderMode = RenderMode.ScreenSpaceCamera;

            this.Bind(model.Camera,x => canvas.worldCamera = x)
                .Bind(model.GameArea,puzzleGameAreaView)
                .Bind(model.ButtonsPanel,buttonsPanel)
                .Bind(model.Items, scrollView)
                .Bind(model.Time,timeStatus)
                .Bind(model.Progress,progressStatus);

            scrollView.Show();
            return UniTask.CompletedTask;
        }

        public bool IsScreenPointInArea(Vector2 point, Camera gameCamera)
        {
            var isInArea = RectTransformUtility.RectangleContainsScreenPoint(gameArea, point, gameCamera);
            return isInArea;
        }

        public int GetElementInAreaByScreenPoint(Vector2 point, Camera gameCamera) => Model.GetPuzzleByPoint(point);
    }
    
    
}
