namespace Game.Code.JigsawPuzzle.Views.GameplayViews
{
    using Cysharp.Threading.Tasks;
    using ViewModels;
    using UniGame.UiSystem.Runtime;
    
    public class JigsawButtonsPanelView : UiView<IJigsawButtonsPanelViewModel>
    {
        public ButtonView homeButton;
        public ButtonView settingsButton;
        public ToggleButtonView cornersButton;
        public ToggleButtonView backgroundButton;

        protected override async UniTask OnInitialize(IJigsawButtonsPanelViewModel model)
        {
            if (homeButton) await homeButton.Initialize(model.Home, Layout);
            if (settingsButton) await settingsButton.Initialize(model.Settings, Layout);
            if (cornersButton) await cornersButton.Initialize(model.Corners, Layout);
            if (backgroundButton) await backgroundButton.Initialize(model.Background, Layout);
        }
    }
}
