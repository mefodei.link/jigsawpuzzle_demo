namespace Game.Code.JigsawPuzzle.Views.GameplayViews
{
    using Cysharp.Threading.Tasks;
    using ViewModels;
    using TMPro;
    using UniGame.Rx.Runtime.Extensions;
    using UniGame.UiSystem.Runtime;
    using UnityEngine.UI;
    using UnityEngine;

    public class ButtonView : UiView<IButtonViewModel>
    {
        public TextMeshProUGUI label;
        public Image icon;
        public Button button;

        protected override UniTask OnInitialize(IButtonViewModel model)
        {
            this.Bind(model.Icon, icon)
                .Bind(model.Label, label)
                .Bind(button, model.Execute);

            return UniTask.CompletedTask;
        }
    }
    
}
