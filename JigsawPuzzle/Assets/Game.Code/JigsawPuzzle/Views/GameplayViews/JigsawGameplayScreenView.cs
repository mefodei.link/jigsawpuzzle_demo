namespace Game.Code.JigsawPuzzle.Views.GameplayViews
{
    using PuzzleScrollViews;
    using UniGame.Rx.Runtime.Extensions;
    using Cysharp.Threading.Tasks;
    using ViewModels;
    using UniGame.UiSystem.Runtime;

    public class JigsawGameplayScreenView : UiCanvasGroupView<JigsawGameplayViewModel>
    {
        public PuzzleItemsScrollView scrollView;
        
        protected override UniTask OnViewInitialize(JigsawGameplayViewModel model)
        {
            this.Bind(model.Items, scrollView);
            scrollView.Show();
            return UniTask.CompletedTask;
        }
    }
}
