using Cysharp.Threading.Tasks;
using DG.Tweening;
using Game.Code.JigsawPuzzle.ViewModels;
using Sirenix.OdinInspector;
using UniGame.Rx.Runtime.Extensions;
using UniGame.UiSystem.Runtime;
using UniModules.UniCore.Runtime.DataFlow;
using UniModules.UniGame.DoTweenRoutines.Runtime;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Code.JigsawPuzzle.Views.GameplayViews
{
    public class PuzzleGameAreaView : UiView<IGameAreaViewModel>
    {
        #region inspector

        public Image background;

        public RectTransform gameBackground;

        [OnValueChanged(nameof(UpdateSize))]
        public Vector2 size;
        
        #endregion
        
        private LifeTimeDefinition _animationLifeTime = new LifeTimeDefinition();

        protected override UniTask OnInitialize(IGameAreaViewModel model)
        {
            _animationLifeTime.Release();

            AnimateAlpha(0f);
            
            this.Bind(model.Size, UpdateSize)
                .Bind(model.Image, background)
                .Bind(model.Transparency,AnimateAlpha);
            
            return UniTask.CompletedTask;
        }


        private void UpdateSize(Vector2 rectSize)
        {
            var rectTransform = gameBackground;
            var sizeDelta = rectTransform.sizeDelta;
            var with = rectSize.x;
            var height = rectSize.y;
            
            with = with > 0 ? with : sizeDelta.x;
            height = height > 0 ? height : sizeDelta.y;

            rectTransform.sizeDelta = new Vector2(with, height);
            rectTransform.ForceUpdateRectTransforms();
        }

        private void AnimateAlpha(float alpha)
        {
            _animationLifeTime.Release();
            
            background.DOFade(alpha,0.2f).AddTo(_animationLifeTime);
        }
    }
}
