using UniRx;

namespace Game.Code.JigsawPuzzle.Views.GameplayViews
{
    using Cysharp.Threading.Tasks;
    using ViewModels;
    using TMPro;
    using UniGame.Rx.Runtime.Extensions;
    using UniGame.UiSystem.Runtime;
    using UnityEngine.UI;
    using UnityEngine;

    public class ToggleButtonView : UiView<IToggleButtonViewModel>
    {
        public TextMeshProUGUI label;
        public Image icon;
        public Toggle button;

        protected override UniTask OnInitialize(IToggleButtonViewModel model)
        {
            this.Bind(model.Icon, icon)
                .Bind(model.Label, label)
                .Bind(button, model.Execute)
                .Bind(model.Execute.Where(x => x != button.isOn),button)
                .Bind(model.Enabled,x => button.enabled = x);

            return UniTask.CompletedTask;
        }
    }
    
}
