﻿using System;
using UniGame.Core.Runtime;
using UniModules.UniCore.Runtime.DataFlow;
using UniModules.UniCore.Runtime.Utils;

namespace Game.Code.JigsawPuzzle.Views.PuzzleScrollViews
{
    using TMPro;
    using FancyScrollView;
    using UnityEngine;
    using UnityEngine.UI;
    using UniRx;
    
    public class PuzzleScrollCell : FancyCell<PuzzleElementViewModel, PuzzleItemViewContext>, ILifeTimeContext
    {
        [SerializeField] private TextMeshProUGUI message = default;
        [SerializeField] private Image image = default;
        [SerializeField] private bool preserveAspect = true;
        [SerializeField] private bool setNativeSize = true;
        [SerializeField] private RectTransform animationTarget;
        [SerializeField] private Vector2 anchorMax = new Vector2(0.5f, 4.45f);
        [SerializeField] private Vector2 anchorMin = new Vector2(0.5f, -3.45f);

        private ILifeTime _assetLifeTime;
        private PuzzleElementViewModel _model;
        private LifeTimeDefinition _viewLifeTime = new LifeTimeDefinition();
        private Vector2 _originSize;
        private float _currentPosition = 0;

        public RectTransform Page => image.rectTransform;

        public sealed override RectTransform RectTransform => Page;

        public int Id => _model.id;

        public ILifeTime LifeTime => _assetLifeTime;
        
        public PuzzleElementViewModel Model => _model;

        public override void UpdateContent(PuzzleElementViewModel item)
        {
            _viewLifeTime.Release();
            
            _model = item;
            _model.state.Subscribe(OnStateChange)
                .AddTo(_viewLifeTime);
            
            name = _model.id.ToStringFromCache();
            message.text = item.Message;
            image.sprite = item.image;
            
            
            if(preserveAspect)
                image.preserveAspect = preserveAspect;
            if(setNativeSize)
                image.SetNativeSize();

            ApplyElementOffset(item);
        }

        public override void UpdatePosition(float position)
        {
            var lerpPosition = Vector2.Lerp( anchorMax,anchorMin, position);
            animationTarget.anchorMax = lerpPosition;
            animationTarget.anchorMin = lerpPosition;
        }

        private void OnStateChange(PuzzleElementViewState state)
        {
            var color = image.color;

            switch (state)
            {
                case PuzzleElementViewState.Active:
                    color.a = 1f;
                    image.color = color;
                    break;
                case PuzzleElementViewState.Empty:
                    color.a = 0f;
                    image.color = color;
                    break;
            }
        }
        
        private void ApplyElementOffset(PuzzleElementViewModel model)
        {
            var element = model.element;
            var size = image.sprite.rect;

            var ratio = size.width / element.maskWidth;
            ratio = ratio > 1 ? 1 - (ratio % 1) : ratio;
            
            var shapeWidth = element.shapeWidth;
            var sizeUnit = ratio * shapeWidth / 2f;//20
            var offsetUnit = sizeUnit / 2f;
            
            var offsetX = 0f;
            var position = Vector2.zero;
            
            if (element.left != 1) offsetX -= sizeUnit;
            if (element.right != 1) offsetX -= sizeUnit;

            if (element.left == 1) position.x -= offsetUnit;
            if (element.right == 1) position.x += offsetUnit;
            
            animationTarget.sizeDelta = new Vector2(_originSize.x + offsetX,_originSize.y);
            animationTarget.anchoredPosition = position;
        }
        
        private void Awake()
        {
            _assetLifeTime = this.GetLifeTime();
            _originSize = animationTarget.sizeDelta;
        }

        //private void OnEnable() => UpdatePosition(currentPosition);

    }
}
