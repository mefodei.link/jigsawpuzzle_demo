﻿using System.Collections.Generic;
using EasingCore;
using FancyScrollView;
using UniModules.UniGame.Core.Runtime.Rx;
using UniRx;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.Views.PuzzleScrollViews
{
    public class PuzzleItemScrollControl : FancyScrollView<PuzzleElementViewModel, PuzzleItemViewContext>
    {
        #region inspector
        
        [SerializeField] public Scroller scroller = default;
        [SerializeField] public GameObject cellPrefab = default;
        [SerializeField] public int showInContainer = 6;
        
        #endregion

        private bool isItemsVisible = false;
        private List<PuzzleScrollCell> _cells = new List<PuzzleScrollCell>();
        private RecycleReactiveProperty<int> _selectedIndex = new RecycleReactiveProperty<int>();

        protected override GameObject CellPrefab => cellPrefab;

        public IReadOnlyReactiveProperty<int> SelectedIndex => _selectedIndex;

        public Scroller Scroller => scroller;

        public IReadOnlyList<PuzzleScrollCell> Cells => _cells;

        protected override void Initialize()
        {
            base.Initialize();

            Context.OnCellClicked = SelectCell;
            
            scroller.OnValueChanged(UpdatePosition);
            scroller.OnSelectionChanged(UpdateSelection);
        }

        private void UpdateSelection(int index)
        {
            if (Context.SelectedIndex == index)
                return;

            Context.SelectedIndex = index;
            _selectedIndex.Value = index;
            
            Refresh();
        }

        public void UpdateData(IList<PuzzleElementViewModel> items)
        {
            UpdateContents(items);
            scroller.SetTotalCount(items.Count);
            UpdateShown(items);
            
            _cells.Clear();
            gameObject.GetComponentsInChildren<PuzzleScrollCell>(_cells);
        }
        
        private void UpdateShown(IList<PuzzleElementViewModel> items)
        {
            var isAllVisible = items.Count <= showInContainer;
            var changed = isItemsVisible != isAllVisible;
            isItemsVisible = isAllVisible;

            distinctSource = isAllVisible;
            scroller.Draggable = !isAllVisible;
            scroller.MovementType = isAllVisible ? MovementType.Clamped : MovementType.Unrestricted;
        }

        public void SelectCell(int index)
        {
            SelectCell(index, 0.2f, false);
        }

        public void SelectCell(int index, float time, bool force)
        {
            if (index < 0 || 
                index >= ItemsSource.Count || 
                (!force && index == Context.SelectedIndex))
                return;
            
            UpdateSelection(index);
            UpdatePosition(index);

            if (time <= 0)
            {
                scroller.JumpTo(index);
            }
            else
            {
                scroller.ScrollTo(index, 0.2f, Ease.OutCubic); 
            }
            
        }
    }
}
