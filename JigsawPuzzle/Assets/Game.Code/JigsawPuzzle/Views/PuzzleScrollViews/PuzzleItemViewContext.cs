﻿namespace Game.Code.JigsawPuzzle.Views.PuzzleScrollViews
{
    using System;

    public class PuzzleItemViewContext
    {
        public int SelectedIndex = -1;
        public Action<int> OnCellClicked;
    }
}
