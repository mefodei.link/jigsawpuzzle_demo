using FancyScrollView;

namespace Game.Code.JigsawPuzzle.Views.PuzzleScrollViews
{
    using Runtime.Services;
    using UnityEngine;
    using System.Collections.Generic;
    using Cysharp.Threading.Tasks;
    using UniGame.Rx.Runtime.Extensions;
    using ViewModels;
    using UniGame.UiSystem.Runtime;
    
    public class PuzzleItemsScrollView : UiCanvasGroupView<JigsawItemsScrollViewModel>,
        IPuzzleArea
    {
        #region inspector
        
        public PuzzleItemScrollControl scrollView;
        public Scroller scroller;

        #endregion

        private List<PuzzleElementViewModel> _scrollItems = new List<PuzzleElementViewModel>();
        private PuzzleElementViewModel _draggedModel;
        private Vector3 _velocity;

        public bool IsScreenPointInArea(Vector2 point,Camera gameCamera)
        {
            return RectTransformUtility.RectangleContainsScreenPoint(RectTransform, point, gameCamera);
        }

        public int GetElementInAreaByScreenPoint(Vector2 point, Camera gameCamera)
        {
            foreach (var cell in scrollView.Cells)
            {
                var contain = RectTransformUtility.RectangleContainsScreenPoint(cell.RectTransform, point,gameCamera);
                if(!contain) continue;
                return cell.Id;
            }

            return -1;
        }

        protected override UniTask OnViewInitialize(JigsawItemsScrollViewModel model)
        {
            UpdateData();

            this.Bind(model.UpdateData, UpdateData)
                .Bind(model.SelectedItem, scrollView.SelectCell)
                .Bind(scrollView.SelectedIndex, model.SetSelectedIndex)
                .Bind(model.Interactable,x => scroller.Draggable = x)
                .Bind(model.FindItemPosition,FindElementIndex);

            return UniTask.CompletedTask;
        }

        public int GetNearestCellIndex(Vector2 point, Camera gameCamera)
        {
            var minDistance = float.MaxValue;
            var selectedIndex = -1;
            
            foreach (var cell in scrollView.Cells)
            {
                var page = cell.Page;
                var position = page.position;
                var screenPosition = gameCamera.WorldToScreenPoint(position);

                var distance = Vector2.Distance(screenPosition, point);
                if(distance > minDistance) continue;

                minDistance = distance;
                selectedIndex = cell.Index;
                selectedIndex = screenPosition.y > point.y ? selectedIndex + 1 : selectedIndex;
            }

            return selectedIndex;
        }

        
        private void FindElementIndex(PuzzleElementViewModel element)
        {
            var gameCamera = Model.Camera;
            var screenPosition = gameCamera.WorldToScreenPoint(element.position);
            var index = GetNearestCellIndex(screenPosition, gameCamera);

            Model.InsertCommand.Execute(new PuzzleElementViewPosition()
            {
                element = element,
                index = index
            });
        }
        
        private void UpdateData()
        {
            _scrollItems.Clear();
            _scrollItems.AddRange(Model.PuzzleItems);

            var selectedCell = Model.SelectedItem.Value;
            scrollView.UpdateData(_scrollItems);
            scrollView.SelectCell(selectedCell);
        }

    }


}
