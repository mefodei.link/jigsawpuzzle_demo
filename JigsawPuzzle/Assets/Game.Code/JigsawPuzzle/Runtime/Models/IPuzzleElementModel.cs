﻿using UniRx;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    public interface IPuzzleElementModel
    {
        int Id { get; }
        
        int Row { get; }
        
        int Column { get; }
        
        IReadOnlyReactiveProperty<PuzzleElementState> State { get; }
    }
}