﻿using System;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    [Serializable]
    public enum JigsawGameState : byte
    {
        Initialize,
        Initialized,
        Active,
        Pause,
        Win,
        PostWin
    }
}