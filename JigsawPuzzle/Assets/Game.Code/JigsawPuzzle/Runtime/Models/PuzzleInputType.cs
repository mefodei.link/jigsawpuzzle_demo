﻿using System;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    [Serializable]
    public enum PuzzleInputType
    {
        None,
        Point,
        Drag,
        Drop,
        Click,
    }
}