using Game.Code.JigsawPuzzle.Runtime.Hints;
using Game.Code.JigsawPuzzle.Runtime.Nodes;
using Game.Code.JigsawPuzzle.Runtime.Services;
using Sirenix.OdinInspector;
using UniModules.UniGame.AddressableTools.Runtime.Attributes;
using UnityEngine.AddressableAssets;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    using System;
    using UniModules.UniGame.Core.Runtime.Rx;
    using UniRx;

    [Serializable]
    public class JigsawModeModel : IJigsawGameModeSettings
    {
        public bool allowLoadProgress = false;
        public bool allowSaveProgress = false;

        [HideLabel]
        [GUIColor(0.2f,0.8f,0.4f)]
        [InlineProperty]
        public JigsawModePuzzleSettings puzzleSettings  = new JigsawModePuzzleSettings();
        
        [BoxGroup("input settings")]     
        public AssetReferenceT<InputServiceSettingsAsset> inputSettings;

        [BoxGroup("hints settings")]   
        public AssetReferenceT<PuzzleHintsSettingsAsset> hintsSettings;

        [BoxGroup("default level")]
        public AssetReferenceT<JigsawLevelAsset> defaultLevel;

        public bool AllowLoadProgress => allowLoadProgress;
        public bool AllowSaveProgress => allowSaveProgress;
        
        public IJigsawModePuzzleSettings ModeSettings => puzzleSettings;
        public AssetReferenceT<JigsawLevelAsset> DefaultLevel => defaultLevel;
        public AssetReferenceT<InputServiceSettingsAsset> InputSettings => inputSettings;
        public AssetReferenceT<PuzzleHintsSettingsAsset> HintSettings => hintsSettings;
    }
}
