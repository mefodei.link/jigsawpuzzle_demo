﻿using System;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    [Serializable]
    public enum PuzzleAreaType : byte
    {
        None,
        DropArea,
        GameArea,
    }
}