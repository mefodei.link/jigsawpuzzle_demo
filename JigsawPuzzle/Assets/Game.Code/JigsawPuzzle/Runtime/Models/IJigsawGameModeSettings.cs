﻿using Game.Code.JigsawPuzzle.Runtime.Hints;
using Game.Code.JigsawPuzzle.Runtime.Nodes;
using Game.Code.JigsawPuzzle.Runtime.Services;
using UniRx;
using UnityEngine.AddressableAssets;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    public interface IJigsawGameModeSettings
    {
        bool AllowLoadProgress { get; }
        
        bool AllowSaveProgress { get; }
        
        IJigsawModePuzzleSettings ModeSettings { get; }
        
        AssetReferenceT<JigsawLevelAsset> DefaultLevel { get; }
        
        AssetReferenceT<InputServiceSettingsAsset> InputSettings { get; }
        
        AssetReferenceT<PuzzleHintsSettingsAsset> HintSettings { get; }
    }
}