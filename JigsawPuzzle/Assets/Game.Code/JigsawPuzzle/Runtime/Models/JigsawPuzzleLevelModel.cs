﻿namespace Game.Code.JigsawPuzzle.Runtime
{
    using System;
    using UnityEngine;
#if ODIN_INSPECTOR
    using Sirenix;
    using Sirenix.OdinInspector;
#endif
    
    [Serializable]
    public class JigsawPuzzleLevelModel
    {
        public const string DefaultLevel = nameof(DefaultLevel);
        public const string GameAreaGroupKey = "game area";
        
        public string id;
        
#if ODIN_INSPECTOR
        [Required]
        [PreviewField(ObjectFieldAlignment.Left,Height = 100)]
#endif
        public Texture2D levelImage;// Will be used as main puzzle image
        public bool generateBackground = true;// Automatically generate puzzle background from the source image
        public int cols = 8;					// Puzzle grid columns number
        public int rows = 8;					// Puzzle rows columns number
    }
    
    
}