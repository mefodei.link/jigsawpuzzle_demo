﻿using System;
using System.Collections.Generic;
using UniModules.UniGame.Core.Runtime.Rx;
using UniRx;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    [Serializable]
    public class JigsawActiveLevelModel : IJigsawActiveLevelModel
    {
        public RecycleReactiveProperty<JigsawPuzzleLevelModel> levelModel =
            new RecycleReactiveProperty<JigsawPuzzleLevelModel>();

        public List<PuzzleElementModel> puzzleItems = new List<PuzzleElementModel>();

        public string Id => levelModel?.Value.id;
        
        public IReadOnlyReactiveProperty<JigsawPuzzleLevelModel> LevelModel => levelModel;
        
        public List<PuzzleElementModel> PuzzleItems => puzzleItems;
    }
}