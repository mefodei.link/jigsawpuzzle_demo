﻿using UnityEngine;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    public interface IJigsawModePuzzleSettings
    {
        bool AdjustWithScreenSpace { get; }
        
        ScreenAnchor PuzzleAreaAnchor { get; }

        Vector2Int Margins { get; }
        
        Vector2Int UiSize { get; }
        
        Vector2Int TopAndBottomMargins { get; }
        
        float PuzzleBackgroundAlpha { get; }
    }
}