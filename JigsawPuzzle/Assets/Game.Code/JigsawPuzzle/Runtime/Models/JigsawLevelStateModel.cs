﻿using System;
using UniModules.UniGame.Core.Runtime.Rx;
using UniRx;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    /// <summary>
    /// current level state
    /// </summary>
    [Serializable]
    public class JigsawLevelStateModel : IJigsawLevelStateModel
    {
        public RecycleReactiveProperty<JigsawGameState> levelState =
            new RecycleReactiveProperty<JigsawGameState>(JigsawGameState.Initialize);

        public IReadOnlyReactiveProperty<JigsawGameState> LevelState => levelState;
    }
}