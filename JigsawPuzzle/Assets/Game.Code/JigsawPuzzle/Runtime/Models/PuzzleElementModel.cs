using UniRx;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    using System;
    using UniModules.UniGame.Core.Runtime.Rx;
    using UnityEngine;
    
    [Serializable]
    public class PuzzleElementModel : IPuzzleElementModel
    {
        #region inspector
        
        public int id;
        public Sprite sprite;
        public int column;
        public int row;
        public Vector3 size;
        public Vector2 screenSize;
        public Vector2 uiSize;
        public Transform itemTransform;
        public PuzzleElement element;
        public PuzzleAreaType area;
        public float zoomDuration = 0.2f;
        
        public RecycleReactiveProperty<Vector3> interactionPoint = new RecycleReactiveProperty<Vector3>();

        [SerializeField]
        public RecycleReactiveProperty<PuzzleElementState> elementState = new(PuzzleElementState.Hidden);

        #endregion

        public int Id => id;
        public int Row => row;
        public int Column => column;

        public IReadOnlyReactiveProperty<PuzzleElementState> State => elementState;
        
        public void ApplyState(PuzzleElementState stateValue) => elementState.SetValueForce(stateValue);
    }
}
