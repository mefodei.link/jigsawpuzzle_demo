﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    [Serializable]
    public class JigsawModePuzzleSettings : IJigsawModePuzzleSettings
    {
        public const string GameAreaGroupKey = "game area settings";
        
        [BoxGroup(GameAreaGroupKey)]
        public bool adjustWithScreenSpace = true;
        
        [BoxGroup(GameAreaGroupKey)]
        [ShowIf(nameof(adjustWithScreenSpace))]
        public ScreenAnchor gameAreaAnchor = ScreenAnchor.Center;
        
        [BoxGroup(GameAreaGroupKey)]
        [ShowIf(nameof(adjustWithScreenSpace))]
        public Vector2Int margins = new Vector2Int(380,0);

        [BoxGroup(GameAreaGroupKey)]
        [ShowIf(nameof(adjustWithScreenSpace))]
        public Vector2Int topAndBottomMargins = new Vector2Int(0,0);
        
        public Vector2Int uiSize = new Vector2Int(200, 200);

        public float puzzleBackgroundAlpha = 0f;
        
        public bool AdjustWithScreenSpace => adjustWithScreenSpace;
        
        public ScreenAnchor PuzzleAreaAnchor => gameAreaAnchor;

        public Vector2Int Margins => margins;
        public Vector2Int UiSize => uiSize;

        public Vector2Int TopAndBottomMargins => topAndBottomMargins;

        public float PuzzleBackgroundAlpha => puzzleBackgroundAlpha;

    }
}