﻿using System;
using UniModules.UniCore.Runtime.Utils;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    [Serializable]
    public struct PuzzleInputData
    {
        public int pieceId;
        public Vector3 worldPosition;
        public PuzzleInputType inputType;
        public PuzzleAreaType areaType;

        public override string ToString()
        {
            return $"ID: {pieceId} | position: {worldPosition} \n input: {inputType.ToStringFromCache()} zone: {areaType.ToStringFromCache()}";
        }
    }
}