﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.Runtime.Hints
{
    [Serializable]
    public class PuzzleHintsSettings
    {
        public const string BackgroundGroup = "background hunt";

        [BoxGroup(BackgroundGroup)]
        [Tooltip("Delay on mouse ")]
        [InlineProperty]
        [HideLabel]
        public BackgroundHintSettings backgroundHint = new BackgroundHintSettings();
    }
}