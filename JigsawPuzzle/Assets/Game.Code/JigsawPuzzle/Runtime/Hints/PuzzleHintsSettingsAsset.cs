namespace Game.Code.JigsawPuzzle.Runtime.Hints
{
    using Services;
    using UniModules.UniGame.SerializableContext.Runtime.Abstract;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Game/Jigsaw/Settings/" + nameof(PuzzleHintsSettingsAsset), fileName = nameof(PuzzleHintsSettingsAsset))]
    public class PuzzleHintsSettingsAsset : ClassValueAsset<PuzzleHintsSettings,PuzzleHintsSettings>
    {

    }
}