﻿using System;
using Game.Code.JigsawPuzzle.Runtime.Models;

namespace Game.Code.JigsawPuzzle.Runtime.Hints
{
    [Serializable]
    public class BackgroundHintSettings
    {
        public float backgroundShowDelay = 3f;
        public PuzzleInputType showBackgroundEvent = PuzzleInputType.Point;
    }
}