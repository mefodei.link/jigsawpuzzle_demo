﻿namespace Game.Code.JigsawPuzzle.Runtime.Nodes
{
    using Sirenix.OdinInspector;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Game/Jigsaw/JigsawLevelAsset",fileName = nameof(JigsawLevelAsset))]
    public class JigsawLevelAsset : ScriptableObject
    {
        [HideLabel]
        [InlineProperty]
        public JigsawPuzzleLevelModel levelModel = new JigsawPuzzleLevelModel()
        {
            id = JigsawPuzzleLevelModel.DefaultLevel
        };
    }
}