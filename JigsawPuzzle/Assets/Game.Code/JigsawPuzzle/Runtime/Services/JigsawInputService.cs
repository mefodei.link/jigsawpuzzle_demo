using UniCore.Runtime.ProfilerTools;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    using UniModules.UniCore.Runtime.Common;
    using UniModules.UniGame.Core.Runtime.Rx;
    using UniRx;
    using Cysharp.Threading.Tasks;
    using Models;
    using UnityEngine;
    using System;
    using UniGame.UniNodes.GameFlow.Runtime;

    [Serializable]
    public class JigsawInputService : GameService, IJigsawInputService
    {
        private readonly InputServiceSettings _settings;
        private readonly IJigsawGameplayStateService _stateService;
        private readonly IJigsawPuzzleElementInfo _infoService;
        private readonly RecycleReactiveProperty<PuzzleInputData> _elementInput;
        private readonly RecycleReactiveProperty<PuzzleAreaType> _elementArea;
        private readonly Camera _gameCamera;

        private PuzzleInputType _previousInput = PuzzleInputType.None;
        private Vector2 _pointPosition;
        private Vector3 _worldPosition;
        private bool _isDragInput;
        private IPuzzleArea _dropArea;
        private IPuzzleArea _gameArea;
        private IPuzzleArea _emptyArea = new EmptyPuzzleArea();
        private float _interactionTime = 0;
        private float _focusTime = 0f;
        private bool _dragLimitsPassed = false;
        private Vector2 _startInteractionPoint;


        private PuzzleInputData _inputData = new PuzzleInputData()
        {
            areaType = PuzzleAreaType.GameArea,
            pieceId = -1
        };

        public JigsawInputService(
            InputServiceSettings settings,
            IJigsawGameplayStateService stateService,
            IJigsawPuzzleElementInfo infoService,
            Camera gameCamera)
        {
            _elementInput = new RecycleReactiveProperty<PuzzleInputData>().AddTo(LifeTime);
            _elementArea = new RecycleReactiveProperty<PuzzleAreaType>().AddTo(LifeTime);

            _settings = settings;

            _stateService = stateService;
            _infoService = infoService;
            _gameCamera = gameCamera;

            ResetInputData();
        }

        public IReadOnlyReactiveProperty<PuzzleInputData> ElementsInput => _elementInput;

        public IReadOnlyReactiveProperty<PuzzleAreaType> ElementAreaInput => _elementArea;

        public IDisposable RegisterArea(IPuzzleArea area, PuzzleAreaType areaType)
        {
            var disposable = Disposable.Empty;
            switch (areaType)
            {
                case PuzzleAreaType.DropArea:
                    _dropArea = area;
                    var dropDisposable = new DisposableAction().AddTo(LifeTime);
                    dropDisposable.Initialize(() => _dropArea = null);
                    disposable = dropDisposable;
                    break;
                case PuzzleAreaType.GameArea:
                    _gameArea = area;
                    var disposableAction = new DisposableAction().AddTo(LifeTime);
                    disposableAction.Initialize(() => _gameArea = null);
                    break;
            }

            return disposable;
        }

        public void ApplyInput(int id, PuzzleInputType inputType)
        {
            _worldPosition = GetWorldPosition(_gameCamera, _pointPosition);

            var areaType = GetZoneType(_pointPosition);
            var area = GetArea(areaType);

            id = id < 0
                ? area.GetElementInAreaByScreenPoint(_pointPosition, _gameCamera)
                : id;

            _inputData.pieceId = id;
            _inputData.worldPosition = _worldPosition;
            _inputData.inputType = inputType;
            _inputData.areaType = areaType;

            NotifyInput(ref _inputData);

            _previousInput = inputType;
        }

        public void NotifyInput(ref PuzzleInputData inputData)
        {
            var inputType = inputData.inputType;

            //GameLog.Log($"INPUT : {inputType}",Color.yellow);

            if (inputType == PuzzleInputType.Drop && _previousInput != PuzzleInputType.Drag)
                return;

            _elementArea.SetValueForce(inputData.areaType);
            _elementInput.SetValueForce(inputData);

            if (inputType != PuzzleInputType.Drag)
                ResetInputData();
        }

        public async UniTask Execute()
        {
            var stateValue = _stateService.LevelState;
            while (LifeTime.IsTerminated == false)
            {
                await UniTask.Yield(PlayerLoopTiming.Update, LifeTime.TokenSource);

                if (stateValue.Value != JigsawGameState.Active) continue;

                if (!IsValidInput())
                {
                    ApplyDropInput();
                    
                    _interactionTime = 0;
                    _focusTime = 0f;
                    _dragLimitsPassed = false;

                    _inputData.pieceId = -1;
                    _inputData.areaType = PuzzleAreaType.None;
                    _inputData.inputType = PuzzleInputType.None;
                    
                    NotifyInput(ref _inputData);
                    
                    continue;
                }
                
                UpdateInput();
            }
        }

        private IPuzzleArea GetArea(PuzzleAreaType areaType)
        {
            var area = areaType switch
            {
                PuzzleAreaType.None => _emptyArea,
                PuzzleAreaType.DropArea when _dropArea != null => _dropArea,
                PuzzleAreaType.GameArea when _gameArea != null => _gameArea,
                _ => _emptyArea
            };
            return area;
        }

        private PuzzleAreaType GetZoneType(Vector2 screenPosition)
        {
            if (_dropArea != null && _dropArea.IsScreenPointInArea(screenPosition, _gameCamera))
                return PuzzleAreaType.DropArea;

            if (_gameArea != null && _gameArea.IsScreenPointInArea(screenPosition, _gameCamera))
                return PuzzleAreaType.GameArea;

            return PuzzleAreaType.None;
        }

        private void UpdateInput()
        {
            _pointPosition = GetPointPosition();
            
            if (Mathf.Approximately(_interactionTime, 0))
                _startInteractionPoint = GetPointPosition();

            var point = GetPointPosition();

            _interactionTime += Time.deltaTime;
            _focusTime += Time.deltaTime;

            var selectedPuzzleId = _infoService.GetCurrentActivePuzzlePiece();
            var areaType = GetZoneType(_pointPosition);

            var xRange = Mathf.Abs(_startInteractionPoint.x - point.x);
            var yRange = Mathf.Abs(_startInteractionPoint.y - point.y);

            if (!_dragLimitsPassed)
            {
                if(_settings.thresholdXRange > 0)
                    _dragLimitsPassed = xRange > _settings.thresholdXRange;
                if(_settings.thresholdYRange > 0)
                    _dragLimitsPassed = _dragLimitsPassed || yRange > _settings.thresholdYRange;
            }

            var isFocusDelay = xRange < _settings.focusResetRange && 
                               yRange < _settings.focusResetRange &&
                               _settings.focusDelay < _focusTime;
            
            _isDragInput = _dragLimitsPassed && _interactionTime > _settings.draggingDelay ||
                           isFocusDelay ||
                           areaType == PuzzleAreaType.GameArea ;
            
            var inputType = _isDragInput ? PuzzleInputType.Drag : PuzzleInputType.Point;

            ApplyInput(selectedPuzzleId, inputType);
        }

        private void ResetInputData()
        {
            _inputData.pieceId = -1;
            _inputData.inputType = PuzzleInputType.None;
            _inputData.areaType = PuzzleAreaType.None;
            _infoService.ResetSelectedPiece();
        }

        private void UpdatePointUp()
        {
            ApplyInput(-1, PuzzleInputType.None);
        }

        private void ApplyDropInput()
        {
            var currentItem = _infoService.GetCurrentActivePuzzlePiece();
            if (currentItem < 0) return;
            ApplyInput(currentItem, PuzzleInputType.Drop);
        }

        private bool IsValidInput()
        {
            return Input.GetMouseButton(0);
        }

        private Vector2 GetPointPosition()
        {
            // For mobile/desktop
            var position = Input.touchCount > 0
                ? Input.GetTouch(0).position
                : (Vector2) Input.mousePosition;

            return position;
        }

        // Get current pointer(mouse or single touch) position  
        private Vector3 GetWorldPosition(Camera targetCamera, Vector3 point)
        {
            return targetCamera.ScreenToWorldPoint(point);
        }
    }
}