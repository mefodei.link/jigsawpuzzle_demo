﻿using UniRx;

namespace Game.Code.JigsawPuzzle.Runtime
{
    public interface ILevelProgress
    {
        IReadOnlyReactiveProperty<float> Progress { get; }
    }
}