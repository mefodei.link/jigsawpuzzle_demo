﻿using JigsawPuzzle_Kit._SystemScripts;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using Cysharp.Threading.Tasks;
    using Game.Code.JigsawPuzzle.Runtime.Models;
    using UniCore.Runtime.ProfilerTools;
    using UniGame.UniNodes.GameFlow.Runtime;
    using UniRx;

    
    [Serializable]
    public class PuzzleLevelProgressStorageService : GameService, IPuzzleLevelProgressStorageService
    {
        private readonly List<IPuzzleProgressHandler> _loadServices;
        private readonly IJigsawPuzzleProgressProvider _progressProvider;

        public PuzzleLevelProgressStorageService(
            IEnumerable<IPuzzleProgressHandler> loadServices,
            IJigsawPuzzleProgressProvider progressProvider)
        {
            _loadServices = new List<IPuzzleProgressHandler>(loadServices);
            _progressProvider = progressProvider;
        }

        public void Register(IPuzzleProgressHandler handler)
        {
            _loadServices.Add(handler);
        }
        
        public async UniTask SaveProgress(IJigsawActiveLevelModel levelModel)
        {
            await _progressProvider.SaveProgress(levelModel);
        }

        public async UniTask<PuzzleSaveData> LoadProgress(string level)
        {
            var progress = await _progressProvider.LoadProgress(level);
            if (progress == null) return null;
            
            foreach (var service in _loadServices)
            {
                var result = await service.ApplyProgress(progress);
                if (result.Complete) continue;
                GameLog.LogError($"Try load puzzle level progress FAILED \n{result.Error} {result.Exception?.Message}");
                return progress;
            }

            return progress;
        }


    }

    public struct LoadProcessResult
    {
        public bool Complete;
        public string Error;
        public Exception Exception;
    }
}