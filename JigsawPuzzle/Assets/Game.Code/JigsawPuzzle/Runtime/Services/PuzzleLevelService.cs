using UniModules.UniCore.Runtime.DataFlow;
using UniModules.UniCore.Runtime.Extension;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using Cysharp.Threading.Tasks;
    using Models;
    using JigsawPuzzle_Kit._SystemScripts;
    using UniCore.Runtime.ProfilerTools;
    using UniGame.Core.Runtime.Extension;
    using UniGame.UniNodes.GameFlow.Runtime;
    using UniModules.UniGame.Core.Runtime.Rx;
    using UniRx;
    using UnityEngine;
    
    [Serializable]
    public class PuzzleLevelService : GameService, IPuzzleLevelService
    {
        private readonly IJigsawModePuzzleSettings _puzzleSettings;
        private readonly Camera _gameCamera;
        private readonly RuntimeGeneration _puzzleGeneration;
        private JigsawActiveLevelModel _activeLevelModel;
        private HashSet<int> _groupsCache = new();
        private HashSet<int> _foundCache = new();
        private RecycleReactiveProperty<PuzzleState> _puzzleState ;
        private PuzzleController _puzzleController;
        private GameController _controller;
        private RecycleReactiveProperty<IJigsawActiveLevelModel> _levelModelValue;
        private RecycleReactiveProperty<Vector2> _puzzleScreenSize;
        private RecycleReactiveProperty<Sprite> _image;
        private RecycleReactiveProperty<IPuzzleElementModel> _puzzleElementState;
        private LifeTimeDefinition _puzzleItemsLifeTime = new LifeTimeDefinition();
        private Dictionary<int, PuzzleElementModel> _puzzleItems = new(64);

        public PuzzleLevelService(
            IJigsawModePuzzleSettings puzzleSettings,
            Camera gameCamera,
            RuntimeGeneration puzzleGeneration)
        {
            _puzzleItemsLifeTime.AddTo(LifeTime);
            _image = new RecycleReactiveProperty<Sprite>().AddTo(LifeTime);
            _puzzleScreenSize = new RecycleReactiveProperty<Vector2>().AddTo(LifeTime);
            _puzzleState = new RecycleReactiveProperty<PuzzleState>().AddTo(LifeTime);
            _levelModelValue = new RecycleReactiveProperty<IJigsawActiveLevelModel>().AddTo(LifeTime);
            _puzzleElementState = new RecycleReactiveProperty<IPuzzleElementModel>().AddTo(LifeTime);
            
            _puzzleSettings = puzzleSettings;
            _gameCamera = gameCamera;
            _puzzleGeneration = puzzleGeneration;
        }

        #region public properties

        public IEnumerable<IPuzzleElementModel> Elements => _puzzleItems.Values;
        public IObservable<IPuzzleElementModel> ElementStateChanged => _puzzleElementState;
        public IReadOnlyReactiveProperty<IJigsawActiveLevelModel> Level => _levelModelValue;
        public IReadOnlyReactiveProperty<Vector2> PuzzleScreenSize => _puzzleScreenSize;
        public IReadOnlyReactiveProperty<Sprite> LevelTexture => _image;
        public IReadOnlyReactiveProperty<PuzzleState> PuzzleState => _puzzleState;

        public float Transparency => _controller == null 
            ? 0f : _controller.Transparency;

        #endregion

       
        public int GetPuzzleItemIdByPosition(Vector3 pointerPosition)
        {
            return _puzzleController.GetPointedPieceId(pointerPosition, true);
        }
        
        public int GetCurrentActivePuzzlePiece()
        {
            return _puzzleController.GetCurrentPuzzleIndex();
        }

        public void ResetSelectedPiece()
        {
            _puzzleController?.ResetSelectedPiece();
        }
        
        public void ApplyBackgroundTransparency(float transparency)
        {
            if (_puzzleGeneration.IsReady == false || _controller == null)
                return;
            
            _controller.ApplyBackgroundTransparency(transparency);
        }
        
        public async UniTask<IJigsawActiveLevelModel> CreateLevel(JigsawPuzzleLevelModel puzzleLevelModel)
        {
            _groupsCache.Clear();
            _foundCache.Clear();
            
            _activeLevelModel = new JigsawActiveLevelModel()
            {
                levelModel = new RecycleReactiveProperty<JigsawPuzzleLevelModel>(puzzleLevelModel)
            };
            
            var image = puzzleLevelModel.levelImage;
            
#if UNITY_EDITOR
            if (image == null)
            {
                GameLog.LogError($"JIGSAW PUZZLE IMAGE IS NULL");
            }
#endif
            _puzzleGeneration.image = puzzleLevelModel.levelImage;
            _puzzleGeneration.generateBackground = puzzleLevelModel.generateBackground;
            _puzzleGeneration.SetCols(puzzleLevelModel.cols);
            _puzzleGeneration.SetRows(puzzleLevelModel.rows);
            _puzzleGeneration.GeneratePuzzle();

            await UniTask.Yield();

            _controller = _puzzleGeneration.gameController;
            _puzzleController = _controller.puzzle;
            _puzzleController.ResetSelectedPiece();
            _puzzleController.StartPuzzle(false);

            if (_puzzleGeneration.gameController)
            {
                var gameController = _puzzleGeneration.gameController;
                gameController.PrepareBackground(gameController.background);
                //gameController.AdjustBackground();
                gameController.AlignCamera();
            }

            if (_puzzleSettings.AdjustWithScreenSpace)
            {
                _puzzleController.FitToScreen(_gameCamera,
                    _puzzleSettings.PuzzleAreaAnchor,
                    _puzzleSettings.Margins,
                    _puzzleSettings.TopAndBottomMargins);
            }

            _image.Value = _puzzleController.puzzleSprite;
            _puzzleScreenSize.Value = _puzzleController.GetPuzzleScreenSize(_gameCamera);
            
            await UniTask.WaitWhile(() => _puzzleGeneration.IsReady == false);

            CollectPuzzleItems(_puzzleController);
            
            _activeLevelModel.levelModel.Value = puzzleLevelModel;
            _activeLevelModel.puzzleItems.AddRange(_puzzleItems.Values);

            _levelModelValue.Value = _activeLevelModel;
            
            ApplyBackgroundTransparency(_puzzleSettings.PuzzleBackgroundAlpha);
            
            return _activeLevelModel;
        }

        public PuzzleState ApplyInput(ref PuzzleInputData inputData)
        {
            var pieceId = inputData.pieceId;
            var isDrag = inputData.inputType == PuzzleInputType.Drag;
            var puzzlePosition = inputData.worldPosition;
            var puzzleState = _puzzleController.ProcessPuzzle(
                puzzlePosition, isDrag, 0,pieceId);
            
            if (inputData.inputType != PuzzleInputType.Drag)
                _puzzleController.ResetSelectedPiece();

            _puzzleState.Value = puzzleState;
            
            return puzzleState;
        }

        private PuzzleElementModel GetPuzzleItem(int id)
        {
            _puzzleItems.TryGetValue(id, out var element);
            return element;
        }
        
        public bool IsInAssemblyGroup(int id)
        {
            if (_groupsCache.Contains(id)) return true;
            var element = GetPuzzleItem(id);
            var isInGroup = IsInAssemblyGroup(element);
            if (isInGroup) _groupsCache.Add(id);
            return isInGroup;
        }
        
        public bool IsAssembled(int id)
        {
            if (_foundCache.Contains(id)) return true;
            var found = _puzzleController.IsFoundElement(id);
            if (found) _foundCache.Add(id);
            return found;
        }
        
        public UniTask<LoadProcessResult> ApplyProgress(PuzzleSaveData data)
        {
            _puzzleController.LoadProgress(data);
            
            foreach (var item in data.elements)
            {
                var model = GetPuzzleItem(item.id);
                var state = item.state;
                model.ApplyState(state);
            }
            
            var result = new LoadProcessResult() {Complete = true,};
            _levelModelValue.SetValueForce(_activeLevelModel);
            
            return UniTask.FromResult(result);
        }
        
        private bool IsInAssemblyGroup(GameObject element)
        {
            var elementObject = element;
            var elementGroup = PuzzlePieceGroup
                .GetGroupObjectIfPossible(elementObject);
            return elementObject != elementGroup;
        }
        
        private bool IsInAssemblyGroup(PuzzleElementModel element)
        {
            return IsInAssemblyGroup(element.itemTransform.gameObject);
        }

        private void CollectPuzzleItems(PuzzleController puzzleController)
        {
            _puzzleItemsLifeTime.Release();
            _puzzleItems.Clear();
            
            var pieces = puzzleController.pieces;
            var gridData = puzzleController.puzzleGrid;
            var itemsCount = pieces.Length;
            
            for (var i = 0; i < itemsCount; i++)
            {
                var element = gridData[i];
                var item = pieces[i];
                var itemModel = CreateFromPuzzlePiece(item, element);
                
                itemModel.elementState
                    .Subscribe(x => _puzzleElementState.SetValueForce(itemModel))
                    .AddTo(_puzzleItemsLifeTime);

                _puzzleItems[element.id] = itemModel;
            }
        }

        private PuzzleElementModel CreateFromPuzzlePiece(PuzzlePiece item,PuzzleElement puzzleElement)
        {
            var state = GetPuzzleItemState(item);
            
            var puzzleItem = new PuzzleElementModel()
            {
                id = puzzleElement.id,
                column = puzzleElement.column,
                row = puzzleElement.row,
                sprite = puzzleElement.sprite,
                itemTransform = puzzleElement.view.transform,
                size = item.size,
                screenSize = item.size.WorldSizeToScreenSize(_gameCamera),
                uiSize = _puzzleSettings.UiSize,
                element = puzzleElement,
                elementState = new RecycleReactiveProperty<PuzzleElementState>(state),
            };

            return puzzleItem;
        }
        
        private PuzzleElementState GetPuzzleItemState(PuzzlePiece item)
        {
            var transform = item.transform;
            var view = transform.gameObject;
            
            var state = PuzzleElementState.Hidden;
            state = IsInAssemblyGroup(view) 
                ? PuzzleElementState.InGroup : state;
            state = IsAssembled(item.id) 
                ? PuzzleElementState.Assembled : state;

            return state;
        }


    }
}
