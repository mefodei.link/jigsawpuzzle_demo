namespace Game.Code.JigsawPuzzle.Runtime
{
    using Services;
    using UniRx;
    using System;
    using UniModules.UniGame.Core.Runtime.Rx;
    using Cysharp.Threading.Tasks;
    using Abstract;
    using Models;
    using UniGame.UniNodes.GameFlow.Runtime;

    [Serializable]
    public class JigsawGameplayService : GameService, IJigsawPuzzleGameService
    {
        private readonly IJigsawGameModeSettings _modeModel;
        private readonly IPuzzleLevelService _levelService;
        private readonly IPuzzleLevelProgressStorageService _progressService;
        
        private RecycleReactiveProperty<JigsawActiveLevelModel> _activeLevel;
        private JigsawActiveLevelModel _activeLevelModel;
        
        private RecycleReactiveProperty<bool> _levelGenerated = new RecycleReactiveProperty<bool>(false);

        public JigsawGameplayService(
            IJigsawGameModeSettings modeModel,
            IPuzzleLevelService levelService,
            IPuzzleLevelProgressStorageService progressService)
        {
            _activeLevelModel = new JigsawActiveLevelModel();
            _activeLevel = new RecycleReactiveProperty<JigsawActiveLevelModel>().AddTo(LifeTime);
            _activeLevel.Value = _activeLevelModel;
            _modeModel = modeModel;
            _levelService = levelService;
            _progressService = progressService;
        }

        #region public methods

        public IReadOnlyReactiveProperty<IJigsawActiveLevelModel> ActiveLevel => _levelService.Level;
        public IReadOnlyReactiveProperty<bool> LevelGenerated => _levelGenerated;

        public async UniTask<IJigsawActiveLevelModel> CreatePuzzle(
            JigsawPuzzleLevelModel puzzleLevelModel, 
            bool restoreState)
        {
            restoreState = restoreState && _modeModel.AllowLoadProgress;
            var model = await CreateLevel(puzzleLevelModel, restoreState);
            _levelGenerated.Value = model != null;
            return model;
        }

        public async UniTask SaveProgress()
        {
            if (_modeModel.AllowSaveProgress == false) return;
            await _progressService.SaveProgress(_levelService.Level.Value);
        }

        public async UniTask LoadProgress()
        {
            await LoadProgress(_levelService.Level.Value.Id);
        }
        
        #endregion

        private async UniTask LoadProgress(string levelID)
        {
            if(_levelGenerated.Value == false) return;
            if (_modeModel.AllowLoadProgress == false) return;
            await _progressService.LoadProgress(levelID);
        }
        
        private async UniTask<IJigsawActiveLevelModel> CreateLevel(
            JigsawPuzzleLevelModel puzzleLevelModel,
            bool restoreState)
        {
            var levelModel = await _levelService.CreateLevel(puzzleLevelModel);
            
            if (!restoreState) return levelModel;
            
            await UniTask.Delay(TimeSpan.FromMilliseconds(1000));
            
            await LoadProgress(levelModel.Id);
            
            return levelModel;
        }
        
    }
}
