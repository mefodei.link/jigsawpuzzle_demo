﻿using Cysharp.Threading.Tasks;
using Game.Code.JigsawPuzzle.Runtime.Models;
using UniGame.AddressableTools.Runtime;
using UniGame.Core.Runtime;
using UniGame.Core.Runtime.Extension;
using UniModules.UniGame.SerializableContext.Runtime.Abstract;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    [CreateAssetMenu(menuName = "Game/Settings/" + nameof(JigsawModeSettingsAsset), fileName = nameof(JigsawModeSettingsAsset))]
    public class JigsawModeSettingsAsset : ClassValueAsset<JigsawModeModel,IJigsawGameModeSettings>
    {
        protected sealed override async UniTask<IJigsawGameModeSettings> OnRegisterAsync(
            JigsawModeModel value,
            IContext context)
        {
            var hints = await value.HintSettings
                .LoadAssetInstanceTaskAsync(LifeTime, true)
                .ToSharedInstanceAsync(LifeTime);
            
            await hints.RegisterAsync(context);
            return value;
        }
    }
}