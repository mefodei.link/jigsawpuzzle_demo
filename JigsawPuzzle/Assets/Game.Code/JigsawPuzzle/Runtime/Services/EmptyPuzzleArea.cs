﻿using UnityEngine;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    public class EmptyPuzzleArea : IPuzzleArea
    {
        public bool IsScreenPointInArea(Vector2 point, Camera gameCamera)
        {
            return false;
        }

        public int GetElementInAreaByScreenPoint(Vector2 point, Camera gameCamera)
        {
            return -1;
        }
    }
}