﻿namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    using UniModules.UniGame.SerializableContext.Runtime.Abstract;
    using UnityEngine;
    
    [CreateAssetMenu(menuName = "Game/Settings/" + nameof(InputServiceSettingsAsset), fileName = nameof(InputServiceSettingsAsset))]
    public class InputServiceSettingsAsset : ClassValueAsset<InputServiceSettings,InputServiceSettings>
    {

    }
}