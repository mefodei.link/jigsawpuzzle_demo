﻿using DG.Tweening;
using UniCore.Runtime.ProfilerTools;
using UniModules.UniGame.DoTweenRoutines.Runtime;

namespace Game.Code.JigsawPuzzle.Runtime
{
    using System;
    using Services;
    using Models;
    using UniGame.Core.Runtime;
    using UniModules.UniCore.Runtime.DataFlow;
    using UnityEngine;
    
    [Serializable]
    public class PuzzleElementBehaviour : IPuzzleElementBehaviour
    {
        private readonly LifeTimeDefinition _lifeTime = new LifeTimeDefinition();
        private readonly PuzzleElementModel _model;
        private readonly IJigsawPuzzleElementInfo _elementInfo;
        private readonly IPuzzleLevelService _levelService;
        private readonly LifeTimeDefinition _tweenLifetime;
        private float _zoomScale;

        private GameObject _modelGameObject;
        
        public PuzzleElementBehaviour(
            PuzzleElementModel item,
            IPuzzleLevelService levelService)
        {
            _model = item;
            _elementInfo = levelService;
            _levelService = levelService;
            _modelGameObject = _model.itemTransform.gameObject;
            _tweenLifetime = new LifeTimeDefinition().AddTo(_lifeTime);
            
            var screenSize = _model.screenSize;
            var uiSize = _model.uiSize;
            var scale = uiSize / screenSize;
            _zoomScale = scale.x;

            ApplyState(_model.State.Value);
        }
        
        public int Id => _model.id;
        
        public ILifeTime LifeTime => _lifeTime;
        
        public PuzzleElementState State => _model.State.Value;

        public PuzzleElementState ApplyState(PuzzleElementState state)
        {
            return ApplyState(state, false);
        }

        public PuzzleElementState ApplyState(PuzzleElementState state,bool force)
        {
            var isInGroup = _elementInfo.IsInAssemblyGroup(Id);

            state = state == PuzzleElementState.Hidden && isInGroup
                ? PuzzleElementState.InGroup
                : state;

            state = state == PuzzleElementState.InGame && isInGroup
                ? PuzzleElementState.InGroup
                : state;
            
            var isFound = _elementInfo.IsAssembled(Id);
            state = isFound ? PuzzleElementState.Assembled : state;
            
            switch (state)
            {
                case PuzzleElementState.Dragging:
                    state = ApplyDraggingState(state);
                    break;
                case PuzzleElementState.Hidden:
                    state = ApplyHiddenState(state,force);
                    break;
                case PuzzleElementState.InGame:
                    state = ApplyInGameState(state);
                    break;
                case PuzzleElementState.Assembled:
                    state = ApplyAssembledState(state);
                    break;
                case PuzzleElementState.ReadyToDrop:
                    state = ApplyReadyToDropState(state);
                    break;
                case PuzzleElementState.InGroup:
                    state = ApplyInGroupState(state);
                    break;
            }
            
            if(state != PuzzleElementState.None)
                _model.ApplyState(state);
            
            return _model.State.Value;
        }

        public PuzzleState ApplyInput(ref PuzzleInputData inputData)
        {
#if UNITY_EDITOR
            if (inputData.pieceId != Id)
            {
                Debug.LogError($"WRONG TARGET INPUT");
                return PuzzleState.None;
            }  
#endif
            _model.interactionPoint.Value = inputData.worldPosition;
            _model.area = inputData.areaType;

            inputData = ref ProcessInput(ref inputData);
            
            var state = UpdateState(ref inputData);
            var puzzleState = PuzzleState.None;

            //GameLog.Log($"state: {State} input: {inputData}",Color.green);
            // if (state == PuzzleElementState.Hidden)
            //     return puzzleState;
            
            puzzleState = _levelService.ApplyInput(ref inputData);

            OnPostInput(ref inputData, puzzleState);
            
            return puzzleState;
        }

        private PuzzleState OnPostInput(ref PuzzleInputData inputData, PuzzleState state)
        {
            var isAssembled = _elementInfo.IsAssembled(Id);
            if (isAssembled && state != PuzzleState.PuzzleAssembled)
            {
                ApplyState(PuzzleElementState.Assembled);
                return state;
            }

            if (State == PuzzleElementState.Hidden)
            {
                var transform = _modelGameObject.transform;
                var positionValue =  50 * (Id + 1);
                var position = new Vector3(positionValue, positionValue, transform.position.z);
                transform.position = position;
            }
            
            return state;
        }

        private ref PuzzleInputData ProcessInput(ref PuzzleInputData inputData)
        {
            switch (inputData.inputType)
            {
                case PuzzleInputType.Point:
                case PuzzleInputType.Drag:
                    inputData = ref UpdatePuzzleInteraction(ref inputData);
                    break;
                case PuzzleInputType.Drop:
                    inputData = ref  ApplyDropInteraction(ref inputData);
                    break;
            }

            return ref inputData;
        }
        
        private PuzzleElementState UpdateState(ref PuzzleInputData inputData)
        {
            var inputType = inputData.inputType;
            var area = inputData.areaType;
            var state = PuzzleElementState.None;

            switch (area)
            {
                case PuzzleAreaType.None:
                    break;
                case PuzzleAreaType.DropArea:
                    if (inputData.inputType == PuzzleInputType.Drag)
                    {
                        state = ApplyState(PuzzleElementState.ReadyToDrop);
                        break;
                    }
                    state = ApplyState(PuzzleElementState.Hidden);
                    break;
                case PuzzleAreaType.GameArea:
                    if (inputType == PuzzleInputType.Drag)
                    {
                        state = ApplyState(PuzzleElementState.Dragging);
                        break;
                    }
                    state = ApplyState(PuzzleElementState.InGame);
                    break;
            }
            
            return state;
        }

        private ref PuzzleInputData ApplyDropInteraction(ref PuzzleInputData inputData)
        {
            if (inputData.areaType == PuzzleAreaType.GameArea)
            {
                return ref inputData;
            }

            if (inputData.areaType == PuzzleAreaType.None || 
                _elementInfo.IsInAssemblyGroup(Id))
            {
                inputData.worldPosition = Vector3.zero;
                inputData.inputType = PuzzleInputType.Drag;
                inputData.areaType = PuzzleAreaType.GameArea;
                return ref inputData;
            }

            return ref inputData;
        }
        
        private ref PuzzleInputData UpdatePuzzleInteraction(ref PuzzleInputData inputData)
        {
            return ref inputData;
        }

        private PuzzleElementState ApplyInGameState(PuzzleElementState state)
        {
            if (State == PuzzleElementState.InGame) 
                return PuzzleElementState.None;
            
            _modelGameObject.transform.localScale = Vector3.one;
            _modelGameObject.SetActive(true);
            
            _tweenLifetime.Release();
            _modelGameObject.transform
                .DOScale(1, _model.zoomDuration)
                .Play()
                .AddTo(_tweenLifetime);
            
            return PuzzleElementState.InGame;
        }
        
        private PuzzleElementState ApplyReadyToDropState(PuzzleElementState state)
        {
            _tweenLifetime.Release();
            _modelGameObject.transform
                .DOScale(_zoomScale, _model.zoomDuration)
                .Play()
                .AddTo(_tweenLifetime);
         
            _modelGameObject.SetActive(true);
            return PuzzleElementState.ReadyToDrop;
        }

        private PuzzleElementState ApplyDraggingState(PuzzleElementState state)
        {
            if (State == PuzzleElementState.Dragging) 
                return PuzzleElementState.None;
            
            _modelGameObject.transform
                .DOScale(1, 0.2f)
                .Play()
                .AddTo(_tweenLifetime);
            
            _modelGameObject.SetActive(true);
            return PuzzleElementState.Dragging;
        }
        
        private PuzzleElementState ApplyAssembledState(PuzzleElementState state)
        {
            _modelGameObject.SetActive(true);
            return PuzzleElementState.Assembled;
        }
        
        private PuzzleElementState ApplyInGroupState(PuzzleElementState state)
        {
            _modelGameObject.SetActive(true);
            return PuzzleElementState.InGroup;
        }
        
        private PuzzleElementState ApplyHiddenState(PuzzleElementState state,bool force)
        {
            if (!force && State == PuzzleElementState.Hidden) 
                return PuzzleElementState.None;
            
            _tweenLifetime.Release();
            _modelGameObject.transform.localScale = new Vector2(_zoomScale,_zoomScale);
            _modelGameObject.SetActive(false);
            
            return PuzzleElementState.Hidden;
        }

        public void Dispose() => _lifeTime.Terminate();

    }
}