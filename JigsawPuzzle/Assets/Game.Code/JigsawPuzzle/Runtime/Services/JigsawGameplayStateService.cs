﻿using System;
using Game.Code.JigsawPuzzle.Runtime.Models;
using UniGame.UniNodes.GameFlow.Runtime;
using UniRx;

namespace Game.Code.JigsawPuzzle.Runtime
{
    [Serializable]
    public class JigsawGameplayStateService : GameService, IJigsawGameplayStateService
    {
        private readonly JigsawLevelStateModel _model;

        public JigsawGameplayStateService(JigsawLevelStateModel model)
        {
            _model = model;
            _model.levelState.Value = JigsawGameState.Initialize;
        }

        public IReadOnlyReactiveProperty<JigsawGameState> LevelState => _model.levelState;

        public bool ApplyState(JigsawGameState state)
        {

            switch (state)
            {
                case JigsawGameState.Initialize:
                    SetActiveState(state);
                    break;
                case JigsawGameState.Initialized:
                    SetActiveState(state);
                    ApplyState(JigsawGameState.Active);
                    break;
                case JigsawGameState.Active:
                    SetActiveState(state);
                    break;
                case JigsawGameState.Pause:
                    SetActiveState(state);
                    break;
                case JigsawGameState.Win:
                    SetActiveState(state);
                    break;
                case JigsawGameState.PostWin:
                    SetActiveState(state);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
            
            return true;
        }

        private void SetActiveState(JigsawGameState state)
        {
            _model.levelState.Value = state;
        }
        
    }
}