﻿using System;
using Game.Code.JigsawPuzzle.Runtime.Models;
using UniGame.Rx.Runtime.Extensions;
using UniGame.UniNodes.GameFlow.Runtime;
using UniRx;

namespace Game.Code.JigsawPuzzle.Runtime
{
    [Serializable]
    public class JigsawLevelProgressService : GameService, ILevelProgress
    {
        private readonly IJigsawActiveLevelModel _level;
        private readonly ReactiveProperty<float> _progress = new ReactiveProperty<float>(0);

        public JigsawLevelProgressService(IJigsawActiveLevelModel level)
        {
            _level = level;
            Bind();
        }

        public IReadOnlyReactiveProperty<float> Progress => _progress;

        private void Bind()
        {
            foreach (var puzzleItem in _level.PuzzleItems)
                this.Bind(puzzleItem.State,UpdateProgress);
        }
        
        private void UpdateProgress()
        {
            var progressValue = 0f;
            var elementsCount = _level.PuzzleItems.Count;
            if (elementsCount == 0)
                return;
            
            var elements = _level.PuzzleItems;
            
            foreach (var element in elements)
            {
                if (element.State.Value is PuzzleElementState.Assembled or PuzzleElementState.InGroup)
                    progressValue++;
            }

            progressValue /= elementsCount;
            _progress.Value = progressValue;
        }
    }
}