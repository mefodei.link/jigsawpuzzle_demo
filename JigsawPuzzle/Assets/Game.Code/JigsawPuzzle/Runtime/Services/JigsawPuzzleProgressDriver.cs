using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using UniCore.Runtime.ProfilerTools;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    using System;
    using Models;
    using JigsawPuzzle_Kit._SystemScripts;
    
    [Serializable]
    public class JigsawPuzzleProgressDriver : IJigsawPuzzleProgressProvider
    {
        public PuzzleSaveData Create(IJigsawActiveLevelModel level)
        {
            var levelId = level.Id;
            var puzzleSaveData = new PuzzleSaveData();
            puzzleSaveData.levelId = levelId;

            foreach (var puzzleItem in level.PuzzleItems)
            {
                var state = puzzleItem.State.Value;
                var position = puzzleItem.itemTransform.position;
                var saveElement = new PuzzleElementSaveData()
                {
                    angle = 0,
                    id = puzzleItem.id,
                    movable = state != PuzzleElementState.Assembled,
                    position = position,
                    state = state
                };
                puzzleSaveData.elements.Add(saveElement);
            }
            
            return puzzleSaveData;
        }

        public async UniTask<PuzzleSaveData> SaveProgress(IJigsawActiveLevelModel level)
        {
            var progress = Create(level);
            progress = await SaveProgress(progress);
            return progress;
        }
        
        public UniTask<PuzzleSaveData> SaveProgress(PuzzleSaveData progress)
        {
            var saveData = JsonConvert.SerializeObject(progress);
            
            GameLog.Log(saveData);
            
            PlayerPrefs.SetString(progress.levelId,saveData);
            
            PlayerPrefs.Save();
            
            return UniTask.FromResult<PuzzleSaveData>(progress);
        }

        public UniTask<PuzzleSaveData> LoadProgress(string levelId)
        {
            if (!PlayerPrefs.HasKey(levelId))
            {
                GameLog.Log($"No saved data found for: <i> {levelId} </i>",Color.yellow);
                return UniTask.FromResult<PuzzleSaveData>(null);
            }

            PuzzleSaveData data = null;
            try
            {
                var savedDataString = PlayerPrefs.GetString(levelId);
                data = JsonConvert.DeserializeObject<PuzzleSaveData>(savedDataString);
            }
            catch (Exception e)
            {
                GameLog.LogError($"Can't restore progress on level {levelId} : \n {e.Message}");
            }

            return UniTask.FromResult<PuzzleSaveData>(data);
        }

        public bool ResetProgress(string levelId)
        {
            if (!PlayerPrefs.HasKey(levelId))
                return false;
            PlayerPrefs.DeleteKey(levelId);
            PlayerPrefs.Save();
            return true;
        }

    }
}
