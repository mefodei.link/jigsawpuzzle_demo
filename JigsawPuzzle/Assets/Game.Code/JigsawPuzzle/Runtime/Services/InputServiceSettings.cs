﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    using System;
    using Cysharp.Threading.Tasks;
    using UniGame.Core.Runtime;

    [Serializable]
    public class InputServiceSettings
    {
        private const string FocusGroup = "focus";
        private const string ThresholdGroup = "threshold";

        [BoxGroup(FocusGroup)]
        [Tooltip("focus delay before dragging if focus reset range not passed")]
        public float focusDelay = 0.4f;
        [BoxGroup(FocusGroup)]
        public int focusResetRange = 10;
        
        [BoxGroup(ThresholdGroup)]
        [Tooltip("delay between touch and drag registration")]
        public float draggingDelay = 0.2f;
        [BoxGroup(ThresholdGroup)]
        [Tooltip("value more than 0 enable threshold check")]
        public float thresholdXRange = 10f;
        [BoxGroup(ThresholdGroup)]
        [Tooltip("value more than 0 enable threshold check")]
        public float thresholdYRange = -1f;
        [BoxGroup(ThresholdGroup)]
        [Tooltip("click detection time")]
        public float clickThreshold = 0.1f;
    }
}