﻿namespace Game.Code.JigsawPuzzle.Runtime
{
    using Cysharp.Threading.Tasks;
    using JigsawPuzzle_Kit._SystemScripts;
    using UniGame.UniNodes.GameFlow.Runtime;
    using System;
    using System.Collections.Generic;
    using Models;
    using Services;
    using UniModules.UniCore.Runtime.DataFlow;

    [Serializable]
    public class PuzzleElementsBehaviourServiceService : GameService, 
        IElementsBehaviourService
    {
        private readonly IPuzzleLevelService _levelService;
        private Dictionary<int, IPuzzleElementBehaviour> _behavioursMap = new Dictionary<int, IPuzzleElementBehaviour>();
        private LifeTimeDefinition _levelTimeLine = new LifeTimeDefinition();

        public PuzzleElementsBehaviourServiceService(
            IPuzzleLevelService levelService)
        {
            _levelService = levelService;
            _levelTimeLine.AddTo(LifeTime);
        }

        public UniTask BindToLevel(IJigsawActiveLevelModel activeLevel)
        {
            _levelTimeLine.Release();
            _behavioursMap.Clear();

            var puzzleItems = activeLevel.PuzzleItems;
            foreach (var model in puzzleItems)
            {
                var behaviour = new PuzzleElementBehaviour(model,_levelService)
                    .AddTo(_levelTimeLine);
                behaviour.ApplyState(model.State.Value,true);
                _behavioursMap[model.id] = behaviour;
            }
            
            UpdateAllStates();
            
            return UniTask.CompletedTask;
        }

        
        public PuzzleState ApplyInputToPuzzle(PuzzleInputData inputData)
        {
            var element = GetElementById(inputData.pieceId);
            if (element == null || element.State == PuzzleElementState.Assembled) return PuzzleState.None;

            return element.ApplyInput(ref inputData);
        }

        public void UpdateAllStates()
        {
            foreach (var behaviour in _behavioursMap)
            {
                var value = behaviour.Value;
                value.ApplyState(value.State);
            }
        }
        
        #region private methods

        private IPuzzleElementBehaviour GetElementById(int id)
        {
            _behavioursMap.TryGetValue(id, out var model);
            return model;
        }

        #endregion

    }
}