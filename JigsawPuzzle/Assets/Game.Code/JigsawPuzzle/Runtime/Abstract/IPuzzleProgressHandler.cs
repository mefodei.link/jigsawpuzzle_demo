﻿using Cysharp.Threading.Tasks;
using JigsawPuzzle_Kit._SystemScripts;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    public interface IPuzzleProgressHandler
    {
        UniTask<LoadProcessResult> ApplyProgress(PuzzleSaveData data);
    }
}