﻿using Cysharp.Threading.Tasks;
using Game.Code.JigsawPuzzle.Runtime.Models;
using JigsawPuzzle_Kit._SystemScripts;
using UniGame.Core.Runtime;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    public interface IJigsawPuzzleProgressProvider : IFactory<IJigsawActiveLevelModel, PuzzleSaveData>
    {
        UniTask<PuzzleSaveData> SaveProgress(IJigsawActiveLevelModel level);
        UniTask<PuzzleSaveData> SaveProgress(PuzzleSaveData progress);
        UniTask<PuzzleSaveData> LoadProgress(string levelId);
        bool ResetProgress(string levelId);
    }
}