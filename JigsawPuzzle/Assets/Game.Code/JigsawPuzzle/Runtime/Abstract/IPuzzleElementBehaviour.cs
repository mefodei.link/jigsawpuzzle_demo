﻿using System;
using Game.Code.JigsawPuzzle.Runtime.Models;
using UniGame.Core.Runtime;

namespace Game.Code.JigsawPuzzle.Runtime
{
    public interface IPuzzleElementBehaviour : IDisposable, ILifeTimeContext
    {

        public PuzzleElementState State { get; }

        PuzzleState ApplyInput(ref PuzzleInputData inputData);

        PuzzleElementState ApplyState(PuzzleElementState state);

    }
}