﻿using System;
using System.Collections.Generic;
using Game.Code.JigsawPuzzle.Runtime.Models;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    public interface IJigsawPuzzleElementInfo
    {
        IObservable<IPuzzleElementModel> ElementStateChanged { get; }
        
        IEnumerable<IPuzzleElementModel> Elements { get; }
        
        float Transparency { get; }
        
        void ApplyBackgroundTransparency(float transparency);
        
        int GetPuzzleItemIdByPosition(Vector3 pointerPosition);

        void ResetSelectedPiece();
        
        int GetCurrentActivePuzzlePiece();
        bool IsInAssemblyGroup(int id);
        bool IsAssembled(int id);
    }
}