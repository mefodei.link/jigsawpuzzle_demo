﻿namespace Game.Code.JigsawPuzzle.Runtime
{
    using Cysharp.Threading.Tasks;
    using Game.Code.JigsawPuzzle.Runtime.Models;
    using UniGame.GameFlow.Runtime.Interfaces;
    
    public interface IElementsBehaviourService : IGameService
    {
        UniTask BindToLevel(IJigsawActiveLevelModel puzzleItems);

        PuzzleState ApplyInputToPuzzle(PuzzleInputData inputData);
        
        public void UpdateAllStates();
    }
}