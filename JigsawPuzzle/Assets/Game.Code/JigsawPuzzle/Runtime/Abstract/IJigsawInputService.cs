﻿using System;
using Game.Code.JigsawPuzzle.Runtime.Models;
using UniRx;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    using UniGame.GameFlow.Runtime.Interfaces;

    public interface IJigsawInputService : IGameService
    {
        IReadOnlyReactiveProperty<PuzzleAreaType> ElementAreaInput { get; }
        
        IReadOnlyReactiveProperty<PuzzleInputData> ElementsInput { get; }

        void ApplyInput(int id, PuzzleInputType inputType);

        IDisposable RegisterArea(IPuzzleArea area,PuzzleAreaType areaType);
    }


    public interface IPuzzleArea
    {

        bool IsScreenPointInArea(Vector2 point, Camera gameCamera);
        
        int GetElementInAreaByScreenPoint(Vector2 point, Camera gameCamera);
        
    }
}