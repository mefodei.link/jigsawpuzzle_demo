﻿using UniRx;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    public interface IJigsawLevelStateModel
    {
        IReadOnlyReactiveProperty<JigsawGameState> LevelState { get; }
    }
}