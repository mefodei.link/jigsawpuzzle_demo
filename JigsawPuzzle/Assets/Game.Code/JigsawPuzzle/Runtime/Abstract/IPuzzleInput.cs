﻿using Game.Code.JigsawPuzzle.Runtime.Models;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    public interface IPuzzleInput
    {
        PuzzleState ApplyInput(ref PuzzleInputData inputData);
    }
}