using UnityEngine;

namespace Game.Code.JigsawPuzzle.Runtime.Abstract
{
    using UniRx;
    using System.Collections.Generic;
    using Cysharp.Threading.Tasks;
    using Models;
    using UniGame.GameFlow.Runtime.Interfaces;

    
    public interface IJigsawPuzzleGameService : IGameService
    {
        public IReadOnlyReactiveProperty<IJigsawActiveLevelModel> ActiveLevel { get; }
        
        public UniTask<IJigsawActiveLevelModel> CreatePuzzle(JigsawPuzzleLevelModel puzzleLevelModel,bool restoreState);
        
        public UniTask SaveProgress();

        public UniTask LoadProgress();

    }
}
