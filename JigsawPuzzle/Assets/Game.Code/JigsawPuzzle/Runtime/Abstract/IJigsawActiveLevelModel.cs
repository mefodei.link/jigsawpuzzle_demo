﻿using System.Collections.Generic;
using UniRx;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    public interface IJigsawActiveLevelModel
    {
        string Id { get; }
        IReadOnlyReactiveProperty<JigsawPuzzleLevelModel> LevelModel { get; }
        List<PuzzleElementModel> PuzzleItems { get; }
    }
}