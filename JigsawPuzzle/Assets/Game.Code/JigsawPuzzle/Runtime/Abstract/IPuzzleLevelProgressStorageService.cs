﻿using Cysharp.Threading.Tasks;
using Game.Code.JigsawPuzzle.Runtime.Models;
using JigsawPuzzle_Kit._SystemScripts;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    public interface IPuzzleLevelProgressStorageService
    {
        void Register(IPuzzleProgressHandler handler);
        UniTask SaveProgress(IJigsawActiveLevelModel levelModel);
        UniTask<PuzzleSaveData> LoadProgress(string level);
    }
}