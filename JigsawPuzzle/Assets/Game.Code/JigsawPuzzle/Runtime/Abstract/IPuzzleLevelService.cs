﻿using System;
using Cysharp.Threading.Tasks;
using Game.Code.JigsawPuzzle.Runtime.Models;
using JigsawPuzzle_Kit._SystemScripts;
using UniGame.GameFlow.Runtime.Interfaces;
using UniRx;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.Runtime.Services
{
    public interface IPuzzleLevelService : IGameService, 
        IJigsawPuzzleElementInfo,
        IPuzzleProgressHandler, 
        IPuzzleInput
    {
        IReadOnlyReactiveProperty<Vector2> PuzzleScreenSize { get; }
        
        IReadOnlyReactiveProperty<PuzzleState> PuzzleState { get; }

        IReadOnlyReactiveProperty<IJigsawActiveLevelModel> Level { get; }
        
        IReadOnlyReactiveProperty<Sprite> LevelTexture { get; }

        UniTask<IJigsawActiveLevelModel> CreateLevel(JigsawPuzzleLevelModel puzzleLevelModel);

        void ApplyBackgroundTransparency(float transparency);
    }
}