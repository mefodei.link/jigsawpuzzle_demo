﻿using Game.Code.JigsawPuzzle.Runtime.Models;
using UniGame.GameFlow.Runtime.Interfaces;
using UniRx;

namespace Game.Code.JigsawPuzzle.Runtime
{
    public interface IJigsawGameplayStateService : IGameService
    {
        
        IReadOnlyReactiveProperty<JigsawGameState> LevelState { get; }

        bool ApplyState(JigsawGameState state);
    }
}