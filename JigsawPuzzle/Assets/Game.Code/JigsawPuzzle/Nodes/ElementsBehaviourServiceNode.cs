﻿using System;
using Cysharp.Threading.Tasks;
using Game.Code.JigsawPuzzle.Runtime.Services;
using UniGame.Context.Runtime.Extension;
using UniGame.Core.Runtime;
using UniGame.Rx.Runtime.Extensions;
using UniGame.UniNodes.GameFlow.Runtime.Nodes;
using UniModules.UniGameFlow.NodeSystem.Runtime.Core.Attributes;

namespace Game.Code.JigsawPuzzle.Runtime.Nodes
{
    [Serializable]
    [CreateNodeMenu("Game/Jigsaw/Elements Behaviour Service")]
    public class ElementsBehaviourServiceNode : ServiceSerializableNode<IElementsBehaviourService>
    {
        protected sealed override async UniTask<IElementsBehaviourService> CreateService(IContext context)
        {
            var inputService = await context.ReceiveFirstAsync<IJigsawInputService>(LifeTime);
            var levelService = await context.ReceiveFirstAsync<IPuzzleLevelService>(LifeTime);
            
            var elementsBehaviour = new PuzzleElementsBehaviourServiceService(levelService);
            
            this.Bind(inputService.ElementsInput,x => elementsBehaviour.ApplyInputToPuzzle(x));

            return elementsBehaviour;
        }
    }
}