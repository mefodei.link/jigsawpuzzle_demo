using Game.Code.JigsawPuzzle.Runtime.Models;
using Game.Code.JigsawPuzzle.Runtime.Abstract;
using Game.Code.JigsawPuzzle.Runtime.Services;

namespace Game.Code.JigsawPuzzle.Runtime.Nodes
{
    using System;
    using System.Linq;
    using Cysharp.Threading.Tasks;
    using Sirenix.OdinInspector;
    using UniGame.UniNodes.GameFlow.Runtime.Nodes;
    using UniGame.Context.Runtime.Extension;
    using UniGame.Core.Runtime;
    using UniModules.UniGameFlow.NodeSystem.Runtime.Core.Attributes;
    using UniRx;
    using UnityEngine;


    [Serializable]
    [CreateNodeMenu("Game/Jigsaw/JigsawPuzzleService")]
    public class JigsawPuzzleServiceNode : ServiceSerializableNode<IJigsawPuzzleGameService>
    {

        protected override async UniTask<IJigsawPuzzleGameService> CreateService(IContext context)
        {
            var levelService = await context.ReceiveFirstAsync<IPuzzleLevelService>(LifeTime);
            var modeModel = await context.ReceiveFirstAsync<IJigsawGameModeSettings>();

            var progressDriver = new JigsawPuzzleProgressDriver();
            var progressService =
                new PuzzleLevelProgressStorageService(Enumerable.Empty<IPuzzleProgressHandler>(), progressDriver);
            //create puzzle service
            var service = new JigsawGameplayService(modeModel,levelService, progressService);
            
            context.Publish<IPuzzleLevelProgressStorageService>(progressService);

            Application.quitting += OnApplicationQuit;

            return service;
        }


        [Button]
        public void Save()
        {
            Service?.SaveProgress().Forget();
        }

        [Button]
        public void Load()
        {
            Service?.LoadProgress().Forget();
        }


        private void OnApplicationQuit()
        {
            Application.quitting -= OnApplicationQuit;
            Service?.SaveProgress();
        }

    }
}
