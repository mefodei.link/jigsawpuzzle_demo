using Game.Code.JigsawPuzzle.Runtime.Hints;
using UniGame.AddressableTools.Runtime;
using UnityEngine.AddressableAssets;

namespace Game.Code.JigsawPuzzle.Runtime.Nodes
{
    using Services;
    using UniGame.Rx.Runtime.Extensions;
    using Models;
    using UniRx;
    using UnityEngine;
    using Cysharp.Threading.Tasks;
    using ViewModels;
    using Views.GameplayViews;
    using UniGame.Core.Runtime;
    using UniGame.Context.Runtime.Extension;
    using UniGame.ViewSystem.Runtime;
    using UniModules.UniGameFlow.NodeSystem.Runtime.Core.Attributes;
    using System.Collections.Generic;
    using System;
    using UniGame.UniNodes.Nodes.Runtime.Common;

    [Serializable]
    [CreateNodeMenu("Game/Jigsaw/Views/CreateJigsawLevelView")]
    public class CreateJigsawLevelViewsNode : SContextNode
    {
        protected override async UniTask<bool> OnContextActivate(IContext context)
        {
            var hintsData = await context.ReceiveFirstAsync<PuzzleHintsSettings>(LifeTime);
            var state = await context.ReceiveFirstAsync<IJigsawLevelStateModel>(LifeTime);
            var levelService = await context.ReceiveFirstAsync<IPuzzleLevelService>(LifeTime);
            var puzzleInfo = await context.ReceiveFirstAsync<IJigsawPuzzleElementInfo>(LifeTime);
            var inputService = await context.ReceiveFirstAsync<IJigsawInputService>(LifeTime);
            var levelProgress = await context.ReceiveFirstAsync<ILevelProgress>(LifeTime);

            await state.LevelState
                .First(x => x != JigsawGameState.Initialize);

            var levelModel = await levelService.Level.First();
            var viewSystem = await context.ReceiveFirstAsync<IGameViewSystem>(LifeTime);
            var gameCamera = await context.ReceiveFirstAsync<Camera>(LifeTime);
            var levelData = levelModel.LevelModel.Value;
            
            var buttonsModel = CreateButtonsPanel(levelData,puzzleInfo,inputService,hintsData);
            var cornersToggle = buttonsModel.Corners;

            var filter = CreateElementsFilter(levelModel, buttonsModel.Corners.Execute);
            var scrollViewModel = CreateScrollViewModel(levelModel, filter, gameCamera);

            //bind toggle button and scroll items
            this.Bind(cornersToggle.Execute, scrollViewModel.UpdateViewData);

            var viewModel = await CreateGameViewModel(
                levelProgress,
                levelService,
                buttonsModel,
                scrollViewModel, gameCamera);

            var view = await viewSystem.Create<JigsawGameplayView>(viewModel,
                LifeTime,
                null, false);

            var scrollView = view.scrollView;
            inputService.RegisterArea(scrollView, PuzzleAreaType.DropArea);
            inputService.RegisterArea(view, PuzzleAreaType.GameArea);

            return true;
        }

        private JigsawItemsScrollViewModel CreateScrollViewModel(
            IJigsawActiveLevelModel levelModel,
            IElementsViewFilter filter,
            Camera gameCamera)
        {
            var scrollViewModel = new JigsawItemsScrollViewModel(
                levelModel.PuzzleItems,
                filter,
                gameCamera).AddTo(LifeTime);

            return scrollViewModel;
        }

        private IElementsViewFilter CreateElementsFilter(
            IJigsawActiveLevelModel levelModel,
            IObservable<bool> bordersToggle)
        {
            var levelData = levelModel.LevelModel.Value;

            var activeViewModelsFilter = new HiddenElementsFilter(levelModel.PuzzleItems)
                .AddTo(LifeTime);
            
            var borderFilter = new CornersElementsFilter(levelData.cols, 
                levelData.rows, bordersToggle)
                .AddTo(LifeTime);
            
            var subFilters = new List<IElementsViewFilter>() {activeViewModelsFilter, borderFilter,};
            var filter = new PuzzleElementsFilter(subFilters).AddTo(LifeTime);

            return filter;
        }

        private IJigsawButtonsPanelViewModel CreateButtonsPanel(
            JigsawPuzzleLevelModel levelModel,
            IJigsawPuzzleElementInfo elementsInfo,
            IJigsawInputService inputService,
            PuzzleHintsSettings hintsSettings)
        {
            var viewModel = new JigsawButtonPanelViewModel();

            viewModel.home = new ButtonViewModel().AddTo(LifeTime);
            viewModel.settings = new ButtonViewModel().AddTo(LifeTime);

            viewModel.background = new BackgroundToggleViewModel(inputService,hintsSettings.backgroundHint)
                .AddTo(LifeTime);
            viewModel.corners = new CornersToggleViewModel(levelModel,elementsInfo)
                .AddTo(LifeTime);

            return viewModel;
        }

        private UniTask<JigsawGameplayAreaViewModel> CreateGameViewModel(
            ILevelProgress progress,
            IPuzzleLevelService levelService,
            IJigsawButtonsPanelViewModel buttonPanel,
            JigsawItemsScrollViewModel itemsModel,
            Camera gameCamera)
        {
            
            var progressModel = new LevelProgressViewModel(progress).AddTo(LifeTime);
            var timeModel = new LevelTimeViewModel().AddTo(LifeTime);
            var gameAreaModel = new GameAreaViewModel().AddTo(LifeTime);

            this.Bind(buttonPanel.Background.Execute,
                    x => gameAreaModel.transparency.SetValue(x ? 1f : 0f))
                .Bind(levelService.PuzzleScreenSize, gameAreaModel.size)
                .Bind(levelService.LevelTexture,gameAreaModel.image);
            
            var model = new JigsawGameplayAreaViewModel(
                levelService,
                gameAreaModel,
                buttonPanel,
                progressModel,
                timeModel,
                itemsModel,
                gameCamera);

            return UniTask.FromResult(model);
        }
    }
}