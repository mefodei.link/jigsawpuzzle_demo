﻿using System;
using Cysharp.Threading.Tasks;
using Game.Code.JigsawPuzzle.Runtime.Models;
using Game.Code.JigsawPuzzle.Runtime.Services;
using Sirenix.OdinInspector;
using UniGame.AddressableTools.Runtime;
using UniGame.Context.Runtime.Extension;
using UniGame.Core.Runtime;
using UniGame.UniNodes.GameFlow.Runtime.Nodes;
using UniModules.UniGameFlow.NodeSystem.Runtime.Core.Attributes;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.Runtime.Nodes
{
    [Serializable]
    [CreateNodeMenu("Game/Jigsaw/Level Service")]
    public class LevelServiceNode : ServiceSerializableNode<IPuzzleLevelService>
    {
        [DrawWithUnity]
        public AssetReferenceComponent<RuntimeGeneration> jigsawGenerator;
        
        protected sealed override async UniTask<IPuzzleLevelService> CreateService(IContext context)
        {
            var generator = await jigsawGenerator
                .LoadAssetInstanceTaskAsync<RuntimeGeneration>(LifeTime,true);

            var modeSettings = await context.ReceiveFirstAsync<IJigsawGameModeSettings>(LifeTime);
            
            //get game camera from generator asset
            var gameCamera = generator.GetComponent<Camera>();
            context.Publish(gameCamera);

            var levelService = new PuzzleLevelService(modeSettings.ModeSettings,gameCamera,generator);
            context.Publish<IJigsawPuzzleElementInfo>(levelService);

            return levelService;
        }
    }
}