﻿using System;
using Cysharp.Threading.Tasks;
using Game.Code.JigsawPuzzle.Runtime.Models;
using UniGame.Core.Runtime;
using UniGame.UniNodes.GameFlow.Runtime.Nodes;
using UniModules.UniGameFlow.NodeSystem.Runtime.Core.Attributes;

namespace Game.Code.JigsawPuzzle.Runtime.Nodes
{
    [Serializable]
    [CreateNodeMenu("Game/Jigsaw/State Service")]
    public class JigsawStateServiceNode : ServiceSerializableNode<IJigsawGameplayStateService>
    {
        protected override UniTask<IJigsawGameplayStateService> CreateService(IContext context)
        {
            //create level state service
            var gameLevelStateModel = new JigsawLevelStateModel();
            var stateService = new JigsawGameplayStateService(gameLevelStateModel);

            context.Publish<IJigsawLevelStateModel>(gameLevelStateModel);
            return UniTask.FromResult<IJigsawGameplayStateService>(stateService);

        }

    }
}