﻿using Sirenix.OdinInspector;
using UniGame.AddressableTools.Runtime;
using UnityEngine.AddressableAssets;

namespace Game.Code.JigsawPuzzle.Runtime.Nodes
{
    using UniGame.Context.Runtime.Extension;
    using UnityEngine;
    using System;
    using Cysharp.Threading.Tasks;
    using Services;
    using UniGame.Core.Runtime;
    using UniGame.UniNodes.GameFlow.Runtime.Nodes;
    using UniModules.UniGameFlow.NodeSystem.Runtime.Core.Attributes;
    
    [Serializable]
    [CreateNodeMenu("Game/Jigsaw/Input Service")]
    public class JigsawInputServiceNode : ServiceSerializableNode<IJigsawInputService>
    {
        [DrawWithUnity]
        public AssetReferenceT<InputServiceSettingsAsset> inputSettings;

        protected sealed override async UniTask<IJigsawInputService> CreateService(IContext context)
        {
            var settings = await inputSettings.LoadAssetTaskAsync(LifeTime);
            var levelService = await context.ReceiveFirstAsync<IPuzzleLevelService>(LifeTime);
            var stateService = await context.ReceiveFirstAsync<IJigsawGameplayStateService>(LifeTime);
            var camera = await context.ReceiveFirstAsync<Camera>(LifeTime);

            var inputService = new JigsawInputService(settings.Value,stateService,levelService,camera);

            inputService.Execute().Forget();
            
            return inputService;
        }
    }
}