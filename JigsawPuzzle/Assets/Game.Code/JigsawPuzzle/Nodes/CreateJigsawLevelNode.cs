using Game.Code.Game.LevelService;
using Game.Code.JigsawPuzzle.Runtime.Abstract;
using Game.Code.JigsawPuzzle.Runtime.Services;
using Game.Code.Lobby.Models;
using Game.Code.JigsawPuzzle.Runtime.Abstract;
using Game.Code.JigsawPuzzle.Runtime.Services;

namespace Game.Code.JigsawPuzzle.Runtime.Nodes
{
    using UniGame.Rx.Runtime.Extensions;
    using Sirenix.OdinInspector;
    using UniModules.UniGame.AddressableTools.Runtime.Attributes;
    using UniGame.AddressableTools.Runtime;
    using UniGame.Context.Runtime.Extension;
    using UnityEngine.AddressableAssets;
    using System;
    using Cysharp.Threading.Tasks;
    using UniGame.UniNodes.Nodes.Runtime.Common;
    using UniGame.Core.Runtime;
    using UniModules.UniGameFlow.NodeSystem.Runtime.Core.Attributes;

    [Serializable]
    [CreateNodeMenu("Game/Jigsaw/CreateJigsawLevel")]
    public class CreateJigsawLevelNode : SContextNode
    {

        protected override async UniTask<bool> OnContextActivate(IContext context)
        {
            var levelsService = await context.ReceiveFirstAsync<ILevelsService>(LifeTime);

            var levelService = await context.ReceiveFirstAsync<IPuzzleLevelService>(LifeTime);
            var jigsawGameService = await context.ReceiveFirstAsync<IJigsawPuzzleGameService>(LifeTime);
            var progressService = await context.ReceiveFirstAsync<IPuzzleLevelProgressStorageService>(LifeTime);
            var behaviourService = await context.ReceiveFirstAsync<IElementsBehaviourService>(LifeTime);

            progressService.Register(levelService);

            //bind actial level to puzzle element behaviour
            this.Bind(levelService.Level, x => behaviourService.BindToLevel(x));

            var levelModel = await CreatePuzzleLevel(levelsService.ActiveLevel);
            var activeLevel = await jigsawGameService.CreatePuzzle(levelModel, true);
            await behaviourService.BindToLevel(activeLevel);
            
            var model = levelModel;

            var levelProgressService = new JigsawLevelProgressService(activeLevel).AddTo(LifeTime);
            
            context.Publish<ILevelProgress>(levelProgressService);
            context.Publish(model);
            context.Publish(activeLevel);
            
            return true;
        }

        private async UniTask<JigsawPuzzleLevelModel> CreatePuzzleLevel(IGameLevelModel gameLevelModel)
        {
            var levelModel = gameLevelModel.Level;
            var preset = gameLevelModel.Difficulty;
            var sprite = await levelModel.SpriteReference.LoadAssetTaskAsync(LifeTime);

            var model = new JigsawPuzzleLevelModel()
            {
                cols = preset.Columns,
                rows = preset.Rows,
                id = levelModel.Id,
                generateBackground = levelModel.GenerateBackground,
                levelImage = sprite.texture
            };

            return model;
        }

    }
}
