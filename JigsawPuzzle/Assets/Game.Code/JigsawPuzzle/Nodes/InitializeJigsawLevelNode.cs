﻿namespace Game.Code.JigsawPuzzle.Runtime.Nodes
{
    using System;
    using Cysharp.Threading.Tasks;
    using Abstract;
    using Models;
    using UniGame.Core.Runtime;
    using UniGame.UniNodes.Nodes.Runtime.Common;
    using UniGame.Context.Runtime.Extension;
    using UniModules.UniGameFlow.NodeSystem.Runtime.Core.Attributes;

    [Serializable]
    [CreateNodeMenu("Game/Jigsaw/Initialize Jigsaw Level")]
    public class InitializeJigsawLevelNode : SContextNode
    {
        public JigsawGameState gameState = JigsawGameState.Initialized;
        
        protected override async UniTask<bool> OnContextActivate(IContext context)
        {
            var stateService = await context.ReceiveFirstAsync<IJigsawGameplayStateService>(LifeTime);
            var gameService = await context.ReceiveFirstAsync<IJigsawPuzzleGameService>(LifeTime);
            var itemController = await context.ReceiveFirstAsync<IElementsBehaviourService>(LifeTime);

            stateService.ApplyState(gameState);

            return true;
        }

    }

}