using System;
using UniGame.UiSystem.Runtime;
using UniModules.UniGame.Core.Runtime.Rx;
using UniRx;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    using UnityEngine;

    [Serializable]
    public class ButtonViewModel : ViewModelBase, IButtonViewModel
    {
        public ReactiveCommand executeCommand;
        public RecycleReactiveProperty<Sprite> icon;
        public RecycleReactiveProperty<string> label;
        
        public ButtonViewModel()
        {
            executeCommand = new ReactiveCommand().AddTo(LifeTime);
            icon = new RecycleReactiveProperty<Sprite>().AddTo(LifeTime);
            label = new RecycleReactiveProperty<string>().AddTo(LifeTime);
        }

        public IReadOnlyReactiveProperty<Sprite> Icon => icon;
        public IReadOnlyReactiveProperty<string> Label => label;
        public IReactiveCommand<Unit> Execute => executeCommand;
    }
}
