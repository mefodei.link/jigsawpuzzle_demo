﻿using UniModules.UniCore.Runtime.DataFlow;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    using System.Collections.Generic;
    using Runtime.Models;
    using Views.PuzzleScrollViews;

    public class HiddenElementsFilter : IElementsViewFilter
    {
        private Dictionary<int, PuzzleElementModel> _elementsMap = new();
        private List<PuzzleElementViewModel> _result = new();
        
        public bool IsActive => true;

        public HiddenElementsFilter(List<PuzzleElementModel> elements)
        {
            foreach (var element in elements)
                _elementsMap[element.id] = element;
        }
        
        public void Filter(List<PuzzleElementViewModel> origin)
        {
            foreach (var item in origin)
            {
                if(!_elementsMap.TryGetValue(item.id,out var element))
                    continue;
                if (element.State.Value == PuzzleElementState.Hidden)
                    _result.Add(item);
            }
         
            origin.Clear();
            origin.AddRange(_result);
            
            _result.Clear();
        }

        public void Dispose()
        {
        }
    }
}