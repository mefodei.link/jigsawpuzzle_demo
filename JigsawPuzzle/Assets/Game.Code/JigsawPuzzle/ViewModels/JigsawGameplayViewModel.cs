using UniRx;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    using System;
    using UniGame.UiSystem.Runtime;
    
    [Serializable]
    public class JigsawGameplayViewModel : ViewModelBase
    {
        private ReactiveProperty<JigsawItemsScrollViewModel> _itemsModel = new ReactiveProperty<JigsawItemsScrollViewModel>();

        public JigsawGameplayViewModel(JigsawItemsScrollViewModel item)
        {
            _itemsModel.Value = item;
        }
        
        public IReactiveProperty<JigsawItemsScrollViewModel> Items => _itemsModel;
    }
}
