using UniCore.Runtime.ProfilerTools;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    using UnityEngine;
    using System.Collections.Generic;
    using Runtime.Models;
    using UniGame.Core.Runtime.Extension;
    using UniGame.Rx.Runtime.Extensions;
    using UniModules.UniCore.Runtime.Utils;
    using UniModules.UniGame.Core.Runtime.Rx;
    using Views.PuzzleScrollViews;
    using UniRx;
    using System;
    using UniGame.UiSystem.Runtime;

    [Serializable]
    public class JigsawItemsScrollViewModel : ViewModelBase, IJigsawItemsScrollViewModel
    {
        private readonly Camera _gameCamera;
        private readonly IElementsViewFilter _filter;

        private Dictionary<PuzzleElementModel, PuzzleElementViewModel> _allItems = new(512);

        private List<PuzzleElementViewModel> _puzzleElements = new(512);
        private RecycleReactiveProperty<int> _activeIndex;
        private RecycleReactiveProperty<bool> _interactable;
        private ReactiveCommand _updateDataCommand;
        private ReactiveCommand<PuzzleElementViewModel> _findPositionCommand;
        private ReactiveCommand<PuzzleElementViewPosition> _insertCommand;

        public JigsawItemsScrollViewModel(
            IReadOnlyList<PuzzleElementModel> items,
            IElementsViewFilter filter,
            Camera gameCamera)
        {
            _filter = filter;
            _gameCamera = gameCamera;

            _activeIndex = new RecycleReactiveProperty<int>(0).AddTo(LifeTime);
            _interactable = new RecycleReactiveProperty<bool>(true).AddTo(LifeTime);
            _updateDataCommand = new ReactiveCommand().AddTo(LifeTime);
            _findPositionCommand = new ReactiveCommand<PuzzleElementViewModel>().AddTo(LifeTime);
            _insertCommand = new ReactiveCommand<PuzzleElementViewPosition>().AddTo(LifeTime);

            CreateData(items);
            BindData();
            UpdateViewData();
        }

        #region public properties

        public Camera Camera => _gameCamera;

        public IObservable<Unit> UpdateData => _updateDataCommand;

        public IObservable<PuzzleElementViewModel> FindItemPosition => _findPositionCommand;

        public IReactiveCommand<PuzzleElementViewPosition> InsertCommand => _insertCommand;

        public IReadOnlyList<PuzzleElementViewModel> PuzzleItems => _puzzleElements;

        public IReadOnlyReactiveProperty<int> SelectedItem => _activeIndex;

        public IReadOnlyReactiveProperty<bool> Interactable => _interactable;

        #endregion

        public void SetSelectedIndex(int index)
        {
            _activeIndex.Value = index;
        }

        public void CreateData(IEnumerable<PuzzleElementModel> items)
        {
            foreach (var item in items)
            {
                var element = item.element;
                var viewModel = new PuzzleElementViewModel()
                {
                    id = item.id,
                    message = item.id.ToStringFromCache(),
                    image = item.sprite,
                    element = element,
                    position = item.interactionPoint.Value
                };

                _allItems[item] = viewModel;
            }
        }

        public void UpdateViewData()
        {
            _puzzleElements.Clear();
            _puzzleElements.AddRange(_allItems.Values);
            _filter.Filter(_puzzleElements);
            _updateDataCommand.Execute();
        }

        private void BindData()
        {
            this.Bind(InsertCommand, InsertIntoView);

            foreach (var item in _allItems)
            {
                var element = item.Key;
                var viewModel = item.Value;

                this.Bind(element.interactionPoint, x => viewModel.position = x)
                    .Bind(element.State, x => OnElementStateChanged(x, viewModel));
            }
        }

        private void OnElementStateChanged(PuzzleElementState state, PuzzleElementViewModel viewModel)
        {
            switch (state)
            {
                case PuzzleElementState.Hidden:
                    _interactable.Value = true;
                    viewModel.state.Value = PuzzleElementViewState.Active;
                    _findPositionCommand.Execute(viewModel);
                    break;
                case PuzzleElementState.Dragging:
                    _interactable.Value = false;
                    break;
                case PuzzleElementState.InGame:
                case PuzzleElementState.Assembled:
                case PuzzleElementState.InGroup:
                    _interactable.Value = true;
                    RemoveFromView(viewModel);
                    break;
                case PuzzleElementState.ReadyToDrop:
                    _interactable.Value = false;
                    viewModel.state.SetValueForce(PuzzleElementViewState.Empty);
                    _findPositionCommand.Execute(viewModel);
                    break;
            }
        }

        private void RemoveFromView(PuzzleElementViewModel item)
        {
            _puzzleElements.Remove(item);
            _updateDataCommand.Execute();
        }

        private void InsertIntoView(PuzzleElementViewPosition data)
        {
            var model = data.element;
            _puzzleElements.Remove(model);

            var index = data.index;
            index = Mathf.Clamp(index, 0, _puzzleElements.Count);
            _puzzleElements.Insert(index, model);

            _updateDataCommand.Execute();
        }
    }

    [Serializable]
    public struct PuzzleElementViewPosition
    {
        public PuzzleElementViewModel element;
        public int index;
    }
}