﻿using System;
using UniGame.UiSystem.Runtime;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    [Serializable]
    public class JigsawButtonPanelViewModel : ViewModelBase, IJigsawButtonsPanelViewModel
    {
        public IButtonViewModel home;
        public IButtonViewModel settings;
        public IToggleButtonViewModel corners;
        public IToggleButtonViewModel background;


        public IButtonViewModel Home => home;
        public IButtonViewModel Settings => settings;
        
        public IToggleButtonViewModel Corners => corners;
        public IToggleButtonViewModel Background => background;

    }
}