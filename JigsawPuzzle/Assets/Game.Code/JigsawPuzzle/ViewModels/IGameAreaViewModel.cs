﻿using UniGame.ViewSystem.Runtime;
using UniRx;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    public interface IGameAreaViewModel : IViewModel
    {
        IReadOnlyReactiveProperty<Vector2> Size { get; }
        IReadOnlyReactiveProperty<Sprite> Image { get; }
        IReadOnlyReactiveProperty<float> Transparency { get; }
    }
}