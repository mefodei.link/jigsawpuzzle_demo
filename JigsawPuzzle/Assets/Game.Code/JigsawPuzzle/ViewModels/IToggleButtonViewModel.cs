﻿using System;
using UniGame.ViewSystem.Runtime;
using UniRx;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    public interface IToggleButtonViewModel : IViewModel
    {
        IReadOnlyReactiveProperty<Sprite> Icon { get; }
        
        IReadOnlyReactiveProperty<string> Label { get; }

        IReactiveCommand<bool> Execute { get; }
        
        IObservable<bool> Enabled { get; }
    }
}