﻿using System;
using Game.Code.JigsawPuzzle.Runtime;
using Game.Code.JigsawPuzzle.Views.GameplayViews;
using UniGame.Rx.Runtime.Extensions;
using UniGame.UiSystem.Runtime;
using UniModules.UniCore.Runtime.Utils;
using UniModules.UniGame.Core.Runtime.Rx;
using UniRx;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    [Serializable]
    public class LevelProgressViewModel :ViewModelBase, IPuzzleStatusViewModel
    {
        private const string ProgressTemplate = "{0}%";
        private readonly ILevelProgress _progress;
        private readonly RecycleReactiveProperty<string> _label;
        private readonly RecycleReactiveProperty<Sprite> _image;

        public LevelProgressViewModel(ILevelProgress progress)
        {
            _progress = progress;
            _image = new RecycleReactiveProperty<Sprite>().AddTo(LifeTime);
            _label = new RecycleReactiveProperty<string>().AddTo(LifeTime);

            this.Bind(_progress.Progress, UpdateProgress);
        }

        public IReadOnlyReactiveProperty<string> Label => _label;
        public IReadOnlyReactiveProperty<Sprite> Image => _image;

        private void UpdateProgress(float value)
        {
            var proc = (int)Mathf.Clamp(value * 100,0,100);
            _label.Value = string.Format(ProgressTemplate,proc.ToStringFromCache());
        }
    }
}