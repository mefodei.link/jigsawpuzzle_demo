using Game.Code.JigsawPuzzle.Views.GameplayViews;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    using Game.Code.JigsawPuzzle.Runtime.Services;
    using UniModules.UniGame.Core.Runtime.Rx;
    using UniRx;
    using System;
    using UnityEngine;
    using UniGame.UiSystem.Runtime;

    [Serializable]
    public class JigsawGameplayAreaViewModel : ViewModelBase
    {
        private readonly IJigsawPuzzleElementInfo _elementInfo;
        private Camera _cameraValue;
        private RecycleReactiveProperty<Camera> _camera = new RecycleReactiveProperty<Camera>();
        private RecycleReactiveProperty<IGameAreaViewModel> _gameAreaModel;
        private RecycleReactiveProperty<IJigsawButtonsPanelViewModel> _buttonsPanel;
        private RecycleReactiveProperty<IPuzzleStatusViewModel> _levelProgressStatus;
        private RecycleReactiveProperty<IPuzzleStatusViewModel> _levelTimeStatus;
        private RecycleReactiveProperty<JigsawItemsScrollViewModel> _itemsModel = new RecycleReactiveProperty<JigsawItemsScrollViewModel>();

        public JigsawGameplayAreaViewModel(
            IJigsawPuzzleElementInfo elementInfo,
            IGameAreaViewModel gameAreaViewModel,
            IJigsawButtonsPanelViewModel buttonsPanelViewModel,
            IPuzzleStatusViewModel progressStatus,
            IPuzzleStatusViewModel levelTimeStatus,
            JigsawItemsScrollViewModel item, 
            Camera camera)
        {
            _elementInfo = elementInfo;

            _buttonsPanel = new RecycleReactiveProperty<IJigsawButtonsPanelViewModel>(buttonsPanelViewModel)
                    .AddTo(LifeTime);

            _gameAreaModel = new RecycleReactiveProperty<IGameAreaViewModel>(gameAreaViewModel)
                .AddTo(LifeTime);
            
            _levelProgressStatus = new RecycleReactiveProperty<IPuzzleStatusViewModel>(progressStatus)
                .AddTo(LifeTime);
            
            _levelTimeStatus = new RecycleReactiveProperty<IPuzzleStatusViewModel>(levelTimeStatus) 
                .AddTo(LifeTime);
            
            _itemsModel.Value = item;
            _camera.Value = camera;
            _cameraValue = camera;
        }

        public IReactiveProperty<JigsawItemsScrollViewModel> Items => _itemsModel;

        public IReadOnlyReactiveProperty<IGameAreaViewModel> GameArea => _gameAreaModel;

        public IReadOnlyReactiveProperty<IJigsawButtonsPanelViewModel> ButtonsPanel => _buttonsPanel;
        
        public IReadOnlyReactiveProperty<IPuzzleStatusViewModel> Progress => _levelProgressStatus;
        
        public IReadOnlyReactiveProperty<IPuzzleStatusViewModel> Time => _levelTimeStatus;

        public IReactiveProperty<Camera> Camera => _camera;

        public int GetPuzzleByPoint(Vector2 point)
        {
            var worldPoint =  _cameraValue.ScreenToWorldPoint(point);
            var id =_elementInfo.GetPuzzleItemIdByPosition(worldPoint);
            return id;
        }
        
    }
}
