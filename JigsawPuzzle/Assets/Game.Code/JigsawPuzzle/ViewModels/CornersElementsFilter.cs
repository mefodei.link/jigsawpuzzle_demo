﻿using System;
using System.Collections.Generic;
using Game.Code.JigsawPuzzle.Views.PuzzleScrollViews;
using UniModules.UniCore.Runtime.DataFlow;
using UniRx;
using UnityEngine.Pool;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    [Serializable]
    public class CornersElementsFilter : IElementsViewFilter
    {
        private LifeTimeDefinition _lifeTime = new();
        private List<PuzzleElementViewModel> _filterResult = new();

        private bool _isActive;
        private int _rowCount;
        private int _columnCount;

        public CornersElementsFilter(int columnCount, int rowCount, IObservable<bool> isActive)
        {
            isActive.Subscribe(x => _isActive = x).AddTo(_lifeTime);
            _columnCount = columnCount;
            _rowCount = rowCount;
        }

        public bool IsActive => _isActive;
        
        
        public void Dispose()
        {
            _lifeTime.Terminate();
        }
        
        public void Filter(List<PuzzleElementViewModel> origin)
        {
            _filterResult.Clear();
            
            foreach (var viewModel in origin)
            {
                var element = viewModel.element;
                var column = element.column;
                var row = element.row;

                if (row == 0 || column == 0 || 
                    row == _rowCount - 1 || column == _columnCount - 1) 
                    _filterResult.Add(viewModel);
            }

            if (_filterResult.Count == 0) return;
            
            origin.Clear();
            origin.AddRange(_filterResult);
            
            _filterResult.Clear();
        }

    }
}