using System;
using UniGame.UiSystem.Runtime;
using UniModules.UniGame.Core.Runtime.Rx;
using UniRx;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    [Serializable]
    public class GameAreaViewModel : ViewModelBase, IGameAreaViewModel
    {
        public RecycleReactiveProperty<Vector2> size;
        public RecycleReactiveProperty<Sprite> image;
        public RecycleReactiveProperty<float> transparency;
        
        public GameAreaViewModel()
        {
            size = new RecycleReactiveProperty<Vector2>().AddTo(LifeTime);
            image = new RecycleReactiveProperty<Sprite>().AddTo(LifeTime);
            transparency = new RecycleReactiveProperty<float>().AddTo(LifeTime);
        }
        
        public IReadOnlyReactiveProperty<Vector2> Size => size;

        public IReadOnlyReactiveProperty<Sprite> Image => image;

        public IReadOnlyReactiveProperty<float> Transparency => transparency;

    }
}
