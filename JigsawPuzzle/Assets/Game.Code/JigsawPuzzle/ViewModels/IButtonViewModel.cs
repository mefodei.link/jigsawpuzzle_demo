﻿using UniGame.ViewSystem.Runtime;
using UniRx;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    public interface IButtonViewModel : IViewModel
    {
        IReadOnlyReactiveProperty<Sprite> Icon { get; }
        
        IReadOnlyReactiveProperty<string> Label { get; }

        IReactiveCommand<Unit> Execute { get; }
    }
}