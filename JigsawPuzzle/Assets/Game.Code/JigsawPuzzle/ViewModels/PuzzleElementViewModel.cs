﻿namespace Game.Code.JigsawPuzzle.Views.PuzzleScrollViews
{
    using UniModules.UniGame.Core.Runtime.Rx;
    using UniRx;
    using System;
    using UnityEngine;
    
    [Serializable]
    public class PuzzleElementViewModel
    {
        public int id;
        public string message;
        public Sprite image;
        public PuzzleElement element;
        public Vector3 position;
        
        public RecycleReactiveProperty<PuzzleElementViewState> state =
            new RecycleReactiveProperty<PuzzleElementViewState>(PuzzleElementViewState.Active);
        
        #region public properties

        public IReadOnlyReactiveProperty<PuzzleElementViewState> State => state;

        public string Message => message;

        public Sprite Sprite => image;

        public int Id => id;
        
        #endregion

    }

    public enum PuzzleElementViewState : byte
    {
        None,
        Active,
        Empty,
    }

}
