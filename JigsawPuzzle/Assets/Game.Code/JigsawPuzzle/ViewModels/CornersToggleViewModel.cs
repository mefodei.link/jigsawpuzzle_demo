﻿using System;
using DG.Tweening;
using Game.Code.JigsawPuzzle.Runtime;
using Game.Code.JigsawPuzzle.Runtime.Models;
using Game.Code.JigsawPuzzle.Runtime.Services;
using UniGame.Rx.Runtime.Extensions;
using UniGame.UiSystem.Runtime;
using UniModules.UniCore.Runtime.DataFlow;
using UniModules.UniGame.Core.Runtime.Rx;
using UniModules.UniGame.DoTweenRoutines.Runtime;
using UniRx;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    [Serializable]
    public class CornersToggleViewModel : ViewModelBase, IToggleButtonViewModel
    {
        private readonly JigsawPuzzleLevelModel _levelModel;
        private IJigsawPuzzleElementInfo _puzzleInfo;
        
        public ReactiveCommand<bool> command;
        public RecycleReactiveProperty<bool> enabled;
        public RecycleReactiveProperty<Sprite> icon;
        public RecycleReactiveProperty<string> label;
        
        public CornersToggleViewModel(
            JigsawPuzzleLevelModel levelModel,
            IJigsawPuzzleElementInfo elementsInfo)
        {
            _levelModel = levelModel;
            _puzzleInfo = elementsInfo;
            
            enabled = new RecycleReactiveProperty<bool>(true).AddTo(LifeTime);
            command = new ReactiveCommand<bool>().AddTo(LifeTime);
            icon = new RecycleReactiveProperty<Sprite>().AddTo(LifeTime);
            label = new RecycleReactiveProperty<string>().AddTo(LifeTime);
            
            BindToData();
        }

        public IReadOnlyReactiveProperty<Sprite> Icon => icon;
        public IReadOnlyReactiveProperty<string> Label => label;
        public IReactiveCommand<bool> Execute => command;
        public IObservable<bool> Enabled => enabled;


        private void BindToData()
        {
            var observable = _puzzleInfo.ElementStateChanged;
            observable.Where(x => x.State.Value == PuzzleElementState.Assembled)
                .Subscribe(OnUpdateEnabled)
                .AddTo(LifeTime);
        }

        private void OnUpdateEnabled()
        {
            foreach (var element in _puzzleInfo.Elements)
            {
                var column = element.Column;
                var row = element.Row;

                var isCorner = element.State.Value == PuzzleElementState.Hidden && 
                               (row == 0 || column == 0 ||
                               row == _levelModel.rows - 1 || 
                               column == _levelModel.cols - 1);

                if (!isCorner) continue;
                
                enabled.Value = true;
                return;
            }

            command.Execute(false);
            enabled.Value = false;
        }
        
    }
}