﻿using System;
using Game.Code.JigsawPuzzle.Runtime.Hints;
using Game.Code.JigsawPuzzle.Runtime.Models;
using Game.Code.JigsawPuzzle.Runtime.Services;
using UniCore.Runtime.ProfilerTools;
using UniGame.Rx.Runtime.Extensions;
using UniGame.UiSystem.Runtime;
using UniModules.UniGame.Core.Runtime.Rx;
using UniRx;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    [Serializable]
    public class BackgroundToggleViewModel : ViewModelBase, IToggleButtonViewModel
    {
        private readonly IJigsawInputService _inputService;
        private readonly BackgroundHintSettings _settings;
        private readonly float _enableOnTouchDelay;
        private float _timer = -1;

        public ReactiveProperty<bool> activated;
        public ReactiveCommand<bool> command;
        public RecycleReactiveProperty<bool> enabled;
        public RecycleReactiveProperty<Sprite> icon;
        public RecycleReactiveProperty<string> label;
        
        public BackgroundToggleViewModel(IJigsawInputService inputService, BackgroundHintSettings settings)
        {
            _inputService = inputService;
            _settings = settings;

            enabled = new RecycleReactiveProperty<bool>(true).AddTo(LifeTime);
            command = new ReactiveCommand<bool>().AddTo(LifeTime);
            activated = new ReactiveProperty<bool>(false).AddTo(LifeTime);
            icon = new RecycleReactiveProperty<Sprite>().AddTo(LifeTime);
            label = new RecycleReactiveProperty<string>().AddTo(LifeTime);
            
            BindData();
        }

        public IReadOnlyReactiveProperty<Sprite> Icon => icon;
        public IReadOnlyReactiveProperty<string> Label => label;
        public IReactiveCommand<bool> Execute => command;
        public IObservable<bool> Enabled => enabled;


        private void BindData()
        {
            this.Bind(command,activated)
                .Bind(_inputService.ElementsInput
                    .Where(x => x.inputType!=PuzzleInputType.None && x.areaType == PuzzleAreaType.GameArea),
                    x => command.Execute(false))
                .Bind(_inputService.ElementsInput, OnProcessInput);
        }

        private void OnProcessInput(PuzzleInputData input)
        {
            var eventType = _settings.showBackgroundEvent;
            if (activated.Value ||
                input.pieceId >= 0 ||
                input.inputType != eventType)
            {
                _timer = -1;
                return;
            }

            var passedTime = _timer > 0 
                ? Time.realtimeSinceStartup - _timer
                : _timer;
            
            if (passedTime > _settings.backgroundShowDelay)
                command.Execute(true);
            
            if (_timer < 0) _timer = Time.realtimeSinceStartup;
        }
    }
}