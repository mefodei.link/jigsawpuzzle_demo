﻿using System;
using System.Collections.Generic;
using Game.Code.JigsawPuzzle.Runtime.Models;
using Game.Code.JigsawPuzzle.Views.PuzzleScrollViews;
using UniGame.Runtime.ObjectPool.Extensions;
using UniModules.UniGame.Core.Runtime.Rx;
using UniRx;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    [Serializable]
    public class PuzzleElementsFilter : IElementsViewFilter
    {
        private readonly IReadOnlyList<IElementsViewFilter> _filters;
        private List<PuzzleElementViewModel> _filterResult = new();

        public PuzzleElementsFilter(IReadOnlyList<IElementsViewFilter> filters)
        {
            _filters = filters;
        }

        public bool IsActive => true;

        public void Filter(List<PuzzleElementViewModel> models)
        {
            if (IsActive == false) return;
            
            _filterResult.Clear();

            foreach (var filter in _filters)
            {
                if(filter.IsActive == false) continue;
                filter.Filter(models);
            }
        }

        public void Dispose()
        {
            
        }
    }
}