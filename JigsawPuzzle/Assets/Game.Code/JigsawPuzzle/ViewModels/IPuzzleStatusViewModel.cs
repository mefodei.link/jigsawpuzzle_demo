﻿using UniGame.ViewSystem.Runtime;
using UniRx;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.Views.GameplayViews
{
    public interface IPuzzleStatusViewModel : IViewModel
    {
        public IReadOnlyReactiveProperty<string> Label { get; }
        public IReadOnlyReactiveProperty<Sprite> Image { get; }
    }
}