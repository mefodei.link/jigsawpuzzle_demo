﻿using System;
using UniGame.UiSystem.Runtime;
using UniModules.UniGame.Core.Runtime.Rx;
using UniRx;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    [Serializable]
    public class ToggleViewModel : ViewModelBase, IToggleButtonViewModel
    {
        public ReactiveCommand<bool> command;
        public RecycleReactiveProperty<bool> enabled;
        public RecycleReactiveProperty<Sprite> icon;
        public RecycleReactiveProperty<string> label;
        
        public ToggleViewModel()
        {
            enabled = new RecycleReactiveProperty<bool>(true).AddTo(LifeTime);
            command = new ReactiveCommand<bool>().AddTo(LifeTime);
            icon = new RecycleReactiveProperty<Sprite>().AddTo(LifeTime);
            label = new RecycleReactiveProperty<string>().AddTo(LifeTime);
        }

        public IReadOnlyReactiveProperty<Sprite> Icon => icon;
        public IReadOnlyReactiveProperty<string> Label => label;
        public IReactiveCommand<bool> Execute => command;
        public IObservable<bool> Enabled => enabled;
    }
    
    
}