﻿using System;
using System.Collections.Generic;
using Game.Code.JigsawPuzzle.Runtime.Models;
using Game.Code.JigsawPuzzle.Views.PuzzleScrollViews;
using UniRx;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    public interface IElementsViewFilter : IDisposable
    {
        bool IsActive { get;  }
        
        void Filter(List<PuzzleElementViewModel> origin);
    }
    
    
}