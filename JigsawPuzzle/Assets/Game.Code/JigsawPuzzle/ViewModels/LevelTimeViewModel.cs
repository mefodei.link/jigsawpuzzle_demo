﻿using System;
using Cysharp.Threading.Tasks;
using Game.Code.JigsawPuzzle.Views.GameplayViews;
using UniGame.Core.Runtime;
using UniGame.UiSystem.Runtime;
using UniModules.UniGame.Core.Runtime.Rx;
using UniRx;
using UnityEngine;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    [Serializable]
    public class LevelTimeViewModel : ViewModelBase,IPuzzleStatusViewModel
    {
        private readonly ILifeTime _levelLifeTime;
        private readonly RecycleReactiveProperty<string> _time;
        private readonly RecycleReactiveProperty<Sprite> _image;

        //todo move into level model
        private float _timePassed;
        
        public LevelTimeViewModel()
        {
            _image = new RecycleReactiveProperty<Sprite>().AddTo(LifeTime);
            _time = new RecycleReactiveProperty<string>().AddTo(LifeTime);
            Update().Forget();
        }

        public IReadOnlyReactiveProperty<string> Label => _time;
        public IReadOnlyReactiveProperty<Sprite> Image => _image;

        /// <summary>
        /// move into service
        /// </summary>
        private async UniTask Update()
        {
            _timePassed = 0f;
            while (LifeTime.IsTerminated == false)
            {
                var timeCounter = Time.timeSinceLevelLoad;
                await UniTask.Delay(TimeSpan.FromSeconds(1));
                _timePassed += Time.timeSinceLevelLoad - timeCounter;
                var timeLabel = TimeSpan.FromSeconds(_timePassed).ToString(@"mm\:ss");
                _time.Value = timeLabel;
            }
        }
    }
}