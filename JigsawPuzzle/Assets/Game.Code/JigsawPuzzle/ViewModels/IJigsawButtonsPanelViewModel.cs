﻿using UniGame.ViewSystem.Runtime;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    public interface IJigsawButtonsPanelViewModel : IViewModel
    {
        IButtonViewModel Home { get; }
        IButtonViewModel Settings { get; }
        IToggleButtonViewModel Corners { get; }
        IToggleButtonViewModel Background { get; }
    }
}