﻿using System.Collections.Generic;
using Game.Code.JigsawPuzzle.Views.PuzzleScrollViews;
using UniGame.ViewSystem.Runtime;
using UniRx;

namespace Game.Code.JigsawPuzzle.ViewModels
{
    public interface IJigsawItemsScrollViewModel : IViewModel
    {
        IReadOnlyList<PuzzleElementViewModel> PuzzleItems { get; }
    }
}