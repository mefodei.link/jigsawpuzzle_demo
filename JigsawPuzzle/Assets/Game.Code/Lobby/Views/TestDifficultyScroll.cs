using System.Collections;
using System.Collections.Generic;
using Game.Code.Lobby.Views;
using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(LevelDifficultyScrollView))]
public class TestDifficultyScroll : MonoBehaviour
{

    public LevelDifficultyScrollView scrollView;

    public int amount = 5;


    private List<LevelSettingsItemModel> data = new List<LevelSettingsItemModel>();

    // Start is called before the first frame update
    private void Awake()
    {
        scrollView = GetComponent<LevelDifficultyScrollView>();
    }

    [Button]
    public void UpdateView()
    {
        data.Clear();
        for (var i = 0; i < amount; i++)
        {
            var selection = Random.Range(0f, 1f) > 0.5f;
            var pieces = i * 10;
            data.Add(new LevelSettingsItemModel()
            {
                amount = pieces,
                duration = pieces,
                selected = selection,
            });
        }
        
        scrollView.UpdateData(data);
    }

}
