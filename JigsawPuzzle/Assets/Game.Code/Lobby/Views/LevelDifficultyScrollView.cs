﻿using UniModules.UniCore.Runtime.DataFlow;

namespace Game.Code.Lobby.Views
{
    using System;
    using System.Collections.Generic;
    using DG.Tweening;
    using UnityEngine;
    using UnityEngine.UIElements;
    
    public class LevelDifficultyScrollView : 
        FancyScrollView.FancyScrollView<LevelSettingsItemModel, LevelSettingsItemView>
    {
        #region inspector

        [SerializeField] FancyScrollView.Scroller scroller = default;
        [SerializeField] LevelCellView cellPrefab = default;
        
        #endregion

        private LifeTimeDefinition _animationLifeTime = new LifeTimeDefinition();
        

        Action<int> onSelectionChanged;

        protected override GameObject CellPrefab => cellPrefab.gameObject;

        protected override void Initialize()
        {
            base.Initialize();

            _animationLifeTime.Release();
            
            Context.OnCellClicked = SelectCell;

            scroller.OnValueChanged(UpdatePosition);
            scroller.OnSelectionChanged(UpdateSelection);
        }

        void UpdateSelection(int index)
        {
            _animationLifeTime.Release();
            
            if (Context.SelectedIndex == index)
            {
                return;
            }

            Context.SelectedIndex = index;
            
            Refresh();

            onSelectionChanged?.Invoke(index);
        }

        public void UpdateData(IList<LevelSettingsItemModel> items)
        {
            UpdateContents(items);
            scroller.SetTotalCount(items.Count);
        }

        public void OnSelectionChanged(Action<int> callback)
        {
            onSelectionChanged = callback;
        }

        public void SelectCell((int index, float position) cellSelection)
        {
            var index = cellSelection.index;
            
            if (index < 0 || index >= ItemsSource.Count || index == Context.SelectedIndex)
            {
                return;
            }

            UpdateSelection(index);

            scroller.ScrollTo(index, 0.1f, EasingCore.Ease.OutCubic);
        }
    }
}
