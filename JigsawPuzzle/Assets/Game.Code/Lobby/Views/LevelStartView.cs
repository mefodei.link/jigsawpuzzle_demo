
using Game.Code.Lobby.Models;
using Sirenix.OdinInspector;

namespace Game.Code.Lobby.Views
{
    using Cysharp.Threading.Tasks;
    using UniGame.Rx.Runtime.Extensions;
    using UniGame.UiSystem.Runtime;
    using UnityEngine.UI;
    using UnityEngine;
    
    public class LevelStartView : UiCanvasGroupView<ILevelStartViewModel>
    {
        #region inspector
        
        public Button playButton;
        public Image levelImage;
        public LevelDifficultyScrollView difficultyView;

        #endregion

        protected override UniTask OnViewInitialize(ILevelStartViewModel model)
        {
            this.Bind(playButton, model.LoadGameMode)
                .Bind(model.LevelImage,levelImage);
            
            difficultyView.UpdateData(model.Presets);
            difficultyView.SelectCell((0,0f));
            
            return UniTask.CompletedTask;
        }
    }
}
