﻿using DG.Tweening;
using FancyScrollView;
using TMPro;
using UniModules.UniCore.Runtime.DataFlow;
using UniModules.UniGame.DoTweenRoutines.Runtime;
using UniModules.UniUiSystem.Runtime.Utils;
using UniRx;

namespace Game.Code.Lobby.Views
{
    using UnityEngine;
    using UnityEngine.UI;
    
    public class LevelCellView : FancyCell<LevelSettingsItemModel,LevelSettingsItemView>
    {
        #region inspector

        [SerializeField] public TextMeshProUGUI amount;
        [SerializeField] public TextMeshProUGUI duration;
        [SerializeField] public Button button = default;
        [SerializeField] public RectTransform container;
        [SerializeField] public Image image = default;
        [SerializeField] public Image selection;
        
        [SerializeField] private Vector2 anchorMax = new Vector2(1.3f, 0.5f);
        [SerializeField] private Vector2 anchorMin = new Vector2(-1.3f, 0.5f);
        [SerializeField] private Vector2 pivotOffset =new Vector2(0.5f, 0f);
        [SerializeField] private Vector2 alphaRange = new Vector2(0f, 1f);
        
        [SerializeField] private Vector2 scaleRange = new Vector2(1f, 1.2f);
        [SerializeField] private float scaleDuration = 0.2f;
        
        
        #endregion

        private float _currentPosition = 0;
        private LifeTimeDefinition _lifeTime = new LifeTimeDefinition();
        private LifeTimeDefinition _scaleAnimationLifeTime = new LifeTimeDefinition();
        private bool _selected;
        private Vector3 _selectedScale;
        private Vector3 _normalScale;

        static class AnimatorHash
        {
            public static readonly int Scroll = Animator.StringToHash("scroll");
        }

        public override void Initialize()
        {
            _lifeTime.Release();
            _scaleAnimationLifeTime.Release();

            button.OnClickAsObservable()
                .Subscribe(x => Context.OnCellClicked?.Invoke((Index,_currentPosition)))
                .AddTo(_lifeTime);
        }

        public override void UpdateContent(LevelSettingsItemModel settingsItemData)
        {
            var selected = Context.SelectedIndex == Index;
            var isSelectionChanged = selected != _selected;
            _selected = selected;
            settingsItemData.selected = _selected;
            amount.SetValue(settingsItemData.amount);
            selection.enabled = _selected;
            duration.SetValue(settingsItemData.duration);

            if (isSelectionChanged)
            {
                _scaleAnimationLifeTime.Release();
                
                container.transform
                    .DOScale(selected ? _selectedScale : _normalScale, scaleDuration)
                    .SetEase(Ease.InBounce)
                    .AddTo(_scaleAnimationLifeTime);
            }
        }

        public override void UpdatePosition(float position)
        {
            _currentPosition = position;

            var lerpPosition = Vector2.Lerp(anchorMin, anchorMax, position) + pivotOffset;

            container.anchorMax = lerpPosition;
            container.anchorMin = lerpPosition;
        }

        private void OnEnable()
        {
            _selectedScale = new Vector3(scaleRange.y, scaleRange.y, scaleRange.y);
            _normalScale = new Vector3(scaleRange.x, scaleRange.x, scaleRange.x);
            
            UpdatePosition(_currentPosition);
            
            var assetLifeTime = this.GetLifeTime();
            _lifeTime.AddTo(assetLifeTime);
            _scaleAnimationLifeTime.AddTo(assetLifeTime);
        }
        
    }
}
