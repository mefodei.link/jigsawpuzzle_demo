﻿using System;
using System.Collections.Generic;
using Game.Code.Lobby.Views;
using UniGame.ViewSystem.Runtime;
using UniRx;
using UnityEngine;

namespace Game.Code.Lobby.Models
{
    public interface ILevelStartViewModel : IViewModel
    {
        IObservable<Sprite> LevelImage { get; }
        
        List<LevelSettingsItemModel> Presets { get; }
        
        ReactiveCommand LoadGameMode { get; }
    }

    public interface ILevelPresetViewModel
    {
        
        
        
        
    }
    
}