using System;
using System.Collections.Generic;
using Game.Code.Lobby.Views;
using UniModules.UniGame.Core.Runtime.Rx;
using UniRx;
using UnityEngine;

namespace Game.Code.Lobby.Models
{
    using UniGame.UiSystem.Runtime;
    
    [Serializable]
    public class LevelStartViewModel : ViewModelBase, ILevelStartViewModel
    {
        public string id;
        public ReactiveCommand loadGameMode;
        
        public List<LevelSettingsItemModel> presets = new List<LevelSettingsItemModel>();
        public RecycleReactiveProperty<Sprite> image;
        
        public LevelStartViewModel()
        {
            loadGameMode = new ReactiveCommand().AddTo(LifeTime);
            image = new RecycleReactiveProperty<Sprite>().AddTo(LifeTime);
        }

        public IObservable<Sprite> LevelImage => image;
        
        public ReactiveCommand LoadGameMode => loadGameMode;

        public List<LevelSettingsItemModel> Presets => presets;

    }
}
