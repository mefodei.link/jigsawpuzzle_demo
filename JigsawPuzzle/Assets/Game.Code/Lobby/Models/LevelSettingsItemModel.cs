﻿using System;

namespace Game.Code.Lobby.Views
{
    [Serializable]
    public class LevelSettingsItemModel
    {
        public int id;
        public int amount;
        public int duration;
        public bool selected;
    }
}
