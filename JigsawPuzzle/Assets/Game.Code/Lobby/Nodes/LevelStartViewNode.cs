using System.Collections.Generic;
using System.Linq;
using Game.Code.Game.LevelService;
using Game.Code.Game.StateService;
using Game.Code.Lobby.Models;
using Game.Code.Lobby.Views;
using UniGame.AddressableTools.Runtime;
using UniRx;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game.Code.Lobby.Nodes
{
    using System;
    using Sirenix.OdinInspector;
    using UniGame.ViewSystem.Runtime;
    using Cysharp.Threading.Tasks;
    using UniGame.Context.Runtime.Extension;
    using UniGame.Core.Runtime;
    using UniGame.UniNodes.Nodes.Runtime.Common;
    using UniModules.UniGameFlow.NodeSystem.Runtime.Core.Attributes;
    
    [Serializable]
    [CreateNodeMenu("Lobby/Views/LevelStart")]
    public class LevelStartViewNode : SContextNode
    {
        [ValueDropdown(nameof(GetStates))]
        public string nextGameMode;

        protected override async UniTask<bool> OnContextActivate(IContext context)
        {
            var gameStateService = await context
                .ReceiveFirstAsync<IGameStateService>(LifeTime);
            var viewService = await context
                .ReceiveFirstAsync<IGameViewSystem>(LifeTime);
            
            var levelsService = await context.ReceiveFirstAsync<ILevelsService>(LifeTime);

            var viewModel = await CreateLevelStartModel(levelsService);
            
            viewModel.LoadGameMode
                .Subscribe(x => 
                    LoadNewState(gameStateService,levelsService,viewModel).Forget())
                .AddTo(LifeTime);

            var view = await viewService.OpenWindow<LevelStartView>(viewModel);

            return true;
        }

        private async UniTask<LevelStartViewModel> CreateLevelStartModel(ILevelsService levelsService)
        {
            var presetsData = levelsService.GetLevelPresets();
            var level = await levelsService.GetLevelAsync(string.Empty);
            var levelStartModel = new LevelStartViewModel().AddTo(LifeTime);
            var items = levelStartModel.presets;
            var sprite = await level.SpriteReference.LoadAssetTaskAsync(LifeTime);

            levelStartModel.id = string.Empty;
            
            for (var i = 0; i < presetsData.Difficulties.Count; i++)
            {
                var preset = presetsData.Difficulties[i];
                items.Add(new LevelSettingsItemModel()
                {
                    id = i,
                    amount = preset.Columns * preset.Rows,
                    duration = preset.Duration,
                    selected = i == 0,
                });
            }

            levelStartModel.image.Value = sprite;

            return levelStartModel;

        }

        private async UniTask LoadNewState(
            IGameStateService service,
            ILevelsService levelsService,
            LevelStartViewModel levelStartViewModel)
        {
            var selectedPreset = levelStartViewModel.presets
                .FirstOrDefault(x => x.selected);
            var presetId = selectedPreset?.id ?? 0;
            levelsService.ActivateLevelAsync(levelStartViewModel.id, presetId);

            await service.Load(nextGameMode);
        }

        public static IEnumerable<string> GetStates() => GameStateKeys.GetStates();
    }
}
