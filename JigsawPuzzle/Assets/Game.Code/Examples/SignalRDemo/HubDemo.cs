using System;
using Cysharp.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using TMPro;
using UniCore.Runtime.ProfilerTools;
using UnityEngine;

namespace Game.Code.Examples.SignalRDemo
{
    public class HubDemo : MonoBehaviour,IDisposable
    {
        public TextMeshProUGUI hubMessage;
        public string hubAddress = "https://localhost:7219/demohub";
        public float connectionDelay = 2f;
        public bool connected = false;
        
        private HubConnection _connection;
        
        // Start is called before the first frame update
        private void Start()
        {
            Execute().Forget();
        }

        public async UniTask Execute()
        {
            Debug.Log("Execute start");
            while (_connection == null)
            {
                await UniTask.Delay(TimeSpan.FromSeconds(connectionDelay));
                
                try
                {
                    _connection = CreateConnection();
                }
                catch (Exception e)
                {
                   Debug.LogException(e);
                }
            }
            
            _connection.On<string, string>("ReceiveMessage", (user, message) =>
            {
                LogAsync($"{user}: {message}").Forget();
            });
            
            Debug.Log("_connection ref created");
                        
            while (connected == false)
            {
                try
                {
                    Debug.Log("start connection");
                    await UniTask.SwitchToMainThread();
                    await _connection.StartAsync();
                    await UniTask.SwitchToMainThread();
                    
                    connected = true;
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }

        }

        private HubConnection CreateConnection()
        {
            var hubConnection = new HubConnectionBuilder()
                .WithUrl(hubAddress)
                .Build();
            return hubConnection;
        }

        public void Dispose()
        {
            _connection.DisposeAsync();
        }
        
        private void OnDisable()
        {
            Dispose();
        }

        private async UniTask LogAsync(string message)
        {
            await UniTask.SwitchToMainThread();
            GameLog.Log(message,Color.yellow);
            
            if(hubMessage!= null)
                hubMessage.text = message;
        }
    }
}
