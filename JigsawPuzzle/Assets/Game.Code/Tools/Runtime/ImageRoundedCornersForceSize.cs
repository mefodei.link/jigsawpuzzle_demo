﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Nobi.UiRoundedCorners {
	[ExecuteInEditMode]
	[RequireComponent(typeof(RectTransform))]
	public class ImageRoundedCornersForceSize : MonoBehaviour {
		private static readonly int Props = Shader.PropertyToID("_WidthHeightRadius");

		public float radius;

		public bool useForceSize = false;
		
		public float width;
		public float height;
		
		private Material material;

		[HideInInspector, SerializeField] private Image image;

		private void OnValidate() {
			Validate();
			Refresh();
		}

		private void OnDestroy() {
			DestroyItem(material);
			image = null;
			material = null;
		}

		private void OnEnable() {
			Validate();
			Refresh();
		}

		private void OnRectTransformDimensionsChange() {
			if (enabled && material != null) {
				Refresh();
			}
		}

		public void Validate() {
			if (material == null) {
				material = new Material(Shader.Find("UI/RoundedCorners/RoundedCorners"));
			}

			if (image == null) {
				TryGetComponent(out image);
			}

			if (image != null) {
				image.material = material;
			}
		}

		[Button]
		public void Refresh() {
			var rect = ((RectTransform)transform).rect;
			var parameter = useForceSize 
					? new Vector4(width, height, radius, 0)
					: new Vector4(rect.width, rect.height, radius, 0);
			
			material.SetVector(Props, parameter);
		}
		
		private static void DestroyItem(Object asset) {
#if UNITY_EDITOR
			if (Application.isPlaying) {
				Destroy(asset);
			} else {
				DestroyImmediate(asset);
			}
#else
		Object.Destroy(asset);
#endif
		}
	}
}