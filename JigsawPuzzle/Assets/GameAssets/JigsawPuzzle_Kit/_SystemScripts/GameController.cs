﻿//-----------------------------------------------------------------------------------------------------	
// Script controls whole gameplay, UI and all sounds
//-----------------------------------------------------------------------------------------------------	

using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;


[AddComponentMenu("Scripts/Jigsaw Puzzle/Game Controller")]
public class GameController : MonoBehaviour
{
    public const string CameraGroupName = "Camera Settings";

    [BoxGroup(CameraGroupName)] public Camera gameCamera;

    // Automatically align puzzle/camera with screen corners to fit puzzle with max filling
    [BoxGroup(CameraGroupName)] public ScreenAnchor fitToScreenAnchor = ScreenAnchor.None;

    // Automatically align puzzle/background with camera center
    [BoxGroup(CameraGroupName)] public bool alignWithCamera = false;

    [BoxGroup(CameraGroupName)] public Vector3 cameraPositionOffset = Vector3.one;

    [BoxGroup(CameraGroupName)] public float cameSizeMultiplier = 1;

    public PuzzleController puzzle;

    public bool findByTag = false;

    // Background (assembled puzzle preview)
    public Renderer background;

    /*public bool adjustBackground = true;*/
    public float backgroundTransparency = 0.1f;

    // Game rules
    public float timer; // Time limit for level
    public int hintLimit = -1; // Hints limit for level
    public bool invertRules = false; // Allows to invert basic rules - i.e. player should decompose  the images


    // Important internal variables - please don't change them blindly
    float remainingTime, elapsedTime;
    bool gameFinished = false;
    Color backgroundColor;
    static Vector3 oldPointerPosition;

    
    public float Transparency => background == null ? 0 : background.material.color.a;


    //=====================================================================================================
    // Initialize
    private void OnEnable()
    {
        // Prepare Camera
        if (!gameCamera)
            gameCamera = Camera.main;

        gameCamera.orthographic = true;

        // Try to automatically find/assign puzzle and background by tags
        if (findByTag)
        {
            var foundObject = GameObject.FindGameObjectWithTag("Puzzle_Main");
            if (foundObject)
                puzzle = foundObject.GetComponent<PuzzleController>();

            foundObject = GameObject.FindGameObjectWithTag("Puzzle_Background");
            if (foundObject)
                background = foundObject.GetComponent<Renderer>();
        }
    }

    public void AlignCamera()
    {
        // Align with Camera if needed
        if (alignWithCamera)
            puzzle.AlignWithCameraCenter(
                gameCamera,
                (puzzle.anchoring == PuzzleAnchor.Center),
                cameraPositionOffset,
                true,
                cameSizeMultiplier);

        // Align with screen corners
        if (fitToScreenAnchor != ScreenAnchor.None)
        {
            puzzle.FitToScreen(gameCamera, fitToScreenAnchor,cameraPositionOffset,Vector2.zero);
            //cameraScript?.ReInit();
        }
    }

    [Button]
    public void FitToCamera()
    {
        if (fitToScreenAnchor != ScreenAnchor.None)
        {
            puzzle.FitToScreen(gameCamera, fitToScreenAnchor,cameraPositionOffset,Vector2.zero);
        }
    }
    
    //-----------------------------------------------------------------------------------------------------	 
    // Get current pointer(mouse or single touch) position  
    public static Vector3 GetPointerPosition(Camera _camera)
    {
        var pointerPosition = oldPointerPosition;

        // For mobile/desktop
        if (Input.touchCount > 0)
            pointerPosition = oldPointerPosition = _camera.ScreenToWorldPoint(Input.GetTouch(0).position);
        else
            pointerPosition = oldPointerPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
        
        return pointerPosition;
    }

    //-----------------------------------------------------------------------------------------------------	 
    // Get current rotation basing on mouse or touches
    float GetRotationDirection()
    {
        float rotation = 0;

        // For Desktop - just set rotation to "clockwise" (don't change the permanent speed)
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_WEBGL
        if (Input.GetMouseButton(1))
            rotation = 1;
#else // For mobile - calculate angle changing between touches and use it.
        if(puzzle.gradualRotation)
		   foreach(Touch touch in Input.touches) 
		   {
			 if (touch.tapCount == 2) 
				  rotation = 1;	
		   } 
		  else
			if (Input.touchCount > 1)  
				{
						// If there are two touches on the device... Store both touches.
						Touch touchZero = Input.GetTouch (0);
						Touch touchOne = Input.GetTouch (1);

						// Find the angle between positions.
						float currentAngle = Vector2.SignedAngle(touchZero.position, touchOne.position); 
						float previousAngle =
 Vector2.SignedAngle(touchZero.position - touchZero.deltaPosition, touchOne.position - touchOne.deltaPosition);

						rotation = currentAngle - previousAngle;
				}
                 //Alternative (sign/direction based):  // rotation = (int)Mathf.Sign(Vector2.SignedAngle(Vector2.up, Input.GetTouch(1).position-Input.GetTouch(0).position));
#endif

        return rotation;
    }

    //-----------------------------------------------------------------------------------------------------	 
    // Switch puzzle and background to another
    public void SwitchPuzzle(PuzzleController _puzzle, Renderer _background = null)
    {
        if (_puzzle && _puzzle != puzzle)
            StartPuzzle(_puzzle);

        if (_background && _background != background)
            PrepareBackground(_background);
    }

    //-----------------------------------------------------------------------------------------------------	 
    // Prepare puzzle and Decompose it if needed
    public bool StartPuzzle(PuzzleController _puzzle)
    {
        if (!_puzzle)
            _puzzle = gameObject.GetComponent<PuzzleController>();

        if (!_puzzle)
        {
            Debug.LogWarning(
                "<b>PuzzleKit::GameController:</b> <i>PuzzleController</i> should be assigned to <i>puzzle</i> property - please check " +
                gameObject.name, gameObject);
            return false;
        }


        if (puzzle.pieces == null || puzzle.pieces.Length == 0)
            puzzle.Prepare();


        if (puzzle && puzzle != _puzzle)
            puzzle.gameObject.SetActive(false);

        puzzle = _puzzle;
        puzzle.gameObject.SetActive(true);
        puzzle.DecomposePuzzle();
        puzzle.invertedRules = invertRules;
        gameFinished = false;
        return true;
    }

    //-----------------------------------------------------------------------------------------------------	 
    // Show background (assembled puzzle)
    [Button]
    void ShowBackground()
    {
        if (background && backgroundColor.a < 1)
        {
            backgroundColor.a = Mathf.Lerp(backgroundColor.a, 1.0f, Time.deltaTime);
            background.material.color = backgroundColor;
        }
    }

    //-----------------------------------------------------------------------------------------------------	 
    // Prepare background (assembled puzzle)
    public void PrepareBackground(Renderer _background = null)
    {
        // Try to get BG from first puzzle child        
        if (!_background && puzzle)
        {
            var tmp = puzzle.thisTransform.GetChild(0);
            if (!_background && puzzle && tmp.tag == "Puzzle_Background")
                _background = tmp.GetComponent<Renderer>();
        }


        // Adjust background
        if (_background)
        {
            if (background)
                background.gameObject.SetActive(false);

            background = _background;
            background.gameObject.SetActive(true);

            backgroundColor = background.material.color;

            if (backgroundTransparency < 1.0f)
            {
                backgroundColor.a = backgroundTransparency;
                background.material.color = backgroundColor;
            }

            /* if (adjustBackground)
                 AdjustBackground(); */
        }
        else
            background = null;
    }

#if ODIN_INSPECTOR
    [Sirenix.OdinInspector.Button]
#endif
    public void ApplyBackgroundTransparency(float transparency)
    {
        if (background == null) return;
        
        backgroundColor.a = transparency;
        background.material.color = backgroundColor;
    }

    [Serializable]
    public class AlignBackgroundSettings
    {
        public float cameraSizeTuner = 1f;
        public bool alignByWidth = true;
    }

    [BoxGroup("align background")]
    [HideLabel]
    [InlineProperty]
    public AlignBackgroundSettings alignSettings = new AlignBackgroundSettings();
    
    //-----------------------------------------------------------------------------------------------------	
    // Automatically align camera with background
    [Button]
    public void AlignCameraWithBackground()
    {
        if (alignSettings.alignByWidth)
        {
            if (gameCamera.aspect > 1)
                gameCamera.orthographicSize = background.bounds.size.x / (gameCamera.aspect * 2) + alignSettings.cameraSizeTuner;
            else
                gameCamera.orthographicSize = background.bounds.size.x - gameCamera.aspect + alignSettings.cameraSizeTuner;
        }
        else
        {
            if (gameCamera.aspect > 1)
                gameCamera.orthographicSize = background.bounds.size.y / (gameCamera.aspect * 2) + alignSettings.cameraSizeTuner;
            else
                gameCamera.orthographicSize = background.bounds.size.y - gameCamera.aspect + alignSettings.cameraSizeTuner;

        }
    }

    //-----------------------------------------------------------------------------------------------------	
    // Adjust background to puzzle
    [Button]
    public void AdjustBackground()
    {
        if (background /*&&  background.transform.parent != puzzle.transform*/)
        {
            background.transform.parent = puzzle.transform;
            background.transform.SetAsFirstSibling();
            background.transform.localPosition = new Vector3(0, 0, 0.2f);


            // Try to adjust background size according to puzzle bounds
            if (background as SpriteRenderer)
            {
                // Temporarily reset Puzzle rotation 
                var tmpRotation = puzzle.transform.rotation;
                puzzle.transform.localRotation = Quaternion.identity;

                // Reset background transform
                background.transform.localRotation = Quaternion.identity;
                background.transform.localScale = Vector3.one;


                // Calculate background scale  to make it the same size as puzzle
                background.transform.localScale = new Vector3(puzzle.puzzleBounds.size.x / background.bounds.size.x,
                    puzzle.puzzleBounds.size.y / background.bounds.size.y, background.transform.localScale.z);

                // Aligned background position
                //background.transform.position = new Vector3(puzzle.puzzleBounds.min.x, puzzle.puzzleBounds.max.y, background.transform.position.z);

                // Shift background if it's origin not in LeftTop corner 		 			 	
                if (Mathf.Abs(background.bounds.min.x - puzzle.puzzleBounds.min.x) > 1 ||
                    Mathf.Abs(background.bounds.max.y - puzzle.puzzleBounds.max.y) > 1)
                    background.transform.localPosition = new Vector3(
                        background.transform.localPosition.x - background.bounds.extents.x,
                        background.transform.localPosition.y + background.bounds.extents.y,
                        background.transform.localPosition.z);

                // Return proprer puzzle rotation
                puzzle.transform.localRotation = tmpRotation;
            }
        }
    }

    //-----------------------------------------------------------------------------------------------------	 
    // Reset current puzzle
    [Button]
    public void ResetPuzzle()
    {
        if (puzzle == null)
            return;

        Time.timeScale = 0;

        puzzle.ResetProgress();

        PlayerPrefs.SetInt(puzzle.name + "_hints", hintLimit);
        PlayerPrefs.SetFloat(puzzle.name + "_timer", timer);

        puzzle.DecomposePuzzle();

        Time.timeScale = 1.0f;
    }


    //-----------------------------------------------------------------------------------------------------	 
    // Restart current puzzle
    [Button]
    public void RestartPuzzle()
    {
        if (puzzle != null)
        {
            PlayerPrefs.SetString(puzzle.name, "");
            PlayerPrefs.DeleteKey(puzzle.name + "_Positions");
        }

        if (background && !invertRules)
        {
            puzzle.SetPiecesActive(true);

            if (backgroundTransparency < 1.0f)
            {
                backgroundColor.a = backgroundTransparency;
                background.material.color = backgroundColor;
            }
        }

        gameFinished = false;
        ResetPuzzle();
    }


}