﻿using System;
using System.Collections.Generic;
using Game.Code.JigsawPuzzle.Runtime.Models;
using UnityEngine;

namespace JigsawPuzzle_Kit._SystemScripts
{
    [Serializable]
    public class PuzzleSaveData
    {
        public string levelId;
        public List<PuzzleElementSaveData> elements = new List<PuzzleElementSaveData>();
    }

    [Serializable]
    public class PuzzleElementSaveData
    {
        public int id;
        public Vector3 position;
        public bool movable;
        public PuzzleElementState state;
        public float angle;
    }
}