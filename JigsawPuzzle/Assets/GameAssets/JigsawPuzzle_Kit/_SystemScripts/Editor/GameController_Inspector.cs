﻿//----------------------------------------------------------------------------------------------------------------------------------------------------------
// Provides with custom and more convenient Inspector GUI for GameController
//----------------------------------------------------------------------------------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEditor.SceneManagement;



//[CustomEditor(typeof(GameController))]
public class GameController_Inspector : Editor 
{
	
	// Important internal variables
	GameController game;
	bool showRules;
	bool showMusic;
	bool showSound;
	bool showGameUI;
	bool showBackground;


	//========================================================================================================================================================== 
	// Draw whole custom inspector GUI
	public override void OnInspectorGUI()
	{   
		game = (GameController)target;

		// General
		game.gameCamera = EditorGUILayout.ObjectField(new GUIContent("Game Camera", "Link to Camera to be used for puzzle (leave empty to try to use Camera.main)"), game.gameCamera , typeof(Camera), true) as Camera;

        game.findByTag = EditorGUILayout.Toggle(new GUIContent("Find puzzle by Tag", "Will try to automatically find/assign puzzle and background by tags"), game.findByTag);

        if (!game.findByTag)
            game.puzzle = EditorGUILayout.ObjectField(new GUIContent("Puzzle", "Link to PuzzleController to be processed (leave empty to try to use PuzzleManager attached to this object)"), game.puzzle, typeof(PuzzleController), true) as PuzzleController;


        if (game.fitToScreenAnchor == ScreenAnchor.None)
            game.alignWithCamera = EditorGUILayout.Toggle(new GUIContent("Align with camera", "Automatically align puzzle/background with camera center"), game.alignWithCamera);

        if (!game.alignWithCamera)
            game.fitToScreenAnchor = (ScreenAnchor)EditorGUILayout.EnumPopup(new GUIContent("Fit To Screen anchor", "Automatically align puzzle/camera with screen corners to fit puzzle with max filling"), game.fitToScreenAnchor);


        // Game rules
        showRules = EditorGUILayout.Foldout(showRules, new GUIContent("GAME  RULES", "Contains settings game-rules settings"));
		if(showRules)
		{
			GUILayout.BeginVertical("box");
			game.timer  = EditorGUILayout.FloatField(new GUIContent("Time limit", "Set time limit for level (-1 to disable)" ), game.timer);
			game.hintLimit  = EditorGUILayout.IntField(new GUIContent("Hints limit", "Set hints limit for level (-1 ro disable)" ), game.hintLimit);
			game.invertRules = EditorGUILayout.Toggle(new GUIContent("Invert rules", "Invert basic rules - i.e. player should decompose  the images"), game.invertRules);
			GUILayout.EndVertical();
		}

		// Background-related (assembled puzzle preview)
		showBackground = EditorGUILayout.Foldout(showBackground, new GUIContent("BACKGROUND  SETTINGS", "Contains settings related to Background (preview of assembled puzzle)"));
		if(showBackground)
		{
			GUILayout.BeginVertical("box");
			    game.background = EditorGUILayout.ObjectField(new GUIContent("Renderer", "Link to background(preview of assembled puzzle) object renderer"), game.background, typeof(Renderer), true) as Renderer;
			    game.backgroundTransparency = EditorGUILayout.FloatField(new GUIContent("Transparency", "Set background transparency" ), game.backgroundTransparency);
                //game.adjustBackground = EditorGUILayout.Toggle(new GUIContent("Auto-adjusting", "Try to automatically adjust background transform to the puzzle (can fails for complex cases)") , game.adjustBackground);       
                if (GUILayout.Button(new GUIContent("Adjust to puzzle", "Try to automatically adjust background to the puzzle (can fails for complex cases)")))
                    game.AdjustBackground();
            GUILayout.EndVertical();
		} 


		// SetDirty if changed and update SceneView
		if (!Application.isPlaying  &&  GUI.changed) 
			EditorSceneManager.MarkSceneDirty(game.gameObject.scene);
		
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------
}