﻿using System;

namespace Game.Code.JigsawPuzzle.Runtime.Models
{
    [Serializable]
    public enum PuzzleElementState : byte 
    {
        Hidden,
        InGame,
        Assembled,
        InGroup,
        ReadyToDrop,
        Dragging,
        None,
    }
}