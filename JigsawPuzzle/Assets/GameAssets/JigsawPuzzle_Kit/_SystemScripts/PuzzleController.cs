﻿//----------------------------------------------------------------------------------------------------------------------------------------------------------
// Main script to prepare, control whole puzzle. Also processes decomposition and user input (like pieces movement)
//----------------------------------------------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using Game.Code.JigsawPuzzle.Runtime.Models;
using JigsawPuzzle_Kit._SystemScripts;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;


// List of basic puzzle state to process
public enum PuzzleState
{
    None,
    DragPiece,
    ReturnPiece,
    DropPiece,
    RotatePiece,
    PuzzleAssembled
}

public enum PuzzleAnchor
{
    TopLeft,
    Center
};

public enum ScreenAnchor
{
    None,
    TopLeft,
    Center,
    CenterRight,
    CenterLeft,
    BottomLeft,
    BottomRight,
    TopRight,
};


[AddComponentMenu("Scripts/Jigsaw Puzzle/Puzzle Controller")]
public partial class PuzzleController : MonoBehaviour
{
    // Position/rotation of all pieces will be saved/restored 
    public bool enablePositionSaving = true;

    // Use pieces grouping (please don't use for imported and 3D puzzles)
    public bool enablePiecesGroups = true;

    // Allow to assemble puzzle in any place as a group
    public bool assembleInAnyPlace = false;

    // Should pieces be rotated during decomposition
    public bool randomizeRotation = false;

    // Pieces willn't be moved during decomposition (only rotated)
    public bool changeOnlyRotation = false;


    // Allow shifting(including at decomposition) pieces in all 3 dimensions, Require them to be strictly in 3D place for assembling
    public bool fullyIn3D = false;

    public PuzzleElement[] puzzleGrid;

    // Set where the puzzle object pivot(center) should be
    public PuzzleAnchor anchoring;

    // Sides (around puzzle) where pieces should be moved during decomposition
    public bool decomposeToLeft = true;
    public bool decomposeToRight = true;
    public bool decomposeToTop;
    public bool decomposeToBottom;

    // Decomposition area size and offset
    public Vector3 horizontalAreasSize = new Vector3(5, 5, 5);
    public bool autoHorizontalAreaOffset = true;
    public Vector3 horizontalAreaOffset = new Vector3(1, 0, 1);

    public Vector3 verticalAreasSize = new Vector3(5, 5, 5);
    public bool autoVerticalAreaOffset = true;
    public Vector3 verticalAreaOffset = new Vector3(0, 1, 1);

    public Vector2 imageScreenSize;

    // Pieces movement/rotation properties
    public float movementTime = 0.1f;
    public float rotationSpeed = 90;
    public bool gradualRotation = false;
    public float dragOffsetZ = 0.0001f;
    public float dragTiltSpeed = 2;
    public float mobileDragOffsetY = 0.5f;

    // When piece is dragged it center will always be under the pointer, otherwise - wherever it was relative to the pointer at drag start
    public bool centerDraggedPiece = true;

    // Allows to enable alternative piece dragging behaviour - after a click piece will be anchored to the pointer until the player clicks again
    public bool alternativeDragging = false;


    // Allowed position/rotation offset to consider piece placed to it origin
    public float allowedDistance = 0.5f;
    public float allowedRotation = 10;
    public float groupingDistance = 0.5f;

    // Change transparency for assembled pieces
    // public float finalTransparency = 0.7f;

    public bool invertedRules;

    // Pieces position will be shuffled, but not moved during decomposition 
    public bool swapPuzzleMode = false;

    public Sprite puzzleSprite;
    
    // Important internal variables - please don't change them blindly    //[HideInInspector] 
    public PuzzlePiece[] pieces;
    public Bounds puzzleBounds;
    public Material pieceMaterial_assembled;
    public Transform thisTransform;
    public PuzzleState state;


    public int remainingPieces
    {
        get { return movedPieces.Count; }
    }

    List<int> movedPieces = new List<int>();
    List<PuzzlePiece> overlappedPieces = new List<PuzzlePiece>();
    List<int> ungroupedPieces = new List<int>();

    Vector3 pieceCenterOffset;
    Vector2 oldPointerPosition;

    int currentPiece = -1;
    int swapPiece = -1;
    Transform currentObjectTransform;
    PuzzlePieceGroup currentGroup = null;
    GameObject currentObject;
    float timeToRotate;
    float grabTime;


    //===================================================================================================== 
    // Prepare puzzle for game
    public void Prepare()
    {
        if (invertedRules)
            enablePiecesGroups = false;

        thisTransform = transform;

        // If there's no Material for pieces assembled state - use normal state material
        if (!pieceMaterial_assembled)
            pieceMaterial_assembled = thisTransform
                .GetChild(0)
                .GetComponent<Renderer>().sharedMaterial;

        // If there's a background included as a child
        var startChildOffset = 0;
        if (thisTransform.childCount > 0 && thisTransform.GetChild(0).tag == "Puzzle_Background") startChildOffset = 1;

        // Gather children as puzzle pieces
        pieces = new PuzzlePiece[thisTransform.childCount - startChildOffset];
        
        for (var i = 0; i < pieces.Length; i++)
        {
             var element = new PuzzlePiece(i, thisTransform.GetChild(i + startChildOffset), pieceMaterial_assembled);
             pieces[i] = element;
        }


        // Check if everyrhing allright
        if (pieces == null || pieces.Length < 0)
        {
            Debug.LogWarning("In some reason the amount of pieces is 0!", gameObject);
            return;
        }

        // Calculate bounds
        CalculateBounds();

        // Calculate decomposition areas offset as half of piece size
        if (autoHorizontalAreaOffset)
        {
            horizontalAreaOffset.x = pieces[0].renderer.bounds.size.x / 2;
            horizontalAreaOffset.y = 0;
        }

        if (autoVerticalAreaOffset)
        {
            verticalAreaOffset.y = pieces[0].renderer.bounds.size.y / 2;
            verticalAreaOffset.x = 0;
        }

        gameObject.tag = "Puzzle_Main";
    }


    //-----------------------------------------------------------------------------------------------------	
    // Process puzzle during gameplay (including user input)
    public PuzzleState ProcessPuzzle(Vector3 _pointerPosition,
        bool _dragInput, float _rotationDirection, int forcePieceId = -1)
    {
        if (IsAssembled())
            return PuzzleState.PuzzleAssembled;
        state = PuzzleState.None;

        _dragInput |= (_rotationDirection != 0);

        // Check is any piece clicked and get it Id 
        if (_dragInput && currentPiece < 0)
        {
            if (Time.time < (grabTime + 0.1f)) return PuzzleState.None;

            _pointerPosition.z = 0;
            currentPiece = forcePieceId < 0
                ? GetPointedPieceId(_pointerPosition, true)
                : forcePieceId;

            if (currentPiece >= 0)
            {
                if (enablePiecesGroups)
                {
                    currentObject =
                        PuzzlePieceGroup.GetGroupObjectIfPossible(pieces[currentPiece].transform.gameObject);
                    currentObjectTransform = currentObject.transform;
                    currentGroup = currentObject.GetComponent<PuzzlePieceGroup>();
                }
                else
                {
                    currentObject = pieces[currentPiece].transform.gameObject;
                    currentObjectTransform = pieces[currentPiece].transform;
                }

                // Piece center position offset from pointer (don't work for groups)
                if (currentGroup)
                    pieceCenterOffset = Vector2.zero;
                else if (centerDraggedPiece)
                    pieceCenterOffset = pieces[currentPiece].GetPieceCenterOffset();
                else
                    pieceCenterOffset = new Vector2(_pointerPosition.x - pieces[currentPiece].transform.position.x,
                        _pointerPosition.y - pieces[currentPiece].transform.position.y);


                // Add Drag offset 
                if (!changeOnlyRotation && !swapPuzzleMode)
                    currentObjectTransform.position = new Vector3(_pointerPosition.x - pieceCenterOffset.x,
                        _pointerPosition.y - pieceCenterOffset.y, currentObjectTransform.position.z - dragOffsetZ);

                state = PuzzleState.DragPiece;
                grabTime = Time.time;
            }
        }
        else if (swapPuzzleMode && Input.GetMouseButtonDown(0) && swapPiece < 0)
        {
            // Debug.Log(swapPiece);
            swapPiece = GetPointedPieceId(_pointerPosition, true);

            if (swapPiece >= 0 && swapPiece != currentPiece)
            {
                var tmpPos = pieces[currentPiece].transform.localPosition;

                MovePiece(currentPiece, pieces[swapPiece].transform.localPosition, true, movementTime / 2);
                MovePiece(swapPiece, tmpPos, true, movementTime / 2);

                currentObject = null;
                currentPiece = -1;
                swapPiece = -1;
                grabTime = Time.time;
                return PuzzleState.DropPiece;
            }
            else
                swapPiece = -1;
        }


        // If no piece is grabbed - RETURN
        if (currentObject == null)
        {
            return PuzzleState.None;
        }
        else
            // Check alternative drag
        if (alternativeDragging)
            if (Time.time > (grabTime + 0.1f))
                _dragInput = !_dragInput;
            else
                _dragInput = true;


        // Pointer position offset for mobile or rotation for desktop
        if (Input.touchCount > 0)
            _pointerPosition.y += pieces[currentPiece].renderer.bounds.size.y * mobileDragOffsetY;
        else if (changeOnlyRotation)
            _rotationDirection = 1;

        // Set currentObject center position  to pointerPosition
        if (!changeOnlyRotation && !swapPuzzleMode)
            currentObjectTransform.position = new Vector3(_pointerPosition.x - pieceCenterOffset.x,
                _pointerPosition.y - pieceCenterOffset.y, currentObjectTransform.position.z);


        // If piece rotation requested(by RMB or 2 touched) and possible - rotate piece around Z-axis
        if (_rotationDirection != 0 && randomizeRotation && (!gradualRotation || Time.time > timeToRotate))
        {
            currentObjectTransform.RotateAround(
                pieces[currentPiece].renderer.bounds.center,
                Vector3.forward,
                gradualRotation ? rotationSpeed : _rotationDirection * rotationSpeed * Time.deltaTime
            );
            timeToRotate = Time.time + 0.25f;

            if (!currentGroup)
                pieceCenterOffset = pieces[currentPiece].GetPieceCenterOffset();

            state = PuzzleState.RotatePiece;
        }


        // Tilt piece according to movement direction if needed
        if (dragTiltSpeed > 0)
        {
            currentObjectTransform.localRotation = new Quaternion(
                Mathf.Lerp(currentObjectTransform.localRotation.x,
                    (oldPointerPosition.y - _pointerPosition.y) * dragTiltSpeed, Time.deltaTime),
                Mathf.Lerp(currentObjectTransform.localRotation.y,
                    (oldPointerPosition.x - _pointerPosition.x) * dragTiltSpeed, Time.deltaTime),
                currentObjectTransform.localRotation.z,
                currentObjectTransform.localRotation.w
            );
            oldPointerPosition = _pointerPosition;
        }


        // Drop piece and assemble it to puzzle (if it close enough to it initial position/rotation)    
        if (!_dragInput)
        {
            currentObjectTransform.localRotation = new Quaternion(0, 0, currentObjectTransform.localRotation.z,
                currentObjectTransform.localRotation.w); //Removes the tilt effect
            if (!changeOnlyRotation && !swapPuzzleMode)
                currentObjectTransform.position = new Vector3(currentObjectTransform.position.x,
                    currentObjectTransform.position.y, currentObjectTransform.position.z + dragOffsetZ);

            // Process groups if needed
            if (enablePiecesGroups && !swapPuzzleMode)
            {
                var grouping = false;

                // Get list of all pieces overlapped by currentPiece
                foreach (var movedPieceId in movedPieces)
                    if (movedPieceId != currentPiece)
                        if (currentGroup != null) // if dropped a group
                        {
                            if (pieces[movedPieceId].transform.parent != currentGroup.transform)
                                foreach (var groupPiece in currentGroup.puzzlePieces)
                                    if (groupPiece.renderer.bounds.Intersects(pieces[movedPieceId].renderer.bounds))
                                        overlappedPieces.Add(pieces[movedPieceId]);
                        }
                        else // if dropped a piece
                        if (pieces[currentPiece].renderer.bounds.Intersects(pieces[movedPieceId].renderer.bounds))
                            overlappedPieces.Add(pieces[movedPieceId]);

                // Try to merge overlapped pieces to groups
                for (var i = 0; i < overlappedPieces.Count; i++)
                    grouping |= PuzzlePieceGroup.MergeGroupsOrPieces(pieces[currentPiece], overlappedPieces[i], this);
                overlappedPieces.Clear();

                if (grouping)
                {
                    UpdateUngroupedPiecesList();
                    state = PuzzleState.DropPiece;
                }


                // Assemble grouped pieces to puzzle
                if (currentGroup != null)
                {
                    if (IsPieceInPlace(currentGroup.puzzlePieces[0], allowedDistance, allowedRotation) ||
                        (invertedRules &&
                         !IsPieceInPlace(currentGroup.puzzlePieces[0], allowedDistance, allowedRotation)))
                    {
                        int groupPieceId;

                        foreach (var groupPiece in currentGroup.puzzlePieces)
                        {
                            groupPiece.transform.parent = thisTransform;
                            groupPieceId = GetPieceId(groupPiece);

                            if (invertedRules)
                                movedPieces.Remove(groupPieceId);
                            else
                                ReturnPiece(groupPieceId, movementTime);
                        }

                        Destroy(currentGroup.gameObject);

                        state = PuzzleState.ReturnPiece;
                    }
                    else // Just drop it
                        state = PuzzleState.DropPiece;
                }
            }

            //Assemble ungrouped piece to puzzle
            if (currentGroup == null && !swapPuzzleMode)
                if (IsPieceInPlace(currentPiece, allowedDistance, allowedRotation) ||
                    (invertedRules && !IsPieceInPlace(currentPiece, allowedDistance, allowedRotation)))
                {
                    if (invertedRules)
                        movedPieces.Remove(currentPiece);
                    else
                        ReturnPiece(currentPiece, movementTime);

                    state = PuzzleState.ReturnPiece;
                }
                else // Just drop it
                    state = PuzzleState.DropPiece;


            if (!swapPuzzleMode)
            {
                currentObject = null;
                currentPiece = -1;
            }
        }


        return state;
    }

    //----------------------------------------------------------------------------------------------------
    // Decompose puzzle (randomly moves pieces to specified decompose-areas)
    // If _filterList!=null - will process only pieces from the list
    public void DecomposePuzzle(bool _allowForUnassembled = false, List<int> _filterList = null)
    {
        if (!IsAssembled() && !_allowForUnassembled)
            return;

        Random.InitState(System.DateTime.Now.Millisecond);

        // Set list of decompose sides
        var decomposeSides = new List<int>();
        if (decomposeToLeft) decomposeSides.Add(0);
        if (decomposeToRight) decomposeSides.Add(1);
        if (decomposeToTop) decomposeSides.Add(2);
        if (decomposeToBottom) decomposeSides.Add(3);

        int randomDecomposeSide;
        var targetPosition = new Vector3();
        Vector3 tmpPos;

        // Start decomposition cycle 
        if (changeOnlyRotation)
        {
            for (var i = 0; i < pieces.Length; i++)
                if (_filterList == null || (_filterList != null && _filterList.Contains(i)))
                {
                    movedPieces.Add(i);

                    // Set random Z-rotation (if needed)
                    if (randomizeRotation)
                        pieces[i].transform.RotateAround(pieces[i].renderer.bounds.center, Vector3.forward,
                            rotationSpeed * Random.rotation.z);
                }
        }
        else if (swapPuzzleMode)
        {
            for (var i = 0; i < pieces.Length; i++)
                if (_filterList == null || (_filterList != null && _filterList.Contains(i)))
                {
                    if (!fullyIn3D)
                        targetPosition.z = i * -0.001f - 0.001f;

                    var swapWithPiece = Random.Range(0, pieces.Length);

                    movedPieces.Add(i);
                    movedPieces.Add(swapWithPiece);

                    tmpPos = pieces[i].transform.localPosition;
                    pieces[i].transform.localPosition = pieces[swapWithPiece].transform.localPosition;
                    pieces[swapWithPiece].transform.localPosition = tmpPos;
                    //MovePiece(i, pieces[swapWithPiece].transform.localPosition, true, movementTime / 12);
                    //MovePiece(swapWithPiece, tmpPos, true, movementTime / 12);

                    // Set random Z-rotation (if needed)
                    if (randomizeRotation)
                    {
                        pieces[i].transform.RotateAround(pieces[i].renderer.bounds.center, Vector3.forward,
                            rotationSpeed * Random.rotation.z);
                        pieces[swapWithPiece].transform.RotateAround(pieces[swapWithPiece].renderer.bounds.center,
                            Vector3.forward, rotationSpeed * Random.rotation.z);
                    }
                }
        }
        else if (decomposeSides.Count > 0)
            for (var i = 0; i < pieces.Length; i++)
                if (_filterList == null || (_filterList != null && _filterList.Contains(i)))
                {
                    randomDecomposeSide = Random.Range(0, decomposeSides.Count);

                    // Setup decompose area depth
                    if (!fullyIn3D)
                        targetPosition.z = i * -0.001f - 0.001f;
                    else if (randomDecomposeSide < 2)
                        targetPosition.z =
                            Random.Range(puzzleBounds.center.z - horizontalAreasSize.z / 2 + horizontalAreaOffset.z,
                                puzzleBounds.center.z + horizontalAreasSize.z / 2 + horizontalAreaOffset.z);
                    else
                        targetPosition.z =
                            Random.Range(puzzleBounds.center.z - verticalAreasSize.z / 2 + verticalAreaOffset.z,
                                puzzleBounds.center.z + verticalAreasSize.z / 2 + verticalAreaOffset.z);


                    // Setup decompose area size
                    switch (decomposeSides[randomDecomposeSide])
                    {
                        case 0: // Left
                            targetPosition.x =
                                Random.Range(puzzleBounds.min.x - horizontalAreaOffset.x - pieces[i].size.x,
                                    puzzleBounds.min.x - horizontalAreaOffset.x - horizontalAreasSize.x);
                            targetPosition.y = Random.Range(
                                puzzleBounds.center.y + horizontalAreaOffset.y + horizontalAreasSize.y / 2 +
                                pieces[i].size.y / 2,
                                puzzleBounds.center.y + horizontalAreaOffset.y - horizontalAreasSize.y / 2 +
                                pieces[i].size.y / 2);
                            break;

                        case 1: // Right
                            targetPosition.x = Random.Range(puzzleBounds.max.x + horizontalAreaOffset.x,
                                puzzleBounds.max.x + horizontalAreasSize.x + horizontalAreaOffset.x -
                                pieces[i].size.x / 2);
                            targetPosition.y = Random.Range(
                                puzzleBounds.center.y + horizontalAreaOffset.y + horizontalAreasSize.y / 2 +
                                pieces[i].size.y / 2,
                                puzzleBounds.center.y + horizontalAreaOffset.y - horizontalAreasSize.y / 2 +
                                pieces[i].size.y / 2);
                            break;

                        case 2: // Top
                            targetPosition.x =
                                Random.Range(puzzleBounds.center.x + verticalAreaOffset.x - verticalAreasSize.x / 2,
                                    puzzleBounds.center.x + verticalAreaOffset.x + verticalAreasSize.x / 2 -
                                    pieces[i].size.x);
                            targetPosition.y =
                                Random.Range(puzzleBounds.max.y + verticalAreaOffset.y + pieces[i].size.y,
                                    puzzleBounds.max.y + verticalAreaOffset.y + verticalAreasSize.y);
                            break;

                        case 3: // Bottom
                            targetPosition.x =
                                Random.Range(puzzleBounds.center.x + verticalAreaOffset.x - verticalAreasSize.x / 2,
                                    puzzleBounds.center.x + verticalAreaOffset.x + verticalAreasSize.x / 2 -
                                    pieces[i].size.x);
                            targetPosition.y = Random.Range(puzzleBounds.min.y - verticalAreaOffset.y,
                                puzzleBounds.min.y - verticalAreaOffset.y - verticalAreasSize.y + pieces[i].size.y);
                            break;
                    }

                    pieces[i].Disassemble();

                    // Initiate piece movement to it decomposed position                                                                                                         
                    MovePiece(i, targetPosition, false, movementTime / 2);


                    // Set random Z-rotation (if needed)
                    if (randomizeRotation)
                        pieces[i].transform.RotateAround(pieces[i].renderer.bounds.center, Vector3.forward,
                            gradualRotation
                                ? rotationSpeed * Random.Range(0, 360 / (int) rotationSpeed)
                                : rotationSpeed * Random.rotation.z);
                    // pieces[i].transform.localRotation = new Quaternion(pieces[i].transform.localRotation.x, pieces[i].transform.localRotation.y, Random.rotation.z, pieces[i].transform.localRotation.w); 
                }


        if (enablePiecesGroups && ungroupedPieces.Count == 0)
            CreateUngroupedPiecesList();
    }

    //-----------------------------------------------------------------------------------------------------	
    public void ChangeHorizontalAreasSize(Vector3 _horizontalAreasSize)
    {
        horizontalAreasSize = _horizontalAreasSize;

        if (autoHorizontalAreaOffset)
        {
            horizontalAreaOffset.x = pieces[0].renderer.bounds.size.x / 2;
            horizontalAreaOffset.y = 0;
        }

        ShufflePuzzle();
    }

    //----------------------------------------------------------------------------------------------------
    // Remix available pieces (that haven't been placed, except groups) / returns them to the decomposition areas off of the board. 
    public void ShufflePuzzle()
    {
        DecomposePuzzle(true, enablePiecesGroups ? ungroupedPieces : movedPieces);
    }

    //----------------------------------------------------------------------------------------------------
    // Skip randomized puzzle decomposition and use current pieces positions
    public void NonrandomPuzzle()
    {
        if (!IsAssembled())
            return;

        invertedRules = true;

        for (var i = 0; i < pieces.Length; i++)
        {
            movedPieces.Add(i);
            ungroupedPieces.Add(i);
        }
    }

    //----------------------------------------------------------------------------------------------------     
    // Claculate bounds of the puzzle in World space	 	        		 
    void CalculateBounds()
    {
        puzzleBounds = new Bounds(pieces[0].renderer.bounds.center, Vector3.zero);
        foreach (var piece in pieces)
            puzzleBounds.Encapsulate(piece.renderer.bounds);
    }

    //----------------------------------------------------------------------------------------------------
    // Check is piece close enough(within allowed position/rotation offset) to it origin to consider it returned (assembled to puzzle)
    bool IsPieceInPlace(int _id, float _allowedDistance, float _allowedRotation)
    {
        if (fullyIn3D)
            // return Vector3.Distance(pieces[_id].transform.position, thisTransform.TransformPoint(pieces[_id].startPosition)) < _allowedDistance
            return (pieces[_id].transform.position - thisTransform.TransformPoint(pieces[_id].startPosition))
                   .sqrMagnitude < _allowedDistance * _allowedDistance
                   && (randomizeRotation
                       ? Mathf.Abs(pieces[_id].transform.localRotation.z - pieces[_id].startRotation.z) *
                       Mathf.Rad2Deg < _allowedRotation
                       : true);
        else
            return Vector2.Distance(pieces[_id].transform.position,
                       thisTransform.TransformPoint(pieces[_id].startPosition)) < _allowedDistance
                   && (randomizeRotation
                       ? Mathf.Abs(pieces[_id].transform.localRotation.z - pieces[_id].startRotation.z) *
                       Mathf.Rad2Deg < _allowedRotation
                       : true);
    }


    bool IsPieceInPlace(PuzzlePiece _piece, float _allowedDistance, float _allowedRotation)
    {
        if (fullyIn3D)
            // return Vector3.Distance(_piece.transform.position, thisTransform.TransformPoint(_piece.startPosition)) < _allowedDistance
            return (_piece.transform.position - thisTransform.TransformPoint(_piece.startPosition)).sqrMagnitude <
                   _allowedDistance * _allowedDistance
                   && (!randomizeRotation || Mathf.Abs(_piece.transform.localRotation.z - _piece.startRotation.z) *
                       Mathf.Rad2Deg <
                       _allowedRotation);
        return Vector2.Distance(_piece.transform.position, thisTransform.TransformPoint(_piece.startPosition)) <
               _allowedDistance
               && (!randomizeRotation ||
                   Mathf.Abs(_piece.transform.localRotation.z - _piece.startRotation.z) * Mathf.Rad2Deg <
                   _allowedRotation);
    }


    //----------------------------------------------------------------------------------------------------
    // Get Id for piece under the pointer (calculates for all piecess or only moved)
    public int GetPointedPieceId(Vector3 _pointerPosition, bool _onlyMoved)
    {
        if (_onlyMoved)
            //for (int i = 0; i < movedPieces.Count; i++) 
            for (var i = movedPieces.Count - 1; i >= 0; i--)
            {
                _pointerPosition.z = pieces[movedPieces[i]].renderer.bounds.center.z;
                if (pieces[movedPieces[i]].renderer.bounds.Contains(_pointerPosition))
                    return movedPieces[i];
            }
        else
            for (var i = pieces.Length - 1; i >= 0; i--)
            {
                _pointerPosition.z = pieces[movedPieces[i]].renderer.bounds.center.z;
                if (pieces[i].renderer.bounds.Contains(_pointerPosition))
                    return i;
            }


        return -1;
    }

    //----------------------------------------------------------------------------------------------------
    // Get PuzzlePiece Id in pieces array
    public int GetPieceId(PuzzlePiece _piece)
    {
        for (var i = 0; i < pieces.Length; i++)
            if (_piece == pieces[i])
                return i;

        return -1;
    }

    // Get PuzzlePiece Id in pieces array
    public PuzzlePiece GetPiece(int id)
    {
        for (var i = 0; i < pieces.Length; i++)
        {
            var element = pieces[i];
            if (element.id == id)
                return element;
        }
        return null;
    }

    //----------------------------------------------------------------------------------------------------
    // Return Id of currently interacted piece
    public PuzzlePiece GetCurrentPiece()
    {
        if (currentPiece >= 0) return pieces[currentPiece];
        return null;
    }

    public int GetCurrentPuzzleIndex()
    {
        return GetCurrentPiece() != null
            ? currentPiece
            : -1;
    }
    
    public bool SetActivePuzzle(int id)
    {
        var element = GetPiece(id);
        if (element == null) return false;
        currentObject = element.transform.gameObject;
        currentPiece = id;
        return true;
    }

    public void ResetSelectedPiece()
    {
        currentPiece = -1;
        currentObject = null;
    }

    // Prepare puzzle and Decompose it if needed
    public bool StartPuzzle(bool invertRules)
    {
        if (pieces == null || pieces.Length == 0)
            Prepare();

        gameObject.SetActive(true);
        DecomposePuzzle();
        invertedRules = invertRules;
        return true;
    }

    
    //----------------------------------------------------------------------------------------------------
    // Initiate piece movement to new position (inLocal or World Space)
    public void MovePiece(int _id, Vector3 _targetPosition, bool _useLocalSpace, float _movementTime = -1)
    {
        if (_id < 0 && _id >= pieces.Length)
        {
            Debug.Log("<color=red>WARNING</color>: No suitable piece for returning (all in place or grouped)",
                gameObject);
            return;
        }


        if (_movementTime == 0)
        {
            if (_useLocalSpace)
                pieces[_id].transform.localPosition = _targetPosition;
            else
                pieces[_id].transform.position = _targetPosition;
        }
        else
        {
            if (_movementTime < 0)
                _movementTime = movementTime;

            StartCoroutine(pieces[_id].Move(_targetPosition, _useLocalSpace, _movementTime));
        }


        if (_targetPosition == pieces[_id].startPosition)
        {
            pieces[_id].Assemble();
            movedPieces.Remove(_id);
            ungroupedPieces.Remove(_id);
        }
        else if (!movedPieces.Contains(_id))
            movedPieces.Add(_id);
    }

    //----------------------------------------------------------------------------------------------------
    // Initiate piece (with custom Id) movement to it origin position
    public void ReturnPiece(int _id, float _movementTime = -1)
    {
        if (_id > pieces.Length)
            return;

        // If id < 0: try to get random piece
        if (_id < 0)
            if (enablePiecesGroups)
            {
                if (ungroupedPieces.Count <= 0) return;
                _id = ungroupedPieces[Random.Range(0, ungroupedPieces.Count)];
            }
            else
                _id = movedPieces[Random.Range(0, movedPieces.Count)];

        if (randomizeRotation)
            pieces[_id].transform.localRotation = pieces[_id].startRotation;
        
        MovePiece(_id, pieces[_id].startPosition, true, _movementTime);
    }

    //----------------------------------------------------------------------------------------------------
    // Initiate currentPiece movement to it origin position
    public void ReturnCurrentPiece(float _movementTime = -1)
    {
        MovePiece(currentPiece, pieces[currentPiece].startPosition, true, _movementTime);
        if (randomizeRotation)
            pieces[currentPiece].transform.localRotation = pieces[currentPiece].startRotation;
    }

    //----------------------------------------------------------------------------------------------------
    // Return all piecess to origins. Equal to full assembling of the puzzle
    public void ReturnAll(float _movementTime = -1)
    {
        for (var i = 0; i < pieces.Length; i++)
            MovePiece(i, pieces[i].startPosition, true, _movementTime);
    }

    //----------------------------------------------------------------------------------------------------
    // Enable/Disable all pieces gameObjects
    public void SetPiecesActive(bool isActive)
    {
        if (pieces != null && pieces.Length > 0)
            if (pieces[0].transform.gameObject.activeSelf != isActive)
                for (var i = 0; i < pieces.Length; i++)
                    pieces[i].transform.gameObject.SetActive(isActive);
    }

    public bool IsFoundElement(int id)
    {
        var element = GetPiece(id);
        return IsFoundElement(element);
    }
    
    public bool IsFoundElement(PuzzlePiece element)
    {
        var id = GetPieceId(element);
        foreach (var movedPiece in movedPieces)
        {
            if (movedPiece == id) return false;
        }

        return true;
    }

    //----------------------------------------------------------------------------------------------------
    // Check is puzzle fully assembled or not
    public bool IsAssembled()
    {
        if (assembleInAnyPlace && enablePiecesGroups)
            if (ungroupedPieces.Count == 0 &&
                transform.GetComponentsInChildren<PuzzlePieceGroup>().Length == 1)
                return true;
        
        return movedPieces is {Count: 0};
    }


    //-----------------------------------------------------------------------------------------------------	
    // Save puzzle progress (Assembled pieces only)
    public void SaveProgress(PuzzleSaveData saveData)
    {
        // string saveString = "";
        // for (int i = 0; i < pieces.Length; i++)
        //     if (!movedPieces.Contains(i))
        //         saveString += (i + "|");
        //
        // PlayerPrefs.SetString(_saveKey, saveString);
        //
        // // Save pieces positions/rotation if needed
        // if (enablePositionSaving)
        // {
        //     saveString = "";
        //     Vector3 piecePosition;
        //     for (int i = 0; i < movedPieces.Count; i++)
        //     {
        //         piecePosition = pieces[movedPieces[i]].transform.position;
        //         saveString += (movedPieces[i] + "/" + piecePosition.x + "/" +
        //                        piecePosition.y + "/" + piecePosition.z + "/" +
        //                        pieces[movedPieces[i]].transform.rotation.eulerAngles.z + "|");
        //     }
        //
        //     PlayerPrefs.SetString(_saveKey + "_Positions", saveString);
        // }
    }

    //-----------------------------------------------------------------------------------------------------	
    // Load puzzle progress (Assembled pieces only)
    public void LoadProgress(PuzzleSaveData saveData)
    {
        //ResetProgress();
        movedPieces.Clear();
        var elements = saveData.elements;
        var defaultPosition = new Vector3(10000, 10000, 0);
        
        foreach (var element in elements)
        {
            var id = element.id;
            var piece = GetPiece(id);
            if (piece == null)
                continue;

            piece.transform.position = defaultPosition;
            var pieceTransform = piece.transform;
            if (randomizeRotation)
            {
                var pieceRotation = pieceTransform.rotation.eulerAngles;
                pieceTransform.Rotate(new Vector3(pieceRotation.x, pieceRotation.y, element.angle));
            }

            var isVisible = element.state != PuzzleElementState.Assembled;
            if (isVisible && !movedPieces.Contains(id))
                movedPieces.Add(id);

            var elementObject = piece.transform.gameObject;
            elementObject.SetActive(isVisible);
            var elementState = element.state;
            
            switch (elementState)
            {
                case PuzzleElementState.Hidden:
                    MovePiece(id,new Vector3(100 + id * 10,100 + id * 10), false, 0);
                    break;
                case PuzzleElementState.InGroup:
                case PuzzleElementState.InGame:
                    MovePiece(id,element.position, false, 0);
                    break;
                case PuzzleElementState.Assembled:
                    ReturnPiece(id, 0);
                    break;
            }
        }
        
        ResetSelectedPiece();

        Invoke(nameof(TryToClampAllGroups), 0.1f);
        
        ResetSelectedPiece();
    }

    //----------------------------------------------------------------------------------------------------
    // Reset puzzle progress and saves
    public void ResetProgress()
    {
        StopAllCoroutines();

        state = PuzzleState.None;
        ResetSelectedPiece();
        ungroupedPieces.Clear();
        movedPieces.Clear();
        overlappedPieces.Clear();

        for (var i = 0; i < pieces.Length; i++)
        {
            var piece =  pieces[i];
            piece.transform.parent = transform;
            piece.transform.position = piece.startPosition;
            piece.transform.rotation = piece.startRotation;
            movedPieces.Add(piece.id);
        }
    }

    //----------------------------------------------------------------------------------------------------
    // Updates list of moved and ungrouped pieces Id
    public void UpdateUngroupedPiecesList()
    {
        for (var i = 0; i < ungroupedPieces.Count; i++)
            if (pieces[ungroupedPieces[i]].transform.parent != thisTransform)
            {
                ungroupedPieces.RemoveAt(i);
                i--;
            }
    }

    //----------------------------------------------------------------------------------------------------
    // Сreate list of moved and ungrouped pieces Id
    public void CreateUngroupedPiecesList()
    {
        ungroupedPieces.Clear();
        foreach (var pieceId in movedPieces)
        {
            if (pieces[pieceId].transform.parent == thisTransform)
                ungroupedPieces.Add(pieceId);
        }
    }

    //-----------------------------------------------------------------------------------------------------	
    // Try to find and clamp all pieces/groups
    void TryToClampAllGroups()
    {
        if (!enablePiecesGroups) return;
        
        overlappedPieces.Clear();
        
        for (var i = 0; i < movedPieces.Count; i++)
        {
            currentObjectTransform = pieces[movedPieces[i]].transform;

            foreach (var movedPieceId in movedPieces)
                if (movedPieces[i] != movedPieceId)
                    if (currentObjectTransform.parent == thisTransform ||
                        (currentObjectTransform.parent != thisTransform && currentObjectTransform.parent !=
                            pieces[movedPieceId].transform.parent))
                        if (pieces[movedPieces[i]].renderer.bounds.Intersects(pieces[movedPieceId].renderer.bounds))
                            overlappedPieces.Add(pieces[movedPieceId]);


            for (var j = 0; j < overlappedPieces.Count; j++)
                PuzzlePieceGroup.MergeGroupsOrPieces(pieces[movedPieces[i]], overlappedPieces[j], this);
            overlappedPieces.Clear();
        }

        CreateUngroupedPiecesList();
    }

    //===================================================================================================== 
    // Automatically generate puzzle background from the source image
    public SpriteRenderer GenerateBackground(Texture2D _image, bool _adjustToPuzzle = true,
        bool _centerAnchoring = true)
    {
        if (_image == null)
        {
            Debug.Log("There's no source image passed (or puzzle generated)", gameObject);
            return null;
        }

        var background = new GameObject(_image.name + "_background");
        background.transform.parent = transform;
        background.transform.SetAsFirstSibling();

        var spriteRenderer = background.AddComponent<SpriteRenderer>();
        spriteRenderer.sprite = Sprite.Create(_image, new Rect(0.0f, 0.0f, _image.width, _image.height),
            _centerAnchoring ? new Vector2(0.5f, 0.5f) : new Vector2(0, 1));

        puzzleSprite = spriteRenderer.sprite;

        // Adjust background to puzzle
        if (_adjustToPuzzle)
        {
            background.transform.gameObject.SetActive(true);
            background.transform.localScale = new Vector3(puzzleBounds.size.x / spriteRenderer.bounds.size.x,
                puzzleBounds.size.y / spriteRenderer.bounds.size.y, spriteRenderer.transform.localScale.z);
            background.transform.localPosition = new Vector3(0, 0, 0.2f);
        }


        //spriteRenderer.sortingLayerID = -1;
        background.tag = "Puzzle_Background";

        return spriteRenderer;
    }

    //-----------------------------------------------------------------------------------------------------	
    // Set puzzle anchor
    public PuzzleAnchor SetAnchor(PuzzleAnchor _anchor)
    {
        if (_anchor == anchoring)
            return anchoring;

        if (puzzleBounds.size == Vector3.zero)
        {
            Debug.Log(
                "<b>PuzzleController: </b> <i> Can't set puzzle anchor</i>. Please ensure that you've called <i>Prepare</i> function heretofore.</br> Or just press 'RECALCULATE'",
                gameObject);
            return anchoring;
        }


        Vector2 anchorOffset;
        if (_anchor == PuzzleAnchor.Center)
            anchorOffset = new Vector2(puzzleBounds.size.x / 2, puzzleBounds.size.y / 2);
        else
            anchorOffset = new Vector2(-puzzleBounds.size.x / 2, -puzzleBounds.size.y / 2);

        foreach (var piece in pieces)
            piece.transform.position = new Vector3(piece.transform.position.x - anchorOffset.x,
                anchorOffset.y + piece.transform.position.y, piece.transform.position.z);

        var background = thisTransform.GetChild(0);
        if (background.tag == "Puzzle_Background")
            background.position = new Vector3(background.position.x - anchorOffset.x,
                anchorOffset.y + background.position.y, background.position.z);

        anchoring = _anchor;
        return anchoring;
    }

    //-----------------------------------------------------------------------------------------------------	
    // Automatically align puzzle with camera center
    public void AlignWithCameraCenter(
        Camera _camera,
        bool _centerAnchoring,
        Vector3 cameraOffset,
        bool changeCameraSize = true,
        float cameraSizeMultiplier = 0)
    {
        if (_camera == null)
            _camera = Camera.main;

        if (!_camera)
        {
            Debug.LogError("Please ensure that <i>_camera</i> exists", gameObject);
            return;
        }

        var viewPosition = new Vector3(0.5f, 0.5f, _camera.nearClipPlane);
        var screenViewCenter = _camera.ViewportToWorldPoint(viewPosition);
        screenViewCenter.z = 0;
        transform.position = _centerAnchoring
            ? screenViewCenter
            : new Vector3(screenViewCenter.x - puzzleBounds.center.x, screenViewCenter.y - puzzleBounds.center.y,
                transform.position.z);

        _camera.transform.position += cameraOffset;
        // CalculateBounds();

        // Change camera view size according to size of (puzzle + decompose areas)
        if (_camera.orthographic && changeCameraSize)
        {
            var bounds = puzzleBounds.size.x + (horizontalAreasSize.x + horizontalAreaOffset.x) * 2;
            var cameraSize = Mathf.Ceil(bounds / 4);
            cameraSize *= cameraSizeMultiplier;
            _camera.orthographicSize = cameraSize;
        }
    }

    public Vector2 GetPuzzleScreenSize(Camera gameCamera)
    {
        var size = puzzleBounds.size;
        imageScreenSize = GetWorldToScreenSize(gameCamera, size);
        return imageScreenSize;
    }

    [Button]
    public void FitToScreen(ScreenAnchor anchor,Vector2 pixelsOffset,Vector2 verticalOffset)
    {
        FitToScreen(Camera.main, anchor, pixelsOffset, verticalOffset);
    }
    
    //-----------------------------------------------------------------------------------------------------	
    // Automatically align puzzle/camera with screen corners to fit puzzle with max filling
    public void FitToScreen(
        Camera puzzleCamera, 
        ScreenAnchor anchor, 
        Vector2 pixelsOffset,
        Vector2 verticalOffset)
    {
        if (anchor == ScreenAnchor.None) return;

        var cameraAdjustPosition = new Vector3(
            puzzleBounds.center.x, 
            puzzleBounds.center.y, 
            puzzleCamera.transform.position.z);
                
        var cameraTransform = puzzleCamera.transform;
        var cameraPosition = cameraTransform.position;

        // Adjust size
        cameraTransform.position = cameraAdjustPosition;
        
        var yOffsetTop = GetWorldOffsetFromPixels(puzzleCamera, new Vector3(0,  verticalOffset.x));
        var yOffsetBottom = GetWorldOffsetFromPixels(puzzleCamera, new Vector3(0, verticalOffset.y));

        var max = puzzleBounds.max + yOffsetBottom / 2;
        var min = puzzleBounds.min - yOffsetTop / 2;
        
        var boundsInView = puzzleCamera.WorldToViewportPoint(max);
        
        var size = puzzleCamera.orthographicSize;
        var orthographicSize = size * (boundsInView.y - 1) * 2;
        size += orthographicSize;
        
        puzzleCamera.orthographicSize = size;

        // Adjust position
        boundsInView = puzzleCamera.WorldToViewportPoint(min);

        float positionXModifier = 0;
        float positionYModifier = 0;
        
        switch (anchor)
        {
            case ScreenAnchor.TopLeft:
                positionXModifier = 0;
                positionYModifier = 1;
                break;
            case ScreenAnchor.Center:
                positionXModifier = 0.5f;
                positionYModifier = 0.5f;
                break;
            case ScreenAnchor.BottomLeft:
                positionXModifier = 0;
                positionYModifier = 0;
                break;
            case ScreenAnchor.BottomRight:
                positionXModifier = 1;
                positionYModifier = 0;
                break;
            case ScreenAnchor.TopRight:
                positionXModifier = 1;
                positionYModifier = 1;
                break;
            case ScreenAnchor.CenterRight:
                positionXModifier = 1;
                positionYModifier = 0.5f;
                break;
            case ScreenAnchor.CenterLeft:
                positionXModifier = 0f;
                positionYModifier = 0.5f;
                break;
            default:
                break;
        }

        var diffInViewX = (0.5f - boundsInView.x);
        var diffInViewY = (0.5f - boundsInView.y);

        var boundsSize = max - min;
        var halfSize = boundsSize * 0.5f;
        var center = min + halfSize;

        var diffInSceneX = (center.x - min.x * positionXModifier);
        var diffInSceneY = (center.y - min.y * positionYModifier);

        var cameraX = cameraPosition.x - boundsInView.x * diffInSceneX / diffInViewX;
        var cameraY = cameraPosition.y - boundsInView.y * diffInSceneY / diffInViewY;
        
        cameraPosition = new Vector3(cameraX,cameraY, cameraPosition.z);

        var offsetPoint = GetWorldOffsetFromPixels(puzzleCamera,pixelsOffset);

        cameraPosition += offsetPoint;

        cameraTransform.position = cameraPosition;
    }

    //===================================================================================================== 

    private bool drawOffsets = false;
    public Vector2 leftRightOffset = new Vector2(300, 300);
    public Vector2 topBottomOffset = new Vector2(300, 300);

    public Vector3 GetWorldToScreenSize(Camera gameCamera,Vector3 offset)
    {
        var zero = gameCamera.WorldToScreenPoint(Vector3.zero);
        var offsetPoint = gameCamera.WorldToScreenPoint(offset);
        offsetPoint = offsetPoint - zero;
        offsetPoint.x = Mathf.Abs(offsetPoint.x);
        offsetPoint.y = Mathf.Abs(offsetPoint.y);
        offsetPoint.z = 0;
        
        return offsetPoint;
    }
    
    public Vector3 GetWorldOffsetFromPixels(Camera gameCamera,Vector3 pixelsOffset)
    {
        var zero = gameCamera.ScreenToWorldPoint(Vector3.zero);
        var offsetPoint = gameCamera.ScreenToWorldPoint(pixelsOffset);
        offsetPoint = offsetPoint - zero;
        offsetPoint.z = 0;
        return offsetPoint;
    }
    
    private void DrawGizmosAtCameraSpace(Vector3 center,Vector3 size,Color color)
    {
        if (!drawOffsets) return;
        
        var gizmosCamera = Camera.main;
        if (gizmosCamera == null) return;
        
        var prevColor = Gizmos.color;
        Gizmos.color = color;

        var halfX = size.x / 2;
        var halfY = size.y / 2;

        var leftPoint = gizmosCamera.ScreenToWorldPoint(center - new Vector3(halfX,0,0));
        var rightPoint = gizmosCamera.ScreenToWorldPoint(center + new Vector3(halfX,0,0));
        var topPoint = gizmosCamera.ScreenToWorldPoint(center + new Vector3(0,halfY,0));
        var bottomPoint = gizmosCamera.ScreenToWorldPoint(center - new Vector3(0,halfY,0));

        var worldSize = new Vector3(rightPoint.x - leftPoint.x, topPoint.y - bottomPoint.y, 10);
        var rectCenter = gizmosCamera.ScreenToWorldPoint(center);

        Gizmos.DrawSphere(rectCenter,0.2f);
        Gizmos.DrawWireCube(rectCenter,worldSize);

        Gizmos.color = prevColor;
    }

    private void GizmosDrawOffsets()
    {
        var gameCamera = Camera.main;
        Gizmos.color = Color.yellow;

        if(gameCamera == null) return;

        var width = gameCamera.pixelWidth;
        var height = gameCamera.pixelHeight;
        var rectWidth = (width - leftRightOffset.x - leftRightOffset.y);
        var rectHeight = (height - topBottomOffset.x - topBottomOffset.y);
        
        var finalHeight = height - topBottomOffset.x - topBottomOffset.y;
        var finalWidth = leftRightOffset.x;
        var centerX = leftRightOffset.x / 2;
        var centerY = topBottomOffset.y + finalHeight / 2f;

        var center = new Vector2(centerX, centerY);
        var size = new Vector2(finalWidth, finalHeight);
        
        DrawGizmosAtCameraSpace(center,size,Color.magenta);
        Gizmos.DrawWireCube(center,size);

        centerX = width - (leftRightOffset.y / 2f);
        finalWidth = leftRightOffset.y;
        
        center = new Vector2(centerX,centerY);
        size = new Vector2(finalWidth,finalHeight);
        
        Gizmos.DrawWireCube(center,size);
        DrawGizmosAtCameraSpace(center,size,Color.magenta);

        centerX = leftRightOffset.x + rectWidth / 2f;
        centerY = topBottomOffset.y + rectHeight / 2f;
        
        var gameAreaCenter = new Vector3(centerX,centerY,0);
        center = gameAreaCenter;
        size = new Vector3(rectWidth,rectHeight, 10);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(center,size);
        DrawGizmosAtCameraSpace(center,size,Color.yellow);

        if (movedPieces != null)
            for (var i = 0; i < movedPieces.Count; i++)
            {
                if (currentPiece == i)
                    Gizmos.color = Color.green;
                else
                    Gizmos.color = Color.white;

                Gizmos.DrawWireCube(pieces[movedPieces[i]].renderer.bounds.center,
                    pieces[movedPieces[i]].renderer.bounds.size);
            }

        // Draw PuzzleBounds
        Gizmos.color = Color.grey;
        Gizmos.DrawWireCube(puzzleBounds.center, puzzleBounds.size);

    }
    
    // Utility visualization function
    private void OnDrawGizmos()
    {
        GizmosDrawOffsets();

        // Set color according to Application state and  re-calculate bounds(only in editor mode)
        if (!Application.isPlaying)
            Gizmos.color = new Color(0.25f, 0.25f, 0.75f, 0.5f);
        else
            Gizmos.color = new Color(0.5f, 0.5f, 0.5f, 0.3f);


        // Draw a semitransparent cubes to visualize decomposition areas 
        Vector3 position;
        if (decomposeToLeft)
        {
            position = new Vector3(puzzleBounds.min.x - horizontalAreaOffset.x - horizontalAreasSize.x / 2,
                puzzleBounds.center.y + horizontalAreaOffset.y, horizontalAreaOffset.z);
            Gizmos.DrawCube(position, horizontalAreasSize);
        }

        if (decomposeToRight)
        {
            position = new Vector3(puzzleBounds.max.x + horizontalAreaOffset.x + horizontalAreasSize.x / 2,
                puzzleBounds.center.y + horizontalAreaOffset.y, horizontalAreaOffset.z);
            Gizmos.DrawCube(position, horizontalAreasSize);
        }

        if (decomposeToTop)
        {
            position = new Vector3(puzzleBounds.center.x + verticalAreaOffset.x,
                puzzleBounds.max.y + verticalAreasSize.y / 2 + verticalAreaOffset.y, verticalAreaOffset.z);
            Gizmos.DrawCube(position, verticalAreasSize);
        }

        if (decomposeToBottom)
        {
            position = new Vector3(puzzleBounds.center.x + verticalAreaOffset.x,
                puzzleBounds.min.y - verticalAreasSize.y / 2 - verticalAreaOffset.y, verticalAreaOffset.z);
            Gizmos.DrawCube(position, verticalAreasSize);
        }
    }

    //-----------------------------------------------------------------------------------------------------	
}