﻿/*
 * FancyScrollView (https://github.com/setchi/FancyScrollView)
 * Copyright (c) 2020 setchi
 * Licensed under MIT (https://github.com/setchi/FancyScrollView/blob/master/LICENSE)
 */

using System.Linq;
using Game.Code.JigsawPuzzle.Views.PuzzleScrollViews;
using UnityEngine;

namespace FancyScrollView.Example03
{
    public class Example03Test : MonoBehaviour
    {
        [SerializeField] PuzzleItemsScrollView scrollView = default;

        void Start()
        {
            var scroll = scrollView.scrollView;
            var items = Enumerable.Range(0, 20)
                .Select(i => new PuzzleElementViewModel()
                {
                    message = $"Cell {i}"
                })
                .ToArray();

            scroll.UpdateData(items);
            scroll.SelectCell(0);
        }
    }
}
