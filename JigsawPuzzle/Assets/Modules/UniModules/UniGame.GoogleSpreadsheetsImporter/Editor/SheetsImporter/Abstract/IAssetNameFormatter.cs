namespace UniModules.UniGame.GoogleSpreadsheetsImporter.Editor.SheetsImporter.Abstract
{
    public interface IAssetNameFormatter
    {
        string FormatName(string assetName);
    }
}