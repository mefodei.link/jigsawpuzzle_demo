﻿using UniGame.Core.Runtime;
using UnityEngine;

namespace UniModules.UniGame.AtlasGenerator.Editor.Abstract
{
    public interface ITextureFactory : IFactory<Sprite, Texture2D[]>
    {
    }
}