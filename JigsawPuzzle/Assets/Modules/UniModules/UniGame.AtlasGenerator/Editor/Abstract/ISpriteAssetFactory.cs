﻿using UniGame.Core.Runtime;
using UnityEngine;

namespace UniModules.UniGame.AtlasGenerator.Editor.Abstract
{
    public interface ISpriteAssetFactory : IFactory<SpriteAssetFactoryInputData, Sprite>
    {
    }
}