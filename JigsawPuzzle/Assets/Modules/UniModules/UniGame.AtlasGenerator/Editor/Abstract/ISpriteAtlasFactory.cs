﻿using UniGame.Core.Runtime;
using UnityEngine.U2D;

namespace UniModules.UniGame.AtlasGenerator.Editor.Abstract
{
    public interface ISpriteAtlasFactory : IFactory<string, SpriteAtlas>
    {
    }
}