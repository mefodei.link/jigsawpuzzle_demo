﻿namespace UniModules.UniStateMachine.Runtime.Interfaces
{
    using global::UniGame.Core.Runtime;

    public interface IStateMachine<TState> : 
        ICommonExecutor<TState>
    {

    }
}