using System.Collections.Generic;
using UniCore.Runtime.ProfilerTools;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameContent.Scenes.Examples.UiRectTest
{
    public class UiRectTest : MonoBehaviour, IPointerDownHandler
    {
        private List<RectTransform> _children = new List<RectTransform>();
        
        // Start is called before the first frame update
        private void Start()
        {
            foreach (RectTransform child in transform)
            {
                _children.Add(child);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            OnPointerEvent(eventData);
        }

        private void OnPointerEvent(PointerEventData eventData)
        {
            var point = eventData.position;
            foreach (var cell in _children)
            {
                var contain = RectTransformUtility.RectangleContainsScreenPoint(cell, point,Camera.main);
                
                if (contain)
                {
                    GameLog.Log($"RectangleContains {point} FOUND in {cell.name}",Color.green);
                }
                
                if(!contain) continue;
            }
        }
    }
}
