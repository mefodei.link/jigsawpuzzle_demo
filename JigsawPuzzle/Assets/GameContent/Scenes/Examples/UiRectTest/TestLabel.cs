using TMPro;
using UnityEngine;

namespace GameContent.Scenes.Examples.UiRectTest
{
    public class TestLabel : MonoBehaviour
    {
        // Start is called before the first frame update
        private void Start()
        {
            var label = GetComponent<TextMeshProUGUI>();
            if(label!=null) label.text = name;
        }

    }
}
