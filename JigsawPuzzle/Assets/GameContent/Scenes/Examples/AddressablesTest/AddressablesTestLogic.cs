using Cysharp.Threading.Tasks;
using UniGame.AddressableTools.Runtime;
using UniGame.Core.Runtime.Extension;


namespace GameContent.Scenes.Examples.AddressablesTest
{
    using Sirenix.OdinInspector;
    using UniCore.Runtime.ProfilerTools;
    using UnityEngine;
    using UnityEngine.AddressableAssets;

#if UNITY_EDITOR
    using UniModules.Editor;
#endif
    
    public class AddressablesTestLogic : MonoBehaviour
    {
        public bool loadAtStart = true;
    
        public AssetReferenceT<ScriptableObject> assetReference;
        public AssetReferenceT<ScriptableObject> assetReference2;
    
        [Button]
        public void Load()
        {
            LoadAsset().Forget();
        }

        private void Start()
        {
            if(loadAtStart) Load();
        }

        private async UniTask LoadAsset()
        {
            var lifeTime = this.GetLifeTime();
            var asset = await assetReference
                .LoadAssetTaskAsync<AddressableTestAsset>(lifeTime)
                .ToSharedInstanceAsync(lifeTime);

            var asset2  = await assetReference2
                .LoadAssetTaskAsync<AddressableTestAsset>(lifeTime)
                .ToSharedInstanceAsync(lifeTime);

            var isEqual = asset == asset2;
            if (!isEqual)
            {
                GameLog.LogError($"SHARED INSTANCES NOT EQAUL");
            }
            
            GameLog.Log($"{nameof(AddressablesTestLogic)} VALUE == {asset.value}",Color.green);
            asset.value++;

#if UNITY_EDITOR

            asset.MarkDirty();
            
#endif
        }
    
    }
}
