using UniCore.Runtime.ProfilerTools;
using UniGame.Core.Runtime.ScriptableObjects;
using UnityEngine;

namespace GameContent.Scenes.Examples.AddressablesTest
{
    [CreateAssetMenu(menuName = "Game/Tests/AddressablesTest")]
    public class AddressableTestAsset : LifetimeScriptableObject
    {
        public int value;
        
        private void OnEnable()
        {
            GameLog.Log($"{nameof(AddressableTestAsset)} : CALL = {nameof(OnEnable)}",Color.green);
        }

        private void OnDisable()
        {
            GameLog.Log($"{nameof(AddressableTestAsset)} : CALL = {nameof(OnDisable)}",Color.green);
        }
    }
}
